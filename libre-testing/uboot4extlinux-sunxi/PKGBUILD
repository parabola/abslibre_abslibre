# U-Boot: sunXi
# Maintainer: André Silva <emulatorman@parabola.nu>
# Contributor: Timothy Redaelli <timothy.redaelli@gmail.com>

buildarch=4

pkgbase=uboot4extlinux-sunxi
pkgname=('uboot4extlinux-a10-olinuxino-lime'
         'uboot4extlinux-a10s-olinuxino-m'
         'uboot4extlinux-a13-olinuxino'
         'uboot4extlinux-a13-olinuxinom'
         'uboot4extlinux-a20-olinuxino-lime'
         'uboot4extlinux-a20-olinuxino-lime2'
         'uboot4extlinux-a20-olinuxino_micro'
         'uboot4extlinux-bananapi'
         'uboot4extlinux-bananapro'
         'uboot4extlinux-chip'
         'uboot4extlinux-cubieboard'
         'uboot4extlinux-cubieboard2'
         'uboot4extlinux-cubietruck'
         'uboot4extlinux-linksprite_pcduino'
         'uboot4extlinux-linksprite_pcduino3'
         'uboot4extlinux-linksprite_pcduino3_nano'
         'uboot4extlinux-orangepi_2'
         'uboot4extlinux-orangepi_one'
         'uboot4extlinux-orangepi_pc'
         'uboot4extlinux-orangepi_plus')
pkgver=2016.07
pkgrel=1
arch=('armv7h')
url="http://git.denx.de/u-boot.git/"
license=('GPL')
makedepends=('bc' 'dtc')
backup=(boot/extlinux/extlinux.conf)
source=("ftp://ftp.denx.de/pub/u-boot/u-boot-${pkgver}.tar.bz2"
        '0001-parabola-arm-modifications.patch'
        'extlinux.conf')
md5sums=('425a3fa610a7d972e5092a0e92276c70'
         '010974bdc7a17927e9c134da8aece5eb'
         'd8199207ba08d32465cc4a2b1e19da95')

boards=('A10-OLinuXino-Lime'
        'A10s-OLinuXino-M'
        'A13-OLinuXino'
        'A13-OLinuXinoM'
        'A20-OLinuXino-Lime'
        'A20-OLinuXino-Lime2'
        'A20-OLinuXino_MICRO'
        'Bananapi'
        'Bananapro'
        'CHIP'
        'Cubieboard'
        'Cubieboard2'
        'Cubietruck'
        'Linksprite_pcDuino'
        'Linksprite_pcDuino3'
        'Linksprite_pcDuino3_Nano'
        'orangepi_2'
        'orangepi_one'
        'orangepi_pc'
        'orangepi_plus')

prepare() {
  cd u-boot-${pkgver}

  patch -Np1 -i ../0001-parabola-arm-modifications.patch
}

build() {
  cd u-boot-${pkgver}

  unset CFLAGS
  unset CXXFLAGS
  unset LDFLAGS

  for i in ${boards[@]}; do
    mkdir ../bin_${i}
    make distclean
    make ${i}_config
    make EXTRAVERSION=-${pkgrel}
    mv u-boot-sunxi-with-spl.bin ../bin_${i}
  done
}

package_uboot4extlinux-a10-olinuxino-lime() {
  pkgdesc="U-Boot with Extlinux support for A10 OLinuXino Lime"
  install=${pkgbase}.install
  provides=('uboot4extlinux-sunxi')
  conflicts=('uboot-sunxi' 'uboot4extlinux-sunxi' 'uboot4grub-sunxi')

  install -d "${pkgdir}"/boot/extlinux
  install -Dm644 bin_A10-OLinuXino-Lime/u-boot-sunxi-with-spl.bin "${pkgdir}"/boot

  install -Dm644 extlinux.conf "${pkgdir}"/boot/extlinux
}

package_uboot4extlinux-a10s-olinuxino-m() {
  pkgdesc="U-Boot with Extlinux support for A10s OLinuXino Micro"
  install=${pkgbase}.install
  provides=('uboot4extlinux-sunxi')
  conflicts=('uboot-sunxi' 'uboot4extlinux-sunxi' 'uboot4grub-sunxi')
  replaces=('uboot4extlinux-a10s-olinuxino-micro')

  install -d "${pkgdir}"/boot/extlinux
  install -Dm644 bin_A10s-OLinuXino-M/u-boot-sunxi-with-spl.bin "${pkgdir}"/boot

  install -Dm644 extlinux.conf "${pkgdir}"/boot/extlinux
}

package_uboot4extlinux-a13-olinuxino() {
  pkgdesc="U-Boot with Extlinux support for A13 OLinuXino"
  install=${pkgbase}.install
  provides=('uboot4extlinux-sunxi')
  conflicts=('uboot-sunxi' 'uboot4extlinux-sunxi' 'uboot4grub-sunxi')

  install -d "${pkgdir}"/boot/extlinux
  install -Dm644 bin_A13-OLinuXino/u-boot-sunxi-with-spl.bin "${pkgdir}"/boot

  install -Dm644 extlinux.conf "${pkgdir}"/boot/extlinux
}

package_uboot4extlinux-a13-olinuxinom() {
  pkgdesc="U-Boot with Extlinux support for A13 OLinuXino Micro"
  install=${pkgbase}.install
  provides=('uboot4extlinux-sunxi')
  conflicts=('uboot-sunxi' 'uboot4extlinux-sunxi' 'uboot4grub-sunxi')
  replaces=('uboot4extlinux-a13-olinuxino-micro')

  install -d "${pkgdir}"/boot/extlinux
  install -Dm644 bin_A13-OLinuXinoM/u-boot-sunxi-with-spl.bin "${pkgdir}"/boot

  install -Dm644 extlinux.conf "${pkgdir}"/boot/extlinux
}

package_uboot4extlinux-a20-olinuxino-lime() {
  pkgdesc="U-Boot with Extlinux support for A20 OLinuXino Lime"
  install=${pkgbase}.install
  provides=('uboot4extlinux-sunxi')
  conflicts=('uboot-sunxi' 'uboot4extlinux-sunxi' 'uboot4grub-sunxi')

  install -d "${pkgdir}"/boot/extlinux
  install -Dm644 bin_A20-OLinuXino-Lime/u-boot-sunxi-with-spl.bin "${pkgdir}"/boot

  install -Dm644 extlinux.conf "${pkgdir}"/boot/extlinux
}

package_uboot4extlinux-a20-olinuxino-lime2() {
  pkgdesc="U-Boot with Extlinux support for A20 OLinuXino Lime2"
  install=${pkgbase}.install
  provides=('uboot4extlinux-sunxi')
  conflicts=('uboot-sunxi' 'uboot4extlinux-sunxi' 'uboot4grub-sunxi')

  install -d "${pkgdir}"/boot/extlinux
  install -Dm644 bin_A20-OLinuXino-Lime2/u-boot-sunxi-with-spl.bin "${pkgdir}"/boot

  install -Dm644 extlinux.conf "${pkgdir}"/boot/extlinux
}

package_uboot4extlinux-a20-olinuxino_micro() {
  pkgdesc="U-Boot with Extlinux support for A20 OLinuXino Micro"
  install=${pkgbase}.install
  provides=('uboot4extlinux-sunxi')
  conflicts=('uboot-sunxi' 'uboot4extlinux-sunxi' 'uboot4grub-sunxi')
  replaces=('uboot-a20-olinuxino-micro')

  install -d "${pkgdir}"/boot/extlinux
  install -Dm644 bin_A20-OLinuXino_MICRO/u-boot-sunxi-with-spl.bin "${pkgdir}"/boot

  install -Dm644 extlinux.conf "${pkgdir}"/boot/extlinux
}

package_uboot4extlinux-bananapi() {
  pkgdesc="U-Boot with Extlinux support for Banana Pi"
  install=${pkgbase}.install
  provides=('uboot4extlinux-sunxi')
  conflicts=('uboot-sunxi' 'uboot4extlinux-sunxi' 'uboot4grub-sunxi')

  install -d "${pkgdir}"/boot/extlinux
  install -Dm644 bin_Bananapi/u-boot-sunxi-with-spl.bin "${pkgdir}"/boot

  install -Dm644 extlinux.conf "${pkgdir}"/boot/extlinux
}

package_uboot4extlinux-bananapro() {
  pkgdesc="U-Boot with Extlinux support for Banana Pro"
  install=${pkgbase}.install
  provides=('uboot4extlinux-sunxi')
  conflicts=('uboot-sunxi' 'uboot4extlinux-sunxi' 'uboot4grub-sunxi')

  install -d "${pkgdir}"/boot/extlinux
  install -Dm644 bin_Bananapro/u-boot-sunxi-with-spl.bin "${pkgdir}"/boot

  install -Dm644 extlinux.conf "${pkgdir}"/boot/extlinux
}

package_uboot4extlinux-chip() {
  pkgdesc="U-Boot with Extlinux support for C.H.I.P"
  install=${pkgbase}.install
  provides=('uboot4extlinux-sunxi')
  conflicts=('uboot-sunxi' 'uboot4extlinux-sunxi' 'uboot4grub-sunxi')

  install -d "${pkgdir}"/boot/extlinux
  install -Dm644 bin_CHIP/u-boot-sunxi-with-spl.bin "${pkgdir}"/boot

  install -Dm644 extlinux.conf "${pkgdir}"/boot/extlinux
}

package_uboot4extlinux-cubieboard() {
  pkgdesc="U-Boot with Extlinux support for Cubieboard"
  install=${pkgbase}.install
  provides=('uboot4extlinux-sunxi')
  conflicts=('uboot-sunxi' 'uboot4extlinux-sunxi' 'uboot4grub-sunxi')

  install -d "${pkgdir}"/boot/extlinux
  install -Dm644 bin_Cubieboard/u-boot-sunxi-with-spl.bin "${pkgdir}"/boot

  install -Dm644 extlinux.conf "${pkgdir}"/boot/extlinux
}

package_uboot4extlinux-cubieboard2() {
  pkgdesc="U-Boot with Extlinux support for Cubieboard 2"
  install=${pkgbase}.install
  provides=('uboot4extlinux-sunxi')
  conflicts=('uboot-sunxi' 'uboot4extlinux-sunxi' 'uboot4grub-sunxi')

  install -d "${pkgdir}"/boot/extlinux
  install -Dm644 bin_Cubieboard2/u-boot-sunxi-with-spl.bin "${pkgdir}"/boot

  install -Dm644 extlinux.conf "${pkgdir}"/boot/extlinux
}

package_uboot4extlinux-cubietruck() {
  pkgdesc="U-Boot with Extlinux support for Cubietruck"
  install=${pkgbase}.install
  provides=('uboot4extlinux-sunxi')
  conflicts=('uboot-sunxi' 'uboot4extlinux-sunxi' 'uboot4grub-sunxi')

  install -d "${pkgdir}"/boot/extlinux
  install -Dm644 bin_Cubietruck/u-boot-sunxi-with-spl.bin "${pkgdir}"/boot

  install -Dm644 extlinux.conf "${pkgdir}"/boot/extlinux
}

package_uboot4extlinux-linksprite_pcduino() {
  pkgdesc="U-Boot with Extlinux support for pcDuino"
  install=${pkgbase}.install
  provides=('uboot4extlinux-sunxi')
  conflicts=('uboot-sunxi' 'uboot4extlinux-sunxi' 'uboot4grub-sunxi')
  replaces=('uboot4extlinux-pcduino')

  install -d "${pkgdir}"/boot/extlinux
  install -Dm644 bin_Linksprite_pcDuino/u-boot-sunxi-with-spl.bin "${pkgdir}"/boot

  install -Dm644 extlinux.conf "${pkgdir}"/boot/extlinux
}

package_uboot4extlinux-linksprite_pcduino3() {
  pkgdesc="U-Boot with Extlinux support for pcDuino3"
  install=${pkgbase}.install
  provides=('uboot4extlinux-sunxi')
  conflicts=('uboot-sunxi' 'uboot4extlinux-sunxi' 'uboot4grub-sunxi')
  replaces=('uboot4extlinux-pcduino3')

  install -d "${pkgdir}"/boot/extlinux
  install -Dm644 bin_Linksprite_pcDuino3/u-boot-sunxi-with-spl.bin "${pkgdir}"/boot

  install -Dm644 extlinux.conf "${pkgdir}"/boot/extlinux
}

package_uboot4extlinux-linksprite_pcduino3_nano() {
  pkgdesc="U-Boot with Extlinux support for pcDuino3 Nano"
  install=${pkgbase}.install
  provides=('uboot4extlinux-sunxi')
  conflicts=('uboot-sunxi' 'uboot4extlinux-sunxi' 'uboot4grub-sunxi')
  replaces=('uboot4extlinux-pcduino3-nano')

  install -d "${pkgdir}"/boot/extlinux
  install -Dm644 bin_Linksprite_pcDuino3_Nano/u-boot-sunxi-with-spl.bin "${pkgdir}"/boot

  install -Dm644 extlinux.conf "${pkgdir}"/boot/extlinux
}

package_uboot4extlinux-orangepi_2() {
  pkgdesc="U-Boot with Extlinux for Orange Pi 2"
  install=${pkgbase}.install
  provides=('uboot4extlinux-sunxi')
  conflicts=('uboot-sunxi' 'uboot4extlinux-sunxi' 'uboot4grub-sunxi')

  install -d "${pkgdir}"/boot/extlinux
  install -Dm644 bin_orangepi_2/u-boot-sunxi-with-spl.bin "${pkgdir}"/boot

  install -Dm644 extlinux.conf "${pkgdir}"/boot/extlinux
}

package_uboot4extlinux-orangepi_one() {
  pkgdesc="U-Boot with Extlinux for Orange Pi One"
  install=${pkgbase}.install
  provides=('uboot4extlinux-sunxi')
  conflicts=('uboot-sunxi' 'uboot4extlinux-sunxi' 'uboot4grub-sunxi')

  install -d "${pkgdir}"/boot/extlinux
  install -Dm644 bin_orangepi_one/u-boot-sunxi-with-spl.bin "${pkgdir}"/boot

  install -Dm644 extlinux.conf "${pkgdir}"/boot/extlinux
}

package_uboot4extlinux-orangepi_pc() {
  pkgdesc="U-Boot with Extlinux for Orange Pi PC"
  install=${pkgbase}.install
  provides=('uboot4extlinux-sunxi')
  conflicts=('uboot-sunxi' 'uboot4extlinux-sunxi' 'uboot4grub-sunxi')

  install -d "${pkgdir}"/boot/extlinux
  install -Dm644 bin_orangepi_pc/u-boot-sunxi-with-spl.bin "${pkgdir}"/boot

  install -Dm644 extlinux.conf "${pkgdir}"/boot/extlinux
}

package_uboot4extlinux-orangepi_plus() {
  pkgdesc="U-Boot with Extlinux for Orange Pi Plus"
  install=${pkgbase}.install
  provides=('uboot4extlinux-sunxi')
  conflicts=('uboot-sunxi' 'uboot4extlinux-sunxi' 'uboot4grub-sunxi')

  install -d "${pkgdir}"/boot/extlinux
  install -Dm644 bin_orangepi_plus/u-boot-sunxi-with-spl.bin "${pkgdir}"/boot

  install -Dm644 extlinux.conf "${pkgdir}"/boot/extlinux
}
