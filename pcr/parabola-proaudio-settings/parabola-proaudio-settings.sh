#!/bin/sh

echo 2048 > /sys/class/rtc/rtc0/max_user_freq
echo 2048 > /proc/sys/dev/hpet/max-user-freq
cpupower frequency-set -g performance
echo noop > /sys/block/sda/queue/scheduler
