# Maintainer: Omar Vega Ramos <ovruni@gnu.org.pe>

pkgname=jsr305
pkgver=0.1
pkgrel=1
pkgdesc="Annotations for Software Defect Detection in Java"
arch=('any')
url="https://code.google.com/p/jsr-305/"
license=('BSD')
depends=('java-runtime')
makedepends=('subversion' 'apache-ant' 'jh')
source=("${pkgname}::svn+http://jsr-305.googlecode.com/svn/trunk/#revision=51"
        'jsr305-build_javadoc.patch')
md5sums=('SKIP'
         '3da9aa214b2a8e10168baf9c0a828d94')

prepare() {
  cd "${srcdir}/${pkgname}"
  sed -i '6,11d' ri/pom.xml
  sed -i 's/-SNAPSHOT//g' ri/pom.xml
  sed -i 's/org.jsr-305/com.google.code.findbugs/g' ri/pom.xml
  sed -i 's/>ri/>jsr305/g' ri/pom.xml

  patch -Np1 -i "${srcdir}/jsr305-build_javadoc.patch"
}

build() {
  cd "${srcdir}/${pkgname}/ri"
  ant
}

package() {
  cd "${srcdir}/${pkgname}/ri"
  # Install license file
  install -Dm644 LICENSE "${pkgdir}/usr/share/licenses/${pkgname}/LICENSE"

  # Install documentation
  install -d "${pkgdir}/usr/share/doc/${pkgname}"
  cp -r "build/javadoc" "${pkgdir}/usr/share/doc/${pkgname}"

  # Install Maven artifacts
  export DESTDIR=${pkgdir}
  jh mvn-install "com.google.code.findbugs" ${pkgname} ${pkgver} \
    "pom.xml" \
    "build/${pkgname}.jar" \
    "${pkgname}-${pkgver}.jar"

  ln -s "/usr/share/java/${pkgname}-${pkgver}.jar" \
    "${pkgdir}/usr/share/java/${pkgname}.jar"
}