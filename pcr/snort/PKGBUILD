# Maintainer (Arch): M0Rf30
# Contributor (Arch): Lukas Fleischer <archlinux at cryptocrack dot de>
# Contributor (Arch): Hugo Doria <hugo@archlinux.org>
# Contributor (Arch): Kessia 'even' Pinheiro <kessiapinheiro at gmail.com>
# Contributor (Arch): dorphell <dorphell@archlinux.org>
# Contributor (Arch): Gregor Ibic <gregor.ibic@intelicom.si>
# Maintainer: Omar Vega Ramos <ovruni@gnu.org.pe>

pkgname=snort
pkgver=2.9.8.0
pkgrel=1
pkgdesc='A lightweight network intrusion detection system.'
arch=('i686' 'x86_64')
url='http://www.snort.org'
license=('GPL')
depends=('libdaq' 'libdnet' 'libpcap' 'openssl' 'pcre' 'zlib')
backup=('etc/snort/snort.conf'
        'etc/snort/threshold.conf'
        'etc/snort/reference.config'
        'etc/snort/classification.config'
	'etc/snort/rules/emerging.conf')
options=('!makeflags' '!libtool')
install='snort.install'
source=("https://www.snort.org/downloads/snort/${pkgname}-${pkgver}.tar.gz"
	"http://rules.emergingthreats.net/open/${pkgname}-2.9.0/emerging.rules.tar.gz"
	'snort@.service')

build() {
  cd "${srcdir}/${pkgname}-${pkgver}"
  ./configure --prefix=/usr --sysconfdir=/etc/snort --with-libpcap-includes=/usr/include/pcap \
	      --with-daq-includes=/usr/include --with-daq-libraries=/usr/lib/daq/ \
              --disable-static-daq
  make
}

package() {
  cd "${srcdir}/${pkgname}-${pkgver}"

  make DESTDIR="${pkgdir}" install

  mkdir -p "${pkgdir}/"{etc/rc.d,etc/snort/rules}

  install -d -m755 "${pkgdir}/var/log/snort"
  install -D -m644 etc/{*.conf*,*.map} "${pkgdir}/etc/snort/"

# init service file
  install -D -m644 ../snort@.service $pkgdir/usr/lib/systemd/system/snort@.service

  sed -i 's#/usr/local/lib/#/usr/lib/#' "${pkgdir}/etc/snort/snort.conf"

# emerginthreats rules
  echo 'include $RULE_PATH/emerging.conf' >> "${pkgdir}/etc/snort/snort.conf"
  cp ${srcdir}/rules/* "${pkgdir}/etc/snort/rules"
}
sha512sums=('46e5f19be5eccad2d5b4d3d55ce42fe616cd5f605b7178ed98e86cc8f2f4cf0f796fad80033d81b71bea7da2abfb6d0b340815ee158190f9b974f671045bf002'
            'acd20a250e2e6cc42303a9e01a75ba5a4804ca2b4ca0d41c86cbdef1655eaa647ed15ecd177a86c2f5e71af3faafd7585eca9501fc4b2d989187161305651afb'
            '4eed55f2c8071fd20b132dd6340de8f9066ad52d592212b8d5ba4f89cea488cf8425555bccf160c32502915c0f585aa68dd955c461524898b5ce2cb3c6e6e3c3')
