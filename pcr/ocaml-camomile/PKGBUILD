# Maintainer : Aurélien Desbrières <aurelien@hackers.camp>
# Contributor: Marek Kubica <marek@xivilization.net>
# Contributor: Serge Zirukin <ftrvxmtrx@gmail.com>
pkgname=ocaml-camomile
pkgver=0.8.5
pkgrel=5
pkgdesc="Comprehensive Unicode library for OCaml"
arch=('i686' 'x86_64')
url="https://github.com/yoriyuki/Camomile"
license=('LGPL')
makedepends=('ocaml' 'camlp4' 'ocaml-findlib>=1.2.3')
source=(https://github.com/yoriyuki/${pkgname/ocaml-}/releases/download/rel-$pkgver/${pkgname/ocaml-/}-$pkgver.tar.bz2
        0002-Install-missing-camomileLibrary.a.patch)
options=(!strip !makeflags staticlibs)

prepare() {
  cd "${srcdir}/${pkgname/ocaml-/}-${pkgver}"
  # Patch from Debian, Slackware to add camomileLibrary.a which is required
  # for some code to build
  patch -Np1 -i ../0002-Install-missing-camomileLibrary.a.patch
}

build() {
  cd "${srcdir}/${pkgname/ocaml-/}-${pkgver}"

  ./configure --prefix=/usr
  sed -i -e 's|ocamlfind install|ocamlfind install -destdir '${pkgdir}$(ocamlfind printconf destdir)'|' Makefile
  make
}

package() {
  cd "${srcdir}/${pkgname/ocaml-/}-${pkgver}"

  mkdir -p "${pkgdir}$(ocamlfind printconf destdir)"
  make DATADIR="${pkgdir}/usr/share" install
  install -Dm 644 COPYING ${pkgdir}/usr/share/licenses/${pkgname}/LICENSE
}

