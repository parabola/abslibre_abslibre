# Maintainer: André Silva <emulatorman@parabola.nu>
# Maintainer: Márcio Silva <coadde@parabola.nu>

# Based on pcsx2 (parabola) package

_pkgname=pcsx2
pkgname=pcsx2-git
pkgver=r8317.247d33a
pkgrel=1.parabola1
pkgdesc='A Sony PlayStation 2 emulator, without nonfree nvidia-cg-toolkit support'
arch=('i686' 'x86_64')
url='http://www.pcsx2.net'
license=('GPL2' 'GPL3' 'LGPL2.1' 'LGPL3')
depends_i686=('glew' 'libaio' 'libcanberra' 'libjpeg-turbo'
              'portaudio' 'sdl2' 'soundtouch' 'wxgtk')
depends_x86_64=('lib32-glew' 'lib32-libaio' 'lib32-libcanberra'
                'lib32-libjpeg-turbo'
                'lib32-portaudio' 'lib32-sdl2' 'lib32-soundtouch' 'lib32-wxgtk')
makedepends=('cmake' 'git')
makedepends_x86_64=('gcc-multilib')
optdepends_x86_64=('lib32-gtk-engines: GTK2 engines support'
                   'lib32-gtk-engine-murrine: Murrine GTK3 engine support'
                   'lib32-gtk-engine-unico: Unico GTK2 engine support')
provides=("${_pkgname}")
conflicts=("${_pkgname}" "${_pkgname}-libre")
replaces=("${_pkgname}-libre")
options=('!emptydirs')
source=(${pkgname}-${pkgver}::git+https://github.com/PCSX2/pcsx2.git)
sha256sums=('SKIP')

pkgver() {
  cd ${pkgname}-${pkgver}
  printf "r%s.%s" "$(git rev-list --count HEAD)" "$(git rev-parse --short HEAD)"
}

prepare() {
  cd ${pkgname}-${pkgver}
  # remove zzogl and zerogs plugins (depends nonfree nvidia-cg-toolkit)
  rm -vr plugins/{zerogs,zzogl-pg}
  sed -i '\|# ZZOGL|,+6 d' linux_various/glsl2h.pl
}

build() {
  cd ${pkgname}-${pkgver}

  if [[ -d build ]]; then
    rm -rf build
  fi
  mkdir build && cd build

  if [[ $CARCH == i686 ]]; then
    cmake .. \
      -DCMAKE_BUILD_TYPE='Release' \
      -DCMAKE_INSTALL_PREFIX='/usr' \
      -DCMAKE_LIBRARY_PATH='/usr/lib' \
      -DPLUGIN_DIR='/usr/lib/pcsx2' \
      -DGAMEINDEX_DIR='/usr/share/pcsx2' \
      -DDISABLE_ADVANCE_SIMD='TRUE' \
      -DEXTRA_PLUGINS='TRUE' \
      -DREBUILD_SHADER='TRUE' \
      -DGLSL_API='TRUE' \
      -DPACKAGE_MODE='TRUE' \
      -DXDG_STD='TRUE'
  elif [[ $CARCH == x86_64 ]]; then
    cmake .. \
      -DCMAKE_BUILD_TYPE='Release' \
      -DCMAKE_TOOLCHAIN_FILE='cmake/linux-compiler-i386-multilib.cmake' \
      -DCMAKE_INSTALL_PREFIX='/usr' \
      -DCMAKE_LIBRARY_PATH='/usr/lib32' \
      -DPLUGIN_DIR='/usr/lib32/pcsx2' \
      -DGAMEINDEX_DIR='/usr/share/pcsx2' \
      -DDISABLE_ADVANCE_SIMD='TRUE' \
      -DEXTRA_PLUGINS='TRUE' \
      -DREBUILD_SHADER='TRUE' \
      -DGLSL_API='TRUE' \
      -DPACKAGE_MODE='TRUE' \
      -DXDG_STD='TRUE'
  fi

  make
}

package() {
  cd ${pkgname}-${pkgver}/build

  make DESTDIR="${pkgdir}" install

  # create pcsx2 binary link and copy PCSX2.1 to pcsx2.1 man page
  ln -vs PCSX2 ${pkgdir}/usr/bin/pcsx2
  cp -va ${pkgdir}/usr/share/man/man1/{PCSX,pcsx}2.1
}

# vim: ts=2 sw=2 et:
