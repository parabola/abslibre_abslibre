# Maintainer (Arch): Jan de Groot <jgc@archlinux.org>
# Maintainer (Arch): Jan Alexander Steffens (heftig) <jan.steffens@gmail.com>
# Contributor (Arch): Tom Gundersen <teg@jklm.no>
# Contributor (Arch): Link Dupont <link@subpop.net>
# Maintainer: Omar Vega Ramos <ovruni@gnu.org.pe>

pkgname=dbus-x11
_pkgname=dbus
pkgver=1.10.10
pkgrel=3.1
pkgdesc="Freedesktop.org message bus system, with X11-autostart support"
url="https://wiki.freedesktop.org/www/Software/dbus/"
arch=(i686 x86_64 armv7h)
license=(GPL custom)
provides=('libdbus' "dbus=$pkgver")
conflicts=('libdbus' 'dbus')
#replaces=(libdbus)
depends=(libsystemd expat)
makedepends=(systemd xmlto docbook-xsl python yelp-tools doxygen libx11)
source=(https://dbus.freedesktop.org/releases/$_pkgname/$_pkgname-$pkgver.tar.gz
        0001-Drop-Install-sections-from-user-services.patch)
sha256sums=('9d8f1d069ab4d1a0255d7b400ea3bcef4430c42e729b1012abb2890e3f739a43'
            '48135124680bd9ea2d7d2bd2a9f457608d97bd9aa7cb4f4396e26a1c2c91af3e')

prepare() {
  cd $_pkgname-$pkgver
  patch -Np1 -i ../0001-Drop-Install-sections-from-user-services.patch
  autoreconf -fvi
}

build() {
  cd $_pkgname-$pkgver
  ./configure --prefix=/usr --sysconfdir=/etc --localstatedir=/var \
      --libexecdir=/usr/lib/dbus-1.0 --with-dbus-user=dbus \
      --with-system-pid-file=/run/dbus/pid \
      --with-system-socket=/run/dbus/system_bus_socket \
      --with-console-auth-dir=/run/console/ \
      --enable-inotify --disable-static \
      --disable-verbose-mode --disable-asserts \
      --with-systemdsystemunitdir=/usr/lib/systemd/system \
      --enable-systemd --enable-user-session \
      --enable-x11-autolaunch
  make
}

check() {
  cd $_pkgname-$pkgver
  make check
}

package() {
  cd $_pkgname-$pkgver

  make DESTDIR="$pkgdir" install

  rm -r "$pkgdir/var/run"

  install -Dm644 COPYING "$pkgdir/usr/share/licenses/$_pkgname/COPYING"

  # Split docs
  rm -r "$pkgdir/usr/share/doc"
}
