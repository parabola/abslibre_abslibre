# Maintainer (Arch): Steffen Weber <-boenki-gmx-de->
# Contributor (Arch): Thayer Williams <thayer@archlinux.org>
# Contributor (Arch): James Rayner <james@archlinux.org>
# Maintainer: André Silva <emulatorman@parabola.nu>

pkgname=tango-icon-theme
pkgver=0.8.90
pkgrel=9.parabola1
pkgdesc="Icon theme that follows the Tango visual guidelines (Parabola rebranded)"
arch=('any')
url="http://tango.freedesktop.org"
license=('custom:public domain')
makedepends=('imagemagick' 'icon-naming-utils' 'intltool' 'librsvg')
options=(!strip !zipman)
source=(${url}/releases/${pkgname}-${pkgver}.tar.bz2
        symbol.svg::https://projects.parabola.nu/pac.git/plain/src/img/parabola_officialIcon-default_r2048px2_noa_nof_nos_svg1d1.svg
        rsvg.patch)
md5sums=('b7b9b16480afb781a4c13f8bceb8688b'
         'e603ce69dc3daefba4feb71cd873a606'
         '46f86ad43d793cd5389a7bd9c58a57ae')

prepare() {
  cd ${pkgname}-${pkgver}
  patch -p1 < "${srcdir}/rsvg.patch"
  autoreconf -fi
}

build() {
  cd ${pkgname}-${pkgver}
  ./configure --prefix=/usr --enable-png-creation
  make
}

package() {
  cd ${pkgname}-${pkgver}
  make DESTDIR="${pkgdir}" install

  # install licenses
  install -Dm644 COPYING "${pkgdir}/usr/share/licenses/${pkgname}/LICENSE"

  cd "${pkgdir}/usr/share/icons/Tango"

  # function using imagemagick to combine symbols with default folder-icon
  # used below to create xdg-folders
  comp() {
  composite -gravity center \( ${size}x${size}/"$1".png -resize 50% \) \
   ${size}x${size}/places/folder.png ${size}x${size}/places/folder-"$2".png
  }

  for size in 16 22 24 32 48 64 72 96 128; do
   # replace default logo with Parabola's
   rsvg-convert -w ${size} -h ${size} "${srcdir}/symbol.svg" \
    -o "${size}x${size}/places/start-here.png"
   # create icon for category "Education"
   ln -s "../status/dialog-information.png" \
    "${size}x${size}/categories/applications-science.png"

   # create xdg-folders
   comp apps/internet-web-browser publicshare
   comp actions/go-down download
   comp actions/document-properties templates
   comp mimetypes/audio-x-generic music
   comp mimetypes/image-x-generic pictures
   comp mimetypes/video-x-generic videos
   comp mimetypes/x-office-document documents
  done

  install -Dm644 "${srcdir}/symbol.svg" \
   "scalable/places/start-here.svg"
  ln -s "../status/dialog-information.svg" \
   "scalable/categories/applications-science.svg"
}
