# Maintainer: Omar Vega Ramos <ovruni@gnu.org.pe>

_pkgname=commons-httpclient
pkgname=java-${_pkgname}
pkgver=3.1
pkgrel=1
pkgdesc="A Java(TM) library for creating HTTP clients"
arch=('any')
url="https://hc.apache.org/httpclient-3.x/"
license=('APACHE')
depends=('java-runtime')
makedepends=('apache-ant' 'java-commons-codec' 'java-commons-logging' 'jh')
source=("https://archive.apache.org/dist/httpcomponents/${_pkgname}/source/${_pkgname}-${pkgver}-src.tar.gz"
        "commons-httpclient-utf8_encoding.patch")
md5sums=('2c9b0f83ed5890af02c0df1c1776f39b'
         'c7eb3d8b63c89bb8165b71f7c7fb4eaf')

prepare() {
  cd "${srcdir}/${_pkgname}-${pkgver}"
  patch -Np1 -i "${srcdir}/commons-httpclient-utf8_encoding.patch"
}

build() {
  cd "${srcdir}/${_pkgname}-${pkgver}"
  ant dist \
    -lib "/usr/share/java/commons-logging-1.2.jar" \
    -lib "/usr/share/java/commons-codec.jar"
}

package() {
  cd "${srcdir}/${_pkgname}-${pkgver}"

  # Install license file
  install -Dm644 LICENSE.txt "${pkgdir}/usr/share/licenses/${pkgname}/LICENSE.txt"

  # Install documentation
  install -d "${pkgdir}/usr/share/doc/${pkgname}"
  cp -r "dist/docs/apidocs" "${pkgdir}/usr/share/doc/${pkgname}"

  # Install Maven artifacts
  export DESTDIR=${pkgdir}
  jh mvn-install ${_pkgname} ${_pkgname} ${pkgver} \
    "${srcdir}/${_pkgname}-${pkgver}/project.xml" \
    "${srcdir}/${_pkgname}-${pkgver}/dist/${_pkgname}.jar" \
    "${_pkgname}.jar"
  ln -s "/usr/share/java/${_pkgname}.jar" \
    "${pkgdir}/usr/share/java/${_pkgname}-${pkgver}.jar"
}