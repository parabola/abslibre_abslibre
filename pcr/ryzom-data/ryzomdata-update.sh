#!/usr/bin/sh
# To the extent possible under law, Jorge Araya Navarro has waived all copyright and related or
# neighboring rights to ryzomdata-update.sh. This work is published from: Costa Rica.

error() {
    printf '\E[31m'; echo "$@"; printf '\E[0m'
}

if [[ $(id -u) -ne 0 ]] ; then
    error "Please run as root" ;
    exit 1 ;
fi

# if running as root, then start the update.
if [ -d "/usr/share/ryzom/data/" ]; then
    cd /usr/share/ryzom/data
    msg "Updating the official Ryzom game data, this can take a while..."
    rsync -rtzvu --progress --stats --recursive --timeout=30 www.ryzom.com::ryzom/data/ ./
    msg 'Update completed...'
else
    error "Seems that /usr/share/ryzom/data does not exists. Please install ryzom-data package using pacman."
fi
