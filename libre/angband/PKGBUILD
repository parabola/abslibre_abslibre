# Maintainer (Arch): schuay <jakob.gruber@gmail.com>
# Contributor (Arch): Mike Sampson <mike at sambodata dot com>
# Contributor (Arch): Adrian Stratulat <adrian.stratulat at inboxcom>
# Contributor (Arch): Anton Bazhenov <anton.bazhenov at gmail>
# Contributor (Arch): KillaB <xGenoBlast@gmail.com>
# Contributor (Arch): Callan Barrett <wizzomafizzo@gmail.com>
# Contributor (Arch): Christian Schmidt <xmucknertx@googlemail.com>
# Contributor (Arch): Sebastian Sareyko <public@nooms.de>
# Maintainer: André Silva <emulatorman@parabola.nu>
# Contributor: Márcio Silva <coadde@parabola.nu>

pkgname=angband
_pkgname=$pkgname-libre
pkgver=4.0.5
pkgrel=1.parabola1
pkgdesc="A roguelike dungeon exploration game based on the writings of JRR Tolkien, without nonfree artwork and sound effects"
arch=('i686' 'x86_64' 'armv7h')
url="http://rephial.org/"
license=('GPL2' 'custom')
replaces=($_pkgname)
conflicts=($_pkgname)
depends=('sdl_image' 'sdl_ttf' 'sdl_mixer' 'ncurses')
makedepends=('python-docutils')
mksource=("http://rephial.org/downloads/${pkgver:0:3}/${pkgname}-${pkgver}.tar.gz")
source=("https://repo.parabola.nu/other/${_pkgname}/${_pkgname}-${pkgver}.tar.gz" 'libre.patch')

mksource() {
  cd "${srcdir}/${pkgname}-${pkgver}"

  # remove nonfree stuff from source
  rm -v lib/sounds/*.mp3
  rm -v lib/tiles/shockbolt/64x64.png
}

prepare() {
  cd "${srcdir}/${pkgname}-${pkgver}"

  # Fix detection of ncurses config script for ncurses 6.0
  sed -i 's/ncursesw5-config/ncursesw6-config/g' acinclude.m4

  # remove nonfree references
  patch -Np1 -i ../libre.patch
}

build() {
  cd "${srcdir}/${pkgname}-${pkgver}"

  ./autogen.sh

  ./configure \
    --prefix=/usr \
    --bindir=/usr/bin \
    --sysconfdir=/usr/share/$pkgname \
    --with-configpath=/usr/share/$pkgname \
    --with-libpath=/usr/share/$pkgname \
    --enable-gtk \
    --enable-sdl \
    --enable-sdl-mixer

  make
}

package() {
  cd "${srcdir}/${pkgname}-${pkgver}"

  make DESTDIR="${pkgdir}" install

  rm -f "${pkgdir}/usr/share/$pkgname/*/delete.me"
  install -Dm644 copying.txt "${pkgdir}/usr/share/licenses/${pkgname}/COPYING"
}

mkmd5sums=('82d28d0e51b167fb2b66603e310d9def')
md5sums=('5cd4afb6d2b7fbf11db34f03121cf7e3'
         '06d123697d06a8a08f5367fae3c38137')
