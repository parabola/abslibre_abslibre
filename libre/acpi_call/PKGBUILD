# Maintainer (Arch): Maxime Gauduin <alucryd@gmail.com>
# Contributor (Arch): mortzu <me@mortzu.de>
# Contributor (Arch): fnord0 <fnord0@riseup.net>
# Maintainer: André Silva <emulatorman@parabola.nu>
# Contributor: Luke Shumaker <lukeshu@sbcglobal.net>
# Contributor: Márcio Silva <coadde@parabola.nu>

_kernelname=

if [[ ${_kernelname} == "" ]]; then
  _basekernel=4.9
  _archrel=53
  _parabolarel=1
elif [[ ${_kernelname} == -lts ]]; then
  _basekernel=4.4
  _archrel=36
  _parabolarel=1
fi

_pkgname=acpi_call
pkgname=${_pkgname}${_kernelname}
pkgver=1.1.0
pkgrel=${_archrel}.parabola${_parabolarel}.basekernel${_basekernel}
_extramodules=extramodules-${_basekernel}${_kernelname}
pkgdesc="A linux kernel module that enables calls to ACPI methods through /proc/acpi/call (built for the linux-libre${_kernelname} kernel package)"
arch=('i686' 'x86_64' 'armv7h')
url="http://github.com/mkottman/${_pkgname}"
license=('GPL')

# Generic (you shouldn't have to modify any of these variables)
_toohigh=$(IFS=. read a b <<<$_basekernel; echo $a.$((b+1)))
depends=("linux-libre${_kernelname}>=${_basekernel}" "linux-libre${_kernelname}<${_toohigh}")
makedepends=("linux-libre${_kernelname}-headers>=${_basekernel}" "linux-libre${_kernelname}-headers<${_toohigh}")
makedepends+=('libelf')

replaces=("${_pkgname}-libre${_kernelname}" "${_pkgname}-parabola${_kernelname}")
[[ -n ${_kernelname} ]] && replaces+=("${_pkgname}${_kernelname}-parabola")
conflicts=("${_pkgname}-libre${_kernelname}" "${_pkgname}-parabola${_kernelname}")
[[ -n ${_kernelname} ]] && conflicts+=("${_pkgname}${_kernelname}-parabola")
if [[ ${_kernelname} != "" ]]; then
  provides=("${_pkgname}")
fi

install='kmod.install'
source=("${_pkgname}-${pkgver}.tar.gz::${url}/archive/v${pkgver}.tar.gz")
sha256sums=('d0d14b42944282724fca76f57d598eed794ef97448f387d1c489d85ad813f2f0')

prepare() {
  cd "${srcdir}/${_pkgname}-${pkgver}"

  if [[ ${_basekernel} > 3.16 ]]; then
    # Fix build with Linux-libre 3.17-gnu
    sed -i 's|acpi/acpi.h|linux/acpi.h|' acpi_call.c
  fi
}

build() {
  cd "${srcdir}/${_pkgname}-${pkgver}"

  _kernver="$(cat /usr/lib/modules/${_extramodules}/version)"

  make KVERSION="${_kernver}"
}

package() {
  cd "${srcdir}/${_pkgname}-${pkgver}"

  # Set the correct extramodules directory for install
  cp -f "${startdir}/${install}" "${startdir}/${install}.pkg"
  true && install=${install}.pkg
  sed -i "s/^_EXTRAMODULES=.*/_EXTRAMODULES="${_extramodules}"/" "${startdir}/${install}"

  # Actually install
  install -dm 755 "${pkgdir}"/usr/lib/{modules/${_extramodules},modules-load.d}
  install -m 644 ${_pkgname}.ko "${pkgdir}"/usr/lib/modules/${_extramodules}
  gzip "${pkgdir}"/usr/lib/modules/${_extramodules}/${_pkgname}.ko
  echo ${_pkgname} > "${pkgdir}"/usr/lib/modules-load.d/${_pkgname}${_kernelname}.conf

  install -dm 755 "${pkgdir}"/usr/share/${_pkgname}${_kernelname}
  cp -dr --no-preserve='ownership' {examples,support} "${pkgdir}"/usr/share/${_pkgname}${_kernelname}/
}

# vim: ts=2 sw=2 et:
