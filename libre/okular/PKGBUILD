# Maintainer (Arch): Felix Yan <felixonmars@archlinux.org>
# Maintainer (Arch): Antonio Rojas <arojas@archlinux.org>
# Contributor (Arch): Andrea Scarpino <andrea@archlinux.org>
# Maintainer: André Silva <emulatorman@parabola.nu>

pkgname=okular
pkgver=16.12.0
pkgrel=1.parabola1
pkgdesc='Document Viewer, with copy-protection anti-feature turn off by default'
arch=(i686 x86_64 armv7h)
url="http://kde.org/applications/graphics/okular/"
license=(GPL LGPL FDL)
groups=(kde-applications kdegraphics)
depends=(chmlib djvulibre libspectre libkexiv2 poppler-qt5 qca-qt5 kpty khtml kactivities threadweaver)
makedepends=(extra-cmake-modules ebook-tools kdegraphics-mobipocket kdoctools python)
optdepends=('ebook-tools: mobi and epub support'
            'kdegraphics-mobipocket: mobi support')
conflicts=(kdegraphics-okular)
replaces=(kdegraphics-okular)
source=("http://download.kde.org/stable/applications/${pkgver}/src/okular-${pkgver}.tar.xz"{,.sig}
        'turn_off_copy-protection_anti-feature.patch')
sha1sums=('7ffdde9237b805bad539edd8a6d431c845bcb03d'
          'SKIP'
          'f7323d1d39d660785fa71c0365a17795008db819')
validpgpkeys=(CA262C6C83DE4D2FB28A332A3A6A4DB839EAA6D7) # Albert Astals Cid <aacid@kde.org>

prepare() {
  mkdir -p build
  cd $pkgname-$pkgver
  patch -p1 -i ../turn_off_copy-protection_anti-feature.patch
}

build() {
  cd build
  cmake ../$pkgname-$pkgver \
    -DCMAKE_BUILD_TYPE=Release \
    -DCMAKE_INSTALL_PREFIX=/usr \
    -DCMAKE_INSTALL_LIBDIR=lib \
    -DBUILD_TESTING=OFF
  make
}

package() {
  cd build
  make DESTDIR="$pkgdir" install
}
