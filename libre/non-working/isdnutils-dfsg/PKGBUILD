# Maintainer: Luke Shumaker <lukeshu@sbcglobal.net>

# Debian information
_debname=isdnutils
_debver=3.25+dfsg1
_debrel=3.5
_debrepo=http://ftp.debian.org/debian/pool/main

pkgname=${_debname}-dfsg
pkgver=${_debver/+dfsg/.}.${_debrel}
pkgdesc="Debian's user space administration programs and tools for ISDN"
url="http://packages.debian.org/source/sid/isdnutils"
license=(
	'GPL2' 'LGPL2' # most everything
	'custom' # Mini-FAQ
	'ZLIB' # areacode
	'BSD4' 'BSD4-modified' 'BSD3' 'BSD2' 'MIT' # ipppd
	'X11' # xisdnload
)

_debfile() { echo "${_debrepo}/${1:0:1}/${1%_*}/${1}"; }
source=($(_debfile ${_debname}_${_debver}-${_debrel}.dsc)
        $(_debfile ${_debname}_${_debver}.orig.tar.bz2)
        $(_debfile ${_debname}_${_debver}-${_debrel}.debian.tar.xz)
        modern-autoconf.patch
)
noextract=("${source[@]##*/}")
md5sums=('19d43212c307d30db9dad2413e84374b'
         '38841f0c697d49fd71ff91e9188f6d4c'
         '68b901c1553771bcef9a9297a93c4a46'
         '9fa4158b19d5c9b774d14af34767ef1b')

# Package meta-data
pkgrel=1
arch=(i686 x86_64)
depends=(sh)
makedepends=(imake dpkg-devtools linuxdoc-tools zip linux-api-headers)
optdepends=(
	"ncurses: for imon and vbox"
	"tcl: for vbox"
)
makedepends+=("${optdepends[@]%%:*}")

# isdnutils-dpkg replaces isdn4k-utils(Arch) replaces capi4k-utils(Arch)
replaces=('isdn4k-utils' 'capi4k-utils')
provides=("isdn4k-utils=$pkgver")

####

# Use `make -j1` because the cappiinfo subdir depends on capi20, and
# automake has no way of expressing this.
prepare() {
	cd "$srcdir"
	dpkg-source -x ${_debname}_${_debver}-${_debrel}.dsc $pkgname-$pkgver
	cd "$srcdir"/$pkgname-$pkgver

	find . -name configure.in -execdir mv {} configure.ac \;
	patch -p1 -i ../modern-autoconf.patch
	(
		set -o pipefail
		find * -name configure.ac -printf '%h\n' |
		while read -r dir; do
			echo "Running \`autoreconf -if' in \`$dir'..."
			pushd "$dir" >/dev/null
			autoreconf -if |& sed 's,^,    ,'
			popd >/dev/null
		done
	)

	sed \
		-e "s#@KERNELDIR@#/usr/include/linux#" \
		-e "s#@LIBDIR@#/usr/lib#" \
		debian/dotconfig > .config
}

build() {
	cd "$srcdir"/$pkgname-$pkgver

	make -j1 subconfig
	make -j1
	make -j1 -C isdnlog/tools/zone zonefiles
	make -j1 -C Mini-FAQ
}

package() {
	cd "$srcdir"/$pkgname-$pkgver

	mkdir -p "$pkgdir"/usr/bin
	ln -s bin "$pkgdir"/usr/sbin
	ln -s usr/bin "$pkgdir"/bin
	ln -s usr/bin "$pkgdir"/sbin

	make -j1 install DESTDIR="$pkgdir"
	install -Dm644 debian/copyright "$pkgdir"/usr/share/licenses/$pkgname/copyright

	rm "$pkgdir"/{bin,sbin,usr/sbin}

	# The following is adapted debian/rules:override_dh_auto_install
	mv "$pkgdir"/usr/share/man/man5/vbox{_file,}.5
	install -Dm0644 vbox/examples/vboxrc.example "$pkgdir"/usr/share/doc/isdnvboxclient/examples/vboxrc
}
