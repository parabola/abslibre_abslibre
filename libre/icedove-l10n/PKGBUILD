# Maintainer: André Silva <emulatorman@parabola.nu>
# Contributor: Márcio Silva <coadde@parabola.nu>

epoch=1
_pkgbase=icedove
pkgbase=$_pkgbase-l10n
_pkgver=45.5.1
pkgver=45.5.1.deb1
pkgrel=1
pkgdesc="Language pack for Debian ${_pkgbase^}."
arch=('any')
url="http://packages.debian.org/sid/icedove"
license=('MPL' 'GPL')
depends=("$_pkgbase=$epoch:$pkgver")
makedepends=('unzip' 'zip')

_languages=(
  'ar     "Arabic"'
  'ast    "Asturian"'
  'be     "Belarusian"'
  'bg     "Bulgarian"'
  'bn-BD  "Bengali (Bangladesh)"'
  'br     "Breton"'
  'ca     "Catalan"'
  'cs     "Czech"'
  'cy     "Welsh"'
  'da     "Danish"'
  'de     "German"'
  'dsb    "Lower Sorbian"'
  'el     "Greek"'
  'en-GB  "English (British)"'
  'en-US  "English (US)"'
  'es-AR  "Spanish (Argentina)"'
  'es-ES  "Spanish (Spain)"'
  'et     "Estonian"'
  'eu     "Basque"'
  'fi     "Finnish"'
  'fr     "French"'
  'fy-NL  "Frisian"'
  'ga-IE  "Irish"'
  'gd     "Gaelic (Scotland)"'
  'gl     "Galician"'
  'he     "Hebrew"'
  'hr     "Croatian"'
  'hsb    "Upper Sorbian"'
  'hu     "Hungarian"'
  'hy-AM  "Armenian"'
  'id     "Indonesian"'
  'is     "Icelandic"'
  'it     "Italian"'
  'ja     "Japanese"'
  'ko     "Korean"'
  'lt     "Lithuanian"'
  'nb-NO  "Norwegian (Bokmål)"'
  'nl     "Dutch"'
  'nn-NO  "Norwegian (Nynorsk)"'
  'pa-IN  "Punjabi (India)"'
  'pl     "Polish"'
  'pt-BR  "Portuguese (Brazilian)"'
  'pt-PT  "Portuguese (Portugal)"'
  'rm     "Romansh"'
  'ro     "Romanian"'
  'ru     "Russian"'
  'si     "Sinhala"'
  'sk     "Slovak"'
  'sl     "Slovenian"'
  'sq     "Albanian"'
  'sr     "Serbian"'
  'sv-SE  "Swedish"'
  'ta-LK  "Tamil (Sri Lanka)"'
  'tr     "Turkish"'
  'uk     "Ukrainian"'
  'vi     "Vietnamese"'
  'zh-CN  "Chinese (Simplified)"'
  'zh-TW  "Chinese (Traditional)"'
)

pkgname=()
source=('brand.dtd' 'brand.properties' 'region.properties')
_url=https://ftp.mozilla.org/pub/mozilla.org/thunderbird/releases/$_pkgver/linux-i686/xpi

for _lang in "${_languages[@]}"; do
  _locale=${_lang%% *}
  _pkgname=$pkgbase-${_locale,,}

  pkgname+=($_pkgname)
  source+=("$pkgbase-$_pkgver-$_locale.xpi::$_url/$_locale.xpi")
  eval "package_$_pkgname() {
    _package $_lang
  }"
done

# Don't extract anything
noextract=(${source[@]%%::*})

_package() {
  pkgdesc="$2 language pack for Debian Icedove."
  replaces=(thunderbird-i18n-${1,,} icedove-l18n-${1,,})
  conflicts=(thunderbird-i18n-${1,,} icedove-l18n-${1,,})
  provides=(thunderbird-i18n-${1,,})

  unzip icedove-l10n-$_pkgver-$1.xpi -d $1
  rm -v icedove-l10n-$_pkgver-$1.xpi
  install -vDm644 $srcdir/brand.dtd $1/chrome/$1/locale/$1/branding
  install -vDm644 $srcdir/brand.properties $1/chrome/$1/locale/$1/branding
  install -vDm644 $srcdir/region.properties $1/chrome/$1/locale/$1/messenger-region
  rm -rv $1/chrome/$1/locale/$1/global-platform/{mac,win}
  sed -i -e 's/thunderbird/icedove/' $1/install.rdf
  cd $1
  zip -r langpack-$1@icedove.mozilla.org.xpi .
  mv -v langpack-$1@icedove.mozilla.org.xpi $srcdir
  cd ..
  rm -rv $1

  install -vDm644 langpack-$1@icedove.mozilla.org.xpi \
    "$pkgdir/usr/lib/icedove/extensions/langpack-$1@icedove.mozilla.org.xpi"
}

sha256sums=('49b419449431d8d64e20427a2e7105e3ac1ffc41e677a5f9a8eb276f5b82df4a'
            'acc2cf95661be7cb8928fca89e08d0681685409ff2428e4e3d25baf1af427b38'
            'e6edcc7c478d73ca5a17e4889acd302d55201ec5c29de545d3f44165201923db'
            'fec980dd1fb857da2862cb7752593cb6ea633b60e27000359bb57509d8f9837d'
            'a014aa0c52c7750afa405cf021804c6a3372c006e5ed1790727cc6dbd67ff6c9'
            'f82e2f29997cbafed143a799191fa36da2f5b668cb4fb58520f1ffaa74cc1e9a'
            '060113942fa37ba88abb1994603fdbcfdcd1cf0ed7c377f700cb54b92f1874dd'
            '90aaaec5db7fb76e71bb092d0619fc6180ba2ede00313f11a30f7e10d9208f4b'
            '21bfd87ff4b5949ab265862bc2c16aa7fdbf1dedd3f6f612a121eaee82cafaea'
            '55a05d560d61d2f78360a1a6dfd0d3c8b8efae2a705b10857a3d43116e4734d3'
            'c19e34d7717e12a8b85e13fd97ee3e7eedf1a34445fe74e2b14044b5cc501112'
            '94df5bc2301a0ee4b7e94cc198395dea0bee71f054d842c5aea4445528cecb4f'
            '83337bc7a3caf67bfb5bdb0e397e5337b2bfa4c09b123a241d1dc90507871a43'
            'a7342c3019b174ed3dba7ef647eff0fcb55347312bb1d84b4effcda050513932'
            '6e7f2ff1ed0b42f932ef36dc3350ec1757e3897cd662165c347e97bb1bcad565'
            '352b2968890884ebecdfb70f77729b25b4a595375ba9acbf1f4f8ba8e5c8f111'
            'b3b533015d9f5a2ecc417f7c5dbcebe15a1be6cbe2325ffff357e34e905b3682'
            '28ed8e10f19e1b55b6dbdd48d065dddecae4a557e71c20fc4681b9f2f2fb7541'
            '6e1c85dce2fd30b4b9555164f98837fb06262a4285510d5c4a9d5d5433359516'
            '929478450eca54157a907698f1d10f70b773419fc7ba155c50627be62e7248e4'
            'b2820d101d9abd1779d9abeac757a7a48a86f617ace163f0447eaa57bcde1cd2'
            'dedfffc8f8ced2de5d5e4bc326e6dfe8d8b8e5d927cd3d468fc6c3345c92b9b7'
            '26745f076f0f337d941082646b2555bf877d16c9deb8dd95d8d90ef5d33d5c21'
            '39635b8a71e8183fe77ef13bf60b87077c223bd9da91345f8349be19f37be140'
            '545b747982cbfa2aebc0857ee627f60f4bc314a97a51f862e5961deb6ab2f7f5'
            '637a1b7d1ac97b77b862e2ed804b8c381c8a8d38e92a586166d51bdacb8d8522'
            'b633407bf67b877655b25ba1d73cc7cc0916d6b75b9f104ac9833f09e6d11678'
            'cdae0ffa3198b7c1439a83d942d2be123e8a01ed8944532456fd8a6f2d31eda3'
            '649c04fc00d6259819ad20106649f3f3c1bdccfaf0f02f2155dbe8c71b7efdda'
            '6ee8423130698115e72bdb46e80fa9d91730e7de4c6175e710a63dc35006c376'
            '9a11c854cd2b67850532f76ec405f12e14980cf742429aa806de0ef3716ea895'
            '3bb4e71c47f4bd69c7440cf48275770486257e402355f92d29643c1c76296398'
            '4308efa5b06b1964c3ccbdf0ddecd005235ea43084a58e8d1cbc75d0b7ea94af'
            '39500f98291b84ec81d6e0e9dfd1d3880c610331e8c64e3338f02d81837a7bdd'
            'dadde3ffc441aba7910ce97d26537311e23f4181ebf58da1b14712f85ac0ae94'
            '9b1a073a493511ebe765ebe9227e464de4df87061cd2e1755bfa883055742a5a'
            '2c2c09097f559fab4b88869cec1580c7f5c01b9e20eafaeff3036c97beb932fe'
            '2e4b5cd28f5f0a8d186e22528e33e5e7650e670ae234812547d41bec343bfb6a'
            '24e2c85be75e15855daac0470b417b9c81b1dce1007561800dcb66dd8763b092'
            'ef7b32bef23f9ced34d45b366c2646ba6298b3ec14cc7742887c4841b580d17a'
            'c54b5ec86b35aba0867a423cd031229c11b173b48107912eadde83266998268a'
            'b7bac0f0ed551cccb7f36894e61ec935db21dfe99d9896234ecb6d00ef08cc36'
            'c672bb1f9fb70eb1b5342bc5b78717aacdad21a0d19e087c3c83c5ee955ff984'
            'daddde1dc789a1a451f0ef4dc5b8f2c1f6a31df12a360f285dbc8ad1e587bc36'
            '3018cc64cf7506e69b006a87f3f818ee3ce394de5c030a5a03fc82e0bd9ba6ae'
            '4b95ff7033e22cf33e80d37b76ba49beee8cd171912e7a69cd8486519a300cdb'
            '66f00cdd771d825c06f92d1766c705f0a72337538de48fd5f9bba80c5e12bbd0'
            '13207aaa727ce3fbf35b282fb69231ee1e0bae0ccea7a56f8588d1e5df12fe7d'
            '7e01a6b6395d7d1815b1260421b82b7b901c6318db4178342a8229ebd008d935'
            '03bb1b1959401ac6726caf3d7442e1f38ae1d8495b64cddbbd2f6c59437fc4ed'
            '2f116dd7a8d87b575da007dc3dd99fb5b4f7ef1ca00feb46c570803e0d6b9a53'
            '0b1a9a82cea99db266d0d20fe433775de8234ef08feb093205706b59feb8b2eb'
            'de5dac529a7271b012826ba59cb1cf46319293c2defdcd51922f636a6e630a85'
            '965e8dc6a6b96d991c521793db7957c76feab3b27667e46a3849639e38096906'
            '4ab6b4c523c2143ec72b15a6afbd5e326d67a212ef7f8e89ca605c9683cb1f78'
            '7229c04541aa23c96a66af4f06e6deae8fb9b101f4153c1cc029916a64733514'
            'aa94327b95f7ad0ec5bf89cf7ad083cebd6fa43b13ce03eb0a33532acca65d2b'
            '4359a54010c18b807a3cf46e8eee122e10b9ebb4d8ce810a7211060360725769'
            '156b5944845979e73f7e9051f6afc2cdd5a8d9b106007fa558171b849aba99b0'
            'f54fc5439f39ca8ede17beddbc44f3f2ee25bcb1db0c0b3a1f4084a157b5cc2a'
            '52c05b5064eec0d91fca90c5007454d0bf4225f45c6833a93b892c0592b4ada3')
sha256sums=('49b419449431d8d64e20427a2e7105e3ac1ffc41e677a5f9a8eb276f5b82df4a'
            'acc2cf95661be7cb8928fca89e08d0681685409ff2428e4e3d25baf1af427b38'
            'e6edcc7c478d73ca5a17e4889acd302d55201ec5c29de545d3f44165201923db'
            'b4fe20d0ca69cb19d58b136b3cd20a8e14b9bffb4ab21817652ba906d75bd4e4'
            '58870dfe8133718588babe6fbc0d994d015639472124cfcfd28fa1e921394e3d'
            'ff471d1ffea64bab40786723c18ceb3fc0e496657b240fb596780907b24d1170'
            'd1a4bf184683cf031ee5b4582df324a2a79091740892429e2b26f8805d627e01'
            '5bc7ed319271a9e30c2d4926dc5bf8e5770de087f3118819ae1cc1f539b68697'
            'cba824a751c1fdfdf9f57405fab2f3920580d806a494545ad10fb3c64e0af7b9'
            '65027455332178cc1e08d69e00791ff9511209f2ff4fa48d7753de00c94dd8cd'
            'fbe270b30213ee3469c866c9c360ea518f7ad01ed68a195e99d0bbf470f9e538'
            '8a0a58fcfcaa7cc919d350c9735199566a639ec1d383914934e6fe21f4e4eb82'
            'ef1b199a05fdfafbbeaf5fbf057fd07ac13a048a33054544dec726f345ea1a0e'
            '7b371f854bb8b7737aa4625f5d559c7fffb06edcf1d138e457d67e8928664103'
            'aa35a8b43ee6e5517705ceddf02479af3dfe60ad544f4cf2d79c6626dc174ac8'
            'e7363b527e78e84d027af6a35055c260932ddad2dda4f3a9f8878990d95f6b4e'
            '4bce48c5a33e541254416590b6bf41151cf0254bf89aaabf5f64b890b2cad1e0'
            '1c5ab20dd75263b734cd54e6fbfd7dbbee6ee69a88bb89b2e03fa650c86bbd9b'
            '7584a2563e5747813068a9f02bed62c63b37eb1ba91f522a807bdf0d781ddffc'
            '2a4497a52454deb83578efe8d532c5b089c3a35eaff79bf67be672a01ec004ff'
            '82ef7cd2827bfb1f928785a212c59214b8b06fb7b559b38aa445d67b367236a2'
            '8051087402329512cc2fcb2380a9b85da0d71d898bf0b6c88bc6ed129ffb22cf'
            '2e2b32ac0f4413fc26c6462031d117fe99c8f8738eef0fae27de811349ca9f8f'
            '8c8f63e68e05ededbd129b992e455af388601a016ab916729e683123aca1cb37'
            '1573d35cba1b687210358f8739ddb59bdfd8a14adda6ade32a84d1204e6a40e9'
            'd5f55b408d894d0f439d2e8c437f4ad13750a096b6d53280eaed629709dfb54b'
            'ef289b48baa8621ea553a17e3357b67e8618f5ce8995f44d82396a5e1aeacb27'
            '60d4adac11929fdb31ee19ae93fc85fff3c523755fe0c1144460ec4bf56b7992'
            '8ff8f876565f08da43d3d1e8c744f2e0c82976499d7d5389326990f1c011b56e'
            '17184e2537122360540431ac7b5ef5a25ce6049b025c865c5fb28aa617a0e3b8'
            '492ffdf47b0a7260be7c7dd4f95b9732dc4c88971fdb5a478b5a9fe158f3eeee'
            'ea016fc595f0643476ef78d5f2e99c1fb5bd0b944cb8460447614753b6c878bb'
            '527004f57d81953de82688f541983c8eb506757ccccad859d9b0e3cf0f448bee'
            '056c1e40533cc751b59e1f004cf890cbb8beecce3e9c583691e85f87939903e0'
            '818e55e875f321caa6e72babccbf24772aee3a8d378904c113769939e4f7d042'
            'f72ec16adeff2158ece5d6991bf95a575acc162db26769fe2bee424d83a0ae97'
            '4c812beefe3e10100b8561a35f95421a148dbb6ad87ecb9e8ed9863ecc331b5e'
            'b7b408f8e2bf6b39c44f30a6678770662042790d937e1adc8d79bf03e55546be'
            '04ca7ad8eddc69be7ce84a0ce0f0bbb3ce0aa09c21fe22fe46fb3cdff1aedceb'
            '1f220485f5cd55cc508103b2bd99115a7c51e206c31bb4bccee3e3c37e07bab8'
            'abc7fd5adef6b3596ef8d3ba1c0b50c9b2087ef9024f72ce8bebe7dc8d1c1251'
            'ab85ae0d9ac9bcee5152b7d7661493e8c9169461520f4315fe6bdfc70898e2b6'
            'ca3d0f6eef30943429d9d5f18c95dc6b267a66c11885675a3328b062edbb01d0'
            '04ef9ebd182ebca5772616f54fa75ae4f950e87daa09f123813b48703b50e255'
            '51f54d206829afdbdaa99e0196e89793fbeac13b375ddb2b4e8b5ff33df8c3b4'
            '244087eaef3497c4cf40a3fea226b4a2b137a75f137bd7c334011a580d1cbd09'
            'ad17ee41827ccf27554b7b02f9483d5e833d23e97a17a61889f54605e3b678db'
            '33facfa8e7588dac496e88536af6addc5f3d9106583b453a26c1044e22e3e79a'
            '437c0fc08ea15a0728896032f367eaf7ea2651ac98d48bf61a4943dde9617c47'
            '1ce0035170ced9c2a5501d9d12eaf967df25c114af109e98e1e878212a591f2d'
            'e32209582f28b2f66bb9176a9afc14fbd3775841613e2eb2aa27a044990a41cc'
            'dd7306851d947439c769dcc99bd463049c7973f1bc17b9fc5c0f767de5c45376'
            'be9910184380a18add89eba2da8836eb25bcb97201e1c1f0cd01f55d0501e205'
            'a6da49fd818f406ea5c3b88695d3f463e74052120d49a81ce2561dc5fbd1bd6f'
            '3670d866e70a37f09605ef233bef1ed5fe5e3a2b5d019a08aa131db8701122dd'
            'b1fbfacfecc5d7653fd2b4e24763859db082f2861590190e52c849a965dc588d'
            '1e1f5bc26508a4bd02c626dd64ee32c69fef40ac2a1ab5fcdd16dbc3a1081b69'
            'a5d7606bd3325c8f6eea889e12731335f4f07deed2c45d324b2a472db6a87194'
            '5a84af2999e86b74f8d36bd66d887f0a22fa1c1e029a092d6c6dd19bc137344e'
            'a7da398e31ca89144351b1107d1079e504ec244a44e305cd8a612a5d5afd4b8a'
            '7c1c647d0bf62faf83fba80710a040f112e61d846af4f703d061ba8278fda063')
