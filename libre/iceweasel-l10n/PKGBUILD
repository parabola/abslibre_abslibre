# Maintainer: André Silva <emulatorman@parabola.nu>
# Contributor: Márcio Silva <coadde@parabola.nu>
# Contributor: Luke Shumaker <lukeshu@sbcglobal.net>
# Contributor: fauno <fauno@kiwwwi.com.ar>
# Contributor: Figue <ffigue@gmail.com>

epoch=1
_pkgbase=iceweasel
pkgbase=$_pkgbase-l10n
_pkgver=50.1.0
pkgver=50.1.0.deb1
pkgrel=1
pkgdesc="Language pack for Debian ${_pkgbase^}."
arch=('any')
url="https://wiki.parabola.nu/$_pkgbase"
license=('MPL' 'GPL')
depends=("$_pkgbase=$epoch:$pkgver")
makedepends=('unzip' 'zip' 'mozilla-searchplugins')

_languages=(
  'ach    "Acholi"'
  'af     "Afrikaans"'
  'an     "Aragonese"'
  'ar     "Arabic"'
  'as     "Assamese"'
  'ast    "Asturian"'
  'az     "Azerbaijani"'
  'be     "Belarusian"'
  'bg     "Bulgarian"'
  'bn-BD  "Bengali (Bangladesh)"'
  'bn-IN  "Bengali (India)"'
  'br     "Breton"'
  'bs     "Bosnian"'
  'ca     "Catalan"'
  'cak    "Maya Kaqchikel"'
  'cs     "Czech"'
  'cy     "Welsh"'
  'da     "Danish"'
  'de     "German"'
  'dsb    "Lower Sorbian"'
  'el     "Greek"'
  'en-GB  "English (British)"'
  #'en-US  "English (US)"'
  'en-ZA  "English (South African)"'
  'eo     "Esperanto"'
  'es-AR  "Spanish (Argentina)"'
  'es-CL  "Spanish (Chile)"'
  'es-ES  "Spanish (Spain)"'
  'es-MX  "Spanish (Mexico)"'
  'et     "Estonian"'
  'eu     "Basque"'
  'fa     "Persian"'
  'ff     "Fulah"'
  'fi     "Finnish"'
  'fr     "French"'
  'fy-NL  "Frisian"'
  'ga-IE  "Irish"'
  'gd     "Gaelic (Scotland)"'
  'gl     "Galician"'
  'gn     "Guarani"'
  'gu-IN  "Gujarati (India)"'
  'he     "Hebrew"'
  'hi-IN  "Hindi (India)"'
  'hr     "Croatian"'
  'hsb    "Upper Sorbian"'
  'hu     "Hungarian"'
  'hy-AM  "Armenian"'
  'id     "Indonesian"'
  'is     "Icelandic"'
  'it     "Italian"'
  'ja     "Japanese"'
  'kk     "Kazakh"'
  'km     "Khmer"'
  'kn     "Kannada"'
  'ko     "Korean"'
  'lij    "Ligurian"'
  'lt     "Lithuanian"'
  'lv     "Latvian"'
  'mai    "Maithili"'
  'mk     "Macedonian"'
  'ml     "Malayalam"'
  'mr     "Marathi"'
  'ms     "Malay"'
  'nb-NO  "Norwegian (Bokmål)"'
  'nl     "Dutch"'
  'nn-NO  "Norwegian (Nynorsk)"'
  'or     "Oriya"'
  'pa-IN  "Punjabi (India)"'
  'pl     "Polish"'
  'pt-BR  "Portuguese (Brazilian)"'
  'pt-PT  "Portuguese (Portugal)"'
  'rm     "Romansh"'
  'ro     "Romanian"'
  'ru     "Russian"'
  'si     "Sinhala"'
  'sk     "Slovak"'
  'sl     "Slovenian"'
  'son    "Songhai"'
  'sq     "Albanian"'
  'sr     "Serbian"'
  'sv-SE  "Swedish"'
  'ta     "Tamil"'
  'te     "Telugu"'
  'th     "Thai"'
  'tr     "Turkish"'
  'uk     "Ukrainian"'
  'uz     "Uzbek"'
  'vi     "Vietnamese"'
  'xh     "Xhosa"'
  'zh-CN  "Chinese (Simplified)"'
  'zh-TW  "Chinese (Traditional)"'
)

pkgname=()
source=('brand.dtd' 'brand.properties' 'region.properties')
_url=https://ftp.mozilla.org/pub/mozilla.org/firefox/releases/$_pkgver/linux-i686/xpi

for _lang in "${_languages[@]}"; do
  _locale=${_lang%% *}
  _pkgname=$pkgbase-${_locale,,}

  pkgname+=($_pkgname)
  source+=("$pkgbase-$_pkgver-$_locale.xpi::$_url/$_locale.xpi")
  eval "package_$_pkgname() {
    _package $_lang
  }"
done

# Don't extract anything
noextract=(${source[@]%%::*})

_package() {
  pkgdesc="$2 language pack for Debian Iceweasel."
  replaces=(iceweasel-i18n-${1,,} iceweasel-libre-l10n-${1,,} firefox-i18n-${1,,})
  conflicts=(iceweasel-i18n-${1,,} iceweasel-libre-l10n-${1,,})

  unzip iceweasel-l10n-$_pkgver-$1.xpi -d $1
  rm -v iceweasel-l10n-$_pkgver-$1.xpi
  sed -i 's|Firefox|Iceweasel|g' $(grep -rlI 'Firefox' $1)
  install -vDm644 $srcdir/brand.dtd $1/browser/chrome/$1/locale/branding
  install -vDm644 $srcdir/brand.properties $1/browser/chrome/$1/locale/branding
  install -vDm644 $srcdir/region.properties $1/browser/chrome/$1/locale/browser-region
  sed -i -e 's/firefox/iceweasel/' $1/install.rdf
  sed -i 's|Iceweasel|Firefox|' $1/chrome/$1/locale/$1/global/aboutRights.dtd
  rm -rv $1/chrome/$1/locale/$1/global-platform/{mac,win}
  rm -rv $1/browser/chrome/$1/locale/browser/searchplugins
  cp -av /usr/lib/mozilla/searchplugins  $1/browser/chrome/$1/locale/browser

  cd $1
  zip -r langpack-$1@iceweasel.mozilla.org.xpi .
  mv -v langpack-$1@iceweasel.mozilla.org.xpi $srcdir
  cd ..
  rm -rv $1

  install -vDm644 langpack-$1@iceweasel.mozilla.org.xpi \
    "$pkgdir/usr/lib/iceweasel/browser/extensions/langpack-$1@iceweasel.mozilla.org.xpi"
}

sha256sums=('d319f07f17268240cdf0c5f996952f09fbfbdfb2905f9d7b1741a7a42b4d8085'
            '754ea5ea2fe184d3bc1b1bb60d4caf72cdaca5e4d8f16065b22b988b1ede9ad1'
            '81d1f98843f29a81c10a9a96655505c72ee34acee45225dcd307ae9a123e63d9'
            '83b9a4d0b464040fa701509a0fd31c8572e5dcf48dbd924944843ab35d0426d3'
            'b4cc49e81caa1fd4f599647a5f082c1932d5f5aa6f3f28da486dbac1d295c3f5'
            '03f42a5ab19f3319df5d6f3197824b76bcf78a2a41e90d0f1b5d6d78b4312f96'
            '731f4ef994c253afb829391fd6247f9e7707b55669f666d17ce682dd2e7693a5'
            '8bf940672fc703ed8809c97183ab77d98100f63175cc1d3e8c2518b54a87feb2'
            'd90e23f658721be12f455f5c3040e27964fd9dcfbb27ccf4b1439e2fc07370af'
            '9982da2a487d00ac5f813dfa939c717163eac7b4f3f9ee8f0eca379bcb188967'
            '6b6a5e639ad3f3f56c6a1cdeae911cd2bfaf1b2faa8f2422e4cbe6faf4e74d14'
            '210e90ae3c71f84d08e04d156403cb57a129ab36d0808aad1b3816c7fb8abdfe'
            '5c9e9d77efab7976809b1ce2dad9095115e5c7df43117d27c9283e0fb7cbb408'
            'aee48e7e89738aa1d070b6c3277b6d8aef8f29326162645eeb45984e89fe4797'
            '7a4cd30df0e11f97015c718748f7f11ffeeb2f742a22d4379f0584816d1e87e3'
            '0597b140517965a7d94eb4a3ed347d738fe9fff9d3b49caf623ffef005f20204'
            'ce222e3d7ddc0e7fee9553302511a02b5fd1278203d810bf5e5e39f58363cdd3'
            '92d253085138149feb921f55f334235b99402249cea5fa71cc2f7db9e44d071a'
            '95083fb779f833194b96d90858c90bb7de055bc41177c7adf590806d6c45ed55'
            'b3e4993445fc55fc2130af52aea5ffad42cb849d2f98afcb96fe4cba9ae811c4'
            '8d560412d4ea6fb7f5ef13459a98bab350dfce09a6e8669f8588a7bd02c773b8'
            '85b078a1224ee15e8a1b8bdfecb00fbf5688188c04cdea17186789218696e82d'
            'a6ca1ebc7c2bf85ca24cd19c84f18a8445027d483c859c1646d6e4a3fa1032ef'
            '24d89691b01f93218f67cd6aa0af9148367faa92fb31498fd5067dab4818a930'
            '54248475af0eb5020b147514ec805621c142a4bf062bdc34c867416bdf43619d'
            'f139d88b13e7f5e55216f05f9e8fa824c8dbf5e5a967a00655c1ca3766714623'
            '00fbf898d33fd23018e23bbfe4b7e2d057a4afdd6a0376bfaebeb9044db89cc3'
            '1b503bdfea75900631e63cff150ae44e97d014420d8cb2b3286c14c6070dea69'
            '15c79b2f402a8b8d3efbbf33753bac3901e4d31e91bbef95e6c58c5749dd38f9'
            '91f35c75797dfed7358460498bf342a8498873bcc4247f5fb428623caa90da3c'
            '7e695f6082241263bb83bff7f119cfef1f85b3facb2a3a23bffcf15dd1362204'
            'a6b3ae1f0b7cac833d08606224c064a5b996171ed953ad30d87dd3ff4b640d0e'
            '599835e211f9e5f451c9a538c94c29caf451c46de54ebd949549486cdca8c51f'
            '4f2e3a16b9f28c4d61720aa5913c28a93887ffed2ef077f1e332bcb2a824ec46'
            'a67037a6d7f4c190dc073bdf15471a01b581337fe457becee757674390e7a4fe'
            'dac5a419372a3da365f772b3e2ecd2acd278c2ebfdb05d701203c43384047b71'
            '18b683f313be7c58079868ea74793287ae4e90668818eb31ed07424c798a9735'
            '2c59c8294f33299f4d016db4bf325e68d1ede0ae6e72387ece95bdf3dd04c17d'
            'd17bea3cd65639b758e70bc037407fd4ebb5a119354fe2a6951bc493306975cc'
            'f5f2d15865fe289a2cf5a0e3248b1b11ee81477fbc7216203f2789d10da5f1c6'
            '43443460f92a397562e41d6d164fc6841c9090e6ad96c5af3eeb48d4730ab20e'
            'f69c5f8470334254ca9e9f7202b84e90c4af1986d759ce6da6c6758a0107ec42'
            'cd93e0c1d0b9c798d09a3f9cba9d83a896affd8d48eee6ed33da86c472455d3a'
            'f091c55a32ca6e6360a7a34d05184c0da111e374ead45de4902bdf3b8af44c7a'
            'ceda99abfaf12189795332ed871d84431458dc1378cc1b3492f301d853a5369a'
            '258f83dd5e575bac420f54defa9ec18bdeff73559c0e0b2f45f818c34b99de0e'
            '95bd0ace426e060957752e7baca61cdc3a9cd867f637de7b0875b69c5bde6818'
            '5a6a447494cfc93ed287c1f518ab1666c06a244f32e9cc494c1867634a5be46d'
            'f0c9b033e6ccbb70660aed1ecc80d06bd02b659ffa2e4ee53b51a489090a0908'
            'eded14951c477a8ebeada56c936e53d737e2907f1e4c5a46de0707c1dd7ca0c7'
            '57c13e9eff970ee4aa0eebf133334713921fff28c3272e9fe85e44b1eae35a2b'
            '23a9ac0d9e56634048eb7dc9ecf57b03e14e56013804481fb4df385798517975'
            'b61cd8d8c1e3add7351d6190a9924ac71357ea61d1ddaa39d28856c9b4cfa807'
            '2dc12c8e107afe86f34dcb18b0a397f2745053e38678abf5e338eaecff1d5357'
            '1bc4de6e9963dcc3d082656fa0a9fad68c9ee662ff1e2f4adb53f6f9751fd160'
            'a0a19c5f65efe0f0102fe236d01e02c6e0cd23dc051002442435d87e05910475'
            '16265343fa9eba15d14fa28ee18d3f30c9d93745070c90f0cabecc5f33155c32'
            '2158be0d8634ee90af494896e9f7be41db0a8f7357ce641bd6459f92251a06b1'
            '034831f5099c415b20837c8955b5a7a6c9bbf7603611b7b39a3258c488b073ed'
            '7133e9c0102fdef8598092505bcd3b69633ce3388d701146abed2897c844a973'
            '18630404bdf62fd1066736ea6af508bbed20894531a80b974cfb0db32baea9e0'
            '8025b0f8eb48729afbf5bb20e6893a155b6cfb43814580b186c18cecbe2cb256'
            'dc8633476180c413ec817e5237ba892c92eacf38a03dee3a00d3d896c52ee54a'
            '96443d6ca0af7ccf849f02c8ed2feca741e07d672e4c72791d6c729b829e5d73'
            '5024e38dc2875aad4348168aa6a1cd9e0034965242cdc2d88945507ea3f66411'
            '2b2e032e96e3fd50e0e18d6489391b3ad4b4bf290b615f2993583462b3eb0d7e'
            'd99d8a348b6acca99396ff3adead4d73365aeedc620477368cdd4eaf94084dbe'
            '3e948361393022bfbdc2471f1f52ae9612bfcc7421bde363b9f2cc92ceb12832'
            'a4371b641228a4c59e1df7eb1b20615d033bf6f1e7d327a275a0c0d2a4eec24f'
            'f2739db2080c82534a8183a2462e82957730b0529e4f44c625f3412b15dc6d37'
            '319a086b7440c62ce5c10e4c61bd7dcc7955580b71984ba95ef10142f3fe96a1'
            'dcb3cc50a6ec033beefc4cdfebefc9e10d9985e2dae9d2d2e7a75df4043e2ebe'
            '4c35b31a40cd1118dd619386c6960b6a3ec2eff5442a486c4747a9e9c082417b'
            'c25b150e435460a0bf80853abe5389aa26493dc05b3eff1db82460d175edd810'
            '9911f11737e690d0415a38013eff67d4282a633e3142c4bdb50f23d311155bb2'
            'd973413d77bfd8a1f98890ee799513ff5f2142440faa2cc43a9d1a11887bbaf2'
            '2c6c83e8af1a1989a73b0894549057d470bb78e6d7a6688302397637b33a78dc'
            '1ee595eaee68074398b4e49dc044cc29019778982f8fbd51df29d810f4ef55fe'
            '796969d3159148c2296955319ea03db0a2e3f129413c89d94e249c61e31575cf'
            '3adb7db131a89597ac38b36a55c51cabfc13a1158cafd6a5af6a0c9d9b539e33'
            '6a5641e47eb8746be82e96060e2aa44335013465b762ed3b96b4f3c06fb92890'
            'ca9826e23b26d4d180bf2bd9f9ee6c7865ae664ad442d8a6fbd761021ec6b08a'
            'fab68bbe7cd7d7179d68bd5a99312a9ea970c7300d45061eab2d2d10fc9b88a8'
            '7c0ea613ddc14ad0f6fbc2c2181f5aed2ca537dac8b96fdba8a9063f0937e47c'
            '1131e25308b9d30ae9fdc1bf3d0d84d4fa1f06f414b0d290a45392c57c8a8a23'
            '795153523a8bf89b16f4e484a8c37e6a3cdc9190dd529b34981a6651400bf6ec'
            '280084081487f18ee2c734e2c17af2ae7566284688501bb797ebcc8e1c7e4633'
            '41b41cb66a37480ae2674b9ab6e3b1e911804ad82423365a8c15c23bb6400c18'
            '408816328660a0089dd5d336929cef5a4a7dcafee5c2153f3f780631a051e317'
            '11c241bc767e00cfb3f7196004d69b121362533f1e30eadc8c3833b3aeebd5d4'
            '3cb09d72d68de9946711ae2174272720b188068fdac8c0b31fa9c2a74d3dc139'
            '3fad24b5d6841669c0c0e79a5770cc475a3618e5d19e22c9562a7ee614595bde'
            '7732340e82d5ca0a7f047b0dfc99e6d928cc79e4d56b1dec3dbe1e9584396eb0')
sha256sums=('d319f07f17268240cdf0c5f996952f09fbfbdfb2905f9d7b1741a7a42b4d8085'
            '754ea5ea2fe184d3bc1b1bb60d4caf72cdaca5e4d8f16065b22b988b1ede9ad1'
            '81d1f98843f29a81c10a9a96655505c72ee34acee45225dcd307ae9a123e63d9'
            'db54b84c0c15fcaba27e2e124049c3f24efa4367fb6ed97daf34a62ad567ce73'
            '1f90832b46c293e69e798a67895b9aba430b869c0e3184a0d611cb06bcb415da'
            '84a0200f752ae65e0bc08d0cc4a351c69e20d2a513199eb32ace260f078b0266'
            '6d3f8fff35b26a9b5dd86607ab3e504b2796ae5af6e07b6d327901803c1cb94b'
            '4f81c6948c19a3c0c0278d0ee0604b84382e013c84cbd729b8bb534ef8d885a9'
            '81918465211a3d0ce575e7d7ef10735a8b7c45738bdf1e2478720c0e5d633709'
            '0c21e6def5113289295652f67f95ea4a3f5be885d781eaac94a0f0ac05111fd2'
            '65d6fe396efc6b95dd90bc7fff7061d8643c64fa20180796cc98840ecbf07e4d'
            '53623d2bfd9907697b5d7b0e747fb0eecbbea714967c9d36571ac62514a7c6b4'
            '5768972ee398ed19f3eb254d26598ebd327a340d26ac4cb329e2a990f99edab8'
            '18ccacde99791e10583c15976b29feb8520cec616896ec2cef3c667a17819def'
            '166d2077653f00163accb1d56ad56f76528c874dac47f0b6ed9f2df27300eb0c'
            '5d9823adc07551066d342a3652a8861d5194a4f6cf3d5c19aff25e095cbcbf71'
            'bde4ce518973aa378f806f425da45b14fa916e19c9246a53c3655c1b6d7f55c8'
            'd6a08d4a29114d586bed33c899954863d9980d204549956ed7fd11938a3dd56f'
            '72dfa15075f3a37114a143f0b2bb02e0f0f70a35a119776713d673bee92881da'
            'a78071188fa1f9f561c52f970cb0412826db62e67bbcb827ba64aaa23d1c94c0'
            '76dc2bca941cb71e4f2233f91632feeb438cdc82c2c23d06c08e28d300f53552'
            'bcd98186dc9e9f19c56ae926f4177d52bb70f0510b0eeddb518fa217a98ac630'
            'bfb9285909e173591750d69ccbe7249b4daa252c6f28462ab04ccdfc8fe2bcc6'
            '146fc2a2bb1a990aeb794fc81a7e952b0d83a8ac15aeb8b508688264b221b751'
            '3187df8d2c923979e9c67ce7658111094a9f3f1eb15aa8f270b0f76cdbf9d2ba'
            '9ab6b4f4fe4df033a95ebb597f97a6ef99517aea895b7df8411a34d72cf9c472'
            '2435e4cb414f2c24cafbf3304530696282bce48cc05e394d60053c0d5277a630'
            'cb71ab12ca44e1bb37d12cc5823068e9ffcf8235e746f72938bd49afd141fc36'
            '8ab50e87e703c5bbc6b8e67a44e660d87cca1227198bb8beb8fd02460eade51a'
            '029956fbed461784de92ec1a0d3fc6ceff52e5ab46158b20369c74f52b254d80'
            '93b072b9658b9dc6c4d88d8b2b9328ee83f9857d29b578531a94bdbfaff8338f'
            'dc5ca066b594e206f0afc598e87129deafcb559363736b7d71f6aa10ad00bd45'
            'eaa622437e55deda535ffe4974a8a09b9567c6a14fc648f1f04e60e9694c0c71'
            'ff8f7077f5eda083624a95405890dc569ea0a9eee12f36b144bfc639715229d4'
            '202821f043547fc43a75ed60de9068b285cb93461e18ec6265829e92a17886cf'
            '9f9c4ea0b9de6a7ae0db1c426d56e3c79c1bfc4087eed78c05db9b083344c7ed'
            '5cd3e02bfae67736c9232c7755c61dd0c6b89824f7b1fa83e5af802e6e87c775'
            '44ddd7d338d386e4adfdf4894d081b3854684280afeb76846f78d5f183e2d505'
            'fddb1d88ed94575c80f8a7bbe9f21dc0fb49e409cc08fbb2ee32bef78e21ae86'
            '33a7c184e2877da1bdf45d5a92b950a4bbc753471f88ea90e7c3585cb2043c32'
            '18c9b30f1de7d04c4675145f05b6e96b18ac0c8f4a26fa7c0ce37f912bc9acb7'
            '2eb03c758f4ade230d611a4f1a965209642429f82d233381bde0a5e2cf43aadb'
            'c1c647eb01b7a7fa71e9fc99750f8a3b23bd5b7d7623d05a175f7eebedb2b5d6'
            'f3734156473a998d51f421b206179a4e78b3fb589e1591a3f85600721bf72876'
            '16e8f425db1d3d08293cc6e92c845c05990e8a89607609362d6371360971a837'
            'b16d41138602b16dc38f669312d04e95b90c249dc6eb7066a421c5bffe43bc28'
            '91e3b5846538e39bbcc314e72adc338297decfa25aeffde8d53eb0c6cbf251a5'
            '6978364271143ab41996efc78f520f3bb687af52267fe01cfb48ec23febb3c26'
            '6a94b0973613dfca2c230713312221d92a3627c7f2128cee4ec81d71b24b416e'
            'd90e4b4ede96c33671e283eb1192038f9dbd7bb2d71d53a2353a3add338e3ea0'
            '3e85b0206e8538b45d4d6548e053e367524d23b336b0be214971531be5cd3c29'
            '37ebf392af8f59e8fd2e241c10d01b61976c52af6909c8a012a40813f2a23647'
            'a1675efea45733c61418a943d8b19db9d5c780a29d81c905618ee06993a87394'
            '020411b4f57db00413cdc648378bf48e6f28b0ff783888b47b9801b31f360363'
            '5f75fba1b2b1094de89b0d050300abbdd8c425718840cab353e01b7515f4cf3a'
            '70ab37759463b4fc11bc78db7c6f1208ddcb711bbefdf1bfae6f1d53c4538b9f'
            '5e5e6631db6d3115578c240016dddb884543ef196647b04d62b0ce1c27c0a38a'
            '8857040d7ad2b7b31bfe1262939e238467fbe0e7fdb9fae3077991b10e847d5d'
            'cd7f0dd21c9316ef4cd028ad213f006b8b065416dac7c25fcf3f70ebf783b96e'
            '499f7870b5436d4d676c5b0237a8e5f3a36dcc7e33e1202e0f756353cf147d64'
            '81b3e68c6b540fdf337fdbcd7a92e813d5d2de67b70696c188b7367dd897eced'
            'ef03c0ce5005e6973e7c6ae52431c9af8493f809999a76d49df21a8844e56608'
            '8ca7aad3bfe7247108b6fefd3eb498e9d20e45bcec74d546cbd77ceee04a9d9d'
            'b349d5ce15844c4a0081396f439f3df9864220c53c89780c94f6ace62c432cce'
            '2150f76796e717f1c5205f75f0486d7b35c318b1d5a5145777286e4631098257'
            'e01046e7f9f3be466e51314faedf31e4f0132f66568f3a1c4017b2b827c91070'
            'e29c868af5d5d218dcf36969e118fc6d8d9889f6562e1fab493b3a0e79b896db'
            '0e62ebc13749cda45cb7c69d5eb68ea5a8e5691559e4d427fc7dc0c949f44be4'
            '3e81619b1ddaa43d21d6cc971965fa675a26181e8f62ed21b18b1791894757d6'
            '085dddc431a6b510e7dc409217e9958adc148c1e19adb47d42fc5579e7b9f120'
            '42dd3339773d32c95a03e9fc5b3816f5433c1c1c3e63aac5e345376f86abaef2'
            'a33ef5db5f8b0cd4d601929d91e8c88b1c6397e9afa155b08eaf20ab78f3953e'
            'c2ddec6e2ed7a1e74fcefa5d0887e99a3015888d54b783d9ed1577cac98b8a4d'
            '0bfd6923f26daed995bf6484a9174add457851177c55b7cc7890df1ab6639084'
            '273e122349d4b003de3de2d0fafbb22e1cf498717f368ffddf1d0993b7b73a87'
            '789e045aec63b3be715b3405369676661c1f3b61aca4089e34d31aa8b659755c'
            'b1e1f3571b16b6094d227bc2ddc55788effbc274918da915f4a17962c97788af'
            'f2c93b899bbfce32102f2eddc35744ee98528384ef1f1a269577e44e0a644c98'
            '19dc731ddd193e207b022607381e68004abc886464d849c50755ea4d31e61df0'
            '1da8f877c0d00333dfdcb21a9115c04f233986ca987ba5739b91ad7877b33d59'
            '041e88f2d34a715225ebc6ec78ae75cbca3c8a97221c1f6a64e3f06fbdc8dde1'
            '7b1ef7b953890ba61c58e57e6149fafea873e39167961831a61599d7be4d39cf'
            'f50f74e78b3d1424aec6396747c66413713bbb82aa26eed37abdfa7b228ca6ec'
            '434425518bba81a965e0771b7c379e32fb0688eb0aeb1302d74823ee5515d9f5'
            '5ede0cec24269fad84f1c8eec17f3da5be59e452f470e13926ddf351fcc5e11b'
            'd6918d086f2e461b50d56f4dec9e98de9f8cd8bb75b84d108eee8020406c2893'
            'f667b82e5893c63a893aa1daede1bbab3468c0cebc35dd64f7f689c85f235b35'
            'ef2a937b6c9015b7b46dc8837aaa4011c83dbeacdfa2a11820d7efe2301e7d5c'
            '22da17f8404fe6a98d58812e85eb70a5c7de898ab0f3f3598b3fc6a0adecc8ed'
            '5fb86b8039a3ec5f5d29887dbbcf6ddd843c6818ea28ce422ac839ac82098b82'
            '5dee657d054ec57f480523ce6ee6b02e63b4173edda319cafc0ed246d9368a52'
            '258db702f1ada9105bcd6b11e8ddf8197246dff9bc18ee2ab0eb9e4b5ea3f6b2'
            'ce6729051516187b63af735a0470a4904183499f03dddd849b314a94d4629bcb')
