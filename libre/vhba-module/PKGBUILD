# Maintainer (Arch): Ray Rashif <schiv@archlinux.org>
# Contributor (Arch): Mateusz Herych <heniekk@gmail.com>
# Contributor (Arch): Charles Lindsay <charles@chaoslizard.org>
# Maintainer: André Silva <emulatorman@parabola.nu>
# Contributor: Luke Shumaker <lukeshu@sbcglobal.net>
# Contributor: Márcio Silva <coadde@parabola.nu>

_kernelname=

if [[ ${_kernelname} == "" ]]; then
  _basekernel=4.9
  _archrel=4
  _parabolarel=1
elif [[ ${_kernelname} == -lts ]]; then
  _basekernel=4.4
  _archrel=1
  _parabolarel=1
fi

_pkgname=vhba-module
pkgname=${_pkgname}${_kernelname}
pkgver=20161009
pkgrel=${_archrel}.parabola${_parabolarel}.basekernel${_basekernel}
_extramodules=extramodules-${_basekernel}${_kernelname}
pkgdesc="Kernel module that emulates SCSI devices (built for the linux-libre${_kernelname} kernel package)"
arch=('i686' 'x86_64' 'armv7h')
url="http://cdemu.sourceforge.net/"
license=('GPL')

# Generic (you shouldn't have to modify any of these variables)
_toohigh=$(IFS=. read a b <<<$_basekernel; echo $a.$((b+1)))
depends=("linux-libre${_kernelname}>=${_basekernel}" "linux-libre${_kernelname}<${_toohigh}")
makedepends=("linux-libre${_kernelname}-headers>=${_basekernel}" "linux-libre${_kernelname}-headers<${_toohigh}")
makedepends+=('libelf')

replaces=("${_pkgname}-libre${_kernelname}" "${_pkgname}-parabola${_kernelname}")
conflicts=("${_pkgname}-libre${_kernelname}" "${_pkgname}-parabola${_kernelname}")
if [[ ${_kernelname} != "" ]]; then
  provides=("${_pkgname}")
fi

options=(!makeflags)
install='kmod.install'
source=("http://downloads.sourceforge.net/cdemu/$_pkgname-$pkgver.tar.bz2"
        '60-vhba.rules')
md5sums=('9a9dc156e4cb73d0ca5892eeba801026'
         '4dc37dc348b5a2c83585829bde790dcc')

build() {
  cd "${srcdir}/${_pkgname}-${pkgver}"

  _kernver="$(cat /usr/lib/modules/${_extramodules}/version)"

  make KDIR=/usr/lib/modules/$_kernver/build
}

package() {
  cd "${srcdir}/${_pkgname}-${pkgver}"

  # Actually install
  install -Dm644 vhba.ko "$pkgdir/usr/lib/modules/$_extramodules/vhba.ko"
  install -Dm644 ../60-vhba.rules "$pkgdir/usr/lib/udev/rules.d/60-vhba.rules"

  # Set the correct extramodules directory for install
  cp -f "${startdir}/${install}" "${startdir}/${install}.pkg"
  true && install=${install}.pkg
  sed -i "s/^_EXTRAMODULES=.*/_EXTRAMODULES="${_extramodules}"/" "${startdir}/${install}"
}

# vim:set ts=2 sw=2 et:
