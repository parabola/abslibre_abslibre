# Maintainer (Arch): Lukas Fleischer <archlinux at cryptocrack dot de>
# Contributor (Arch): Stefan Husmann <stefan-husmann@t-online.de>
# Contributor (Arch): Simon Lipp <sloonz+aur@gmail.com>
# Maintainer: Omar Vega Ramos <ovruni@gnu.org.pe>

_pkgname=commons-io
pkgname=java-${_pkgname}
pkgver=2.5
pkgrel=1.parabola1
pkgdesc="IO related classes for Java."
arch=('any')
url="https://commons.apache.org/io/"
license=('APACHE')
depends=('java-runtime')
makedepends=('apache-ant' 'jh')
source=("https://archive.apache.org/dist/commons/io/source/${_pkgname}-${pkgver}-src.tar.gz")
sha256sums=('631f75ea09870b40bb140dff456a36be6965619ba51fa23f4c7d4b966520be60')

build() {
  cd "${srcdir}/${_pkgname}-${pkgver}-src"
  ant jar javadoc -Dcomponent.version=${pkgver}
}

package() {
  cd "${srcdir}/${_pkgname}-${pkgver}-src"

  # Install license file
  install -Dm644 LICENSE.txt "${pkgdir}/usr/share/licenses/${pkgname}/LICENSE.txt"

  # Install documentation
  install -d "${pkgdir}/usr/share/doc/${pkgname}"
  cp -r "target/apidocs" "${pkgdir}/usr/share/doc/${pkgname}"

  # Install Maven artifacts
  export DESTDIR=${pkgdir}
  jh mvn-install ${_pkgname} ${_pkgname} ${pkgver} \
    pom.xml \
    "target/${_pkgname}-${pkgver}.jar" \
    "${_pkgname}-${pkgver}.jar"
  ln -s "/usr/share/java/${_pkgname}-${pkgver}.jar" \
    "${pkgdir}/usr/share/java/${_pkgname}.jar"
}
