# $Id: PKGBUILD 267688 2016-05-10 21:47:55Z arojas $
# Maintainer (Arch): Eric Bélanger <eric@archlinux.org>
# Maintainer (ConnochaetOS): Henry Jensen <hjensen@connochaetos.org>
# Maintainer: André Silva <emulatorman@parabola.nu>

pkgname=xchat
pkgver=2.8.8
pkgrel=19.parabola1
pkgdesc="A GTK+ based IRC client, without nonfree browser reference"
arch=('i686' 'x86_64' 'armv7h')
url="http://www.xchat.org/"
replaces=('xchat-libre')
conflicts=('xchat-libre')
license=('GPL')
depends=('gtk2' 'openssl' 'dbus-glib' 'libnotify')
makedepends=('tcl' 'perl' 'python2' 'librsvg')
optdepends=('enchant: for spell checking support'
            'perl: for perl plugin'
            'tcl: for tcl plugin'
            'python2: for python plugin')
source=(http://www.xchat.org/files/source/${pkgver%.*}/${pkgname}-${pkgver}.tar.xz
        http://xchat.org/files/icons/xchat-svg.tar.bz2
        xchat-2.8.8-libnotify07.patch
        xchat-2.8.8-link-against-libnotify.patch
        xchat-2.8.8-glib-2.31.patch
        xchat-sslv23.patch
        remove-non-free-suggestion.patch
        no-firefox.patch)
sha1sums=('e12305da42d1aacc26c2ca25e239f393d4dd3532'
          '0d366346cc11e0efb57fc2648fe423c94a3469bd'
          'a053fba4e1911d1ee6a8248fe19e344797920fe3'
          '70c3cc29fc55ff35f701ef8ac23078b6e3761ce1'
          'aecaf6176a7cfd62555207b02f2793b360aa39da'
          'ff2a3ca5ac297071c8a084d90fe9562e14745bc1'
          'e6a3b69ba333fc7ba0f70792d78d44fba7c4a911'
          '20aef0ae608af6b30e1b4249f9c53830f8f514f6')

prepare() {
  cd ${pkgname}-${pkgver}
  sed -i 's/GDK_HAND1/GDK_HAND2/' src/fe-gtk/xtext.c
  patch -p1 -i "${srcdir}/xchat-2.8.8-libnotify07.patch"
  patch -p1 -i "${srcdir}/xchat-2.8.8-link-against-libnotify.patch"
  patch -p1 -i "${srcdir}/xchat-2.8.8-glib-2.31.patch"
  patch -p1 -i "${srcdir}/xchat-sslv23.patch"
  patch -p1 -i "${srcdir}/remove-non-free-suggestion.patch"
  patch -p1 -i "${srcdir}/no-firefox.patch"
}

build() {
  cd ${pkgname}-${pkgver}
  autoconf
  autoheader

  LIBS+="-lgmodule-2.0" ./configure --prefix=/usr --sysconfdir=/etc --datadir=/usr/share \
    --enable-openssl --enable-dbus --disable-textfe --enable-ipv6 --enable-shm --enable-spell=static
  sed -i -e 's/ -shared / -Wl,-O1,--as-needed\0/g' libtool
  make
}

package() {
  cd ${pkgname}-${pkgver}
  make DESTDIR="${pkgdir}" install

  for i in 24 32 48 64 128 256 ; do
    install -d "${pkgdir}/usr/share/icons/hicolor/${i}x${i}/apps"
    rsvg-convert -w $i -h $i -o "${pkgdir}/usr/share/icons/hicolor/${i}x${i}/apps/xchat.png" ../xchat-cloned.svg
  done
  install -D -m644 ../xchat-cloned.svg "${pkgdir}/usr/share/icons/hicolor/scalable/apps/xchat.svg"
  install -D -m644 ../xchat-used.svg "${pkgdir}/usr/share/icons/hicolor/scalable/apps/xchat2.svg"
}
