# vim:set ts=2 sw=2 et:
# $Id: PKGBUILD 196959 2016-11-26 19:50:59Z idevolder $
# Maintainer (Arch): Sergej Pupykin <pupykin.s+arch@gmail.com>
# Maintainer (Arch): BlackIkeEagle < ike DOT devolder AT gmail DOT com >
# Contributor (Arch): Brad Fanella <bradfanella@archlinux.us>
# Contributor (Arch): [vEX] <niechift.dot.vex.at.gmail.dot.com>
# Contributor (Arch): Zeqadious <zeqadious.at.gmail.dot.com>
# Contributor (Arch): BlackIkeEagle < ike DOT devolder AT gmail DOT com >
# Contributor (Arch): Bartłomiej Piotrowski <bpiotrowski@archlinux.org>
# Contributor (Arch): Maxime Gauduin <alucryd@gmail.com>
# Maintainer: André Silva <emulatorman@parabola.nu>
# Contributor: Isaac David <isacdaavid () isacdaavid!info>

_pkgbase=kodi-libre
pkgbase=kodi
pkgname=kodi
pkgver=16.1
_codename=Jarvis
pkgrel=5.parabola1
arch=('i686' 'x86_64' 'armv7h')
url="http://kodi.tv"
license=('GPL2')

# Attention!
# Don't repeat libvdpau in makedepends=(), it will break the armv7h build
makedepends_i686=('libvdpau')
makedepends_x86_64=('libvdpau')
makedepends=(
  'afpfs-ng' 'bluez-libs' 'boost' 'cmake' 'curl' 'cwiid' 'doxygen' 'git' 'glew'
  'gperf' 'hicolor-icon-theme' 'jasper' 'java-runtime' 'libaacs' 'libass'
  'libbluray' 'libcdio' 'libcec' 'libgl' 'libmariadbclient' 'libmicrohttpd'
  'libmodplug' 'libmpeg2' 'libnfs' 'libplist' 'libpulse' 'libssh' 'libva'
  'libxrandr' 'libxslt' 'lzo' 'mesa' 'nasm' 'nss-mdns'
  'python2-pillow' 'python2-pybluez' 'python2-simplejson' 'rtmpdump' 'sdl2'
  'sdl_image' 'shairplay' 'smbclient' 'swig' 'taglib' 'tinyxml' 'unzip' 'upower'
  'yajl' 'zip' 'mesa' 'dcadec' 'libcrossguid'
)
mksource=(
  "$pkgname-$pkgver-$_codename.tar.gz::https://github.com/xbmc/xbmc/archive/$pkgver-$_codename.tar.gz"
)
source=(
  "https://repo.parabola.nu/other/$_pkgbase/$_pkgbase-$pkgver-$_codename.tar.gz"
  '9703.patch'
  '10160.patch'
  '10775.patch'
  'libre.patch'
)
mksha256sums=('7d82c8aff2715c83deecdf10c566e26105bec0473af530a1356d4c747ebdfd10')
sha256sums=('68a6713567a6f65add13c6295d54709457e4c31e70d70cfc9c81ad64300405eb'
            'b0fe75d10b2678894d1dec48f3258c0bec2a4a170f33d76a9a8334bb1969b18f'
            '870037ddedc09f161db16df8550e1b0eaaa67a8bdfe47e7151bc9ee25c9bfead'
            '75529f0f42e9f84d2b55c86ba48d89f5e1286d84365f3ba6676c5b41fba6a314'
            '3c9b4e647a1eea8d910e7ddc4df3125de05a4c0468c3350fe4e44208158d8822')

mksource() {
  cd "$srcdir/xbmc-$pkgver-$_codename"

  msg2 "remove nonfree unRAR utility files from the source"
  rm -rv lib/UnrarXLib
}

prepare() {
  cd "$srcdir/xbmc-$pkgver-$_codename"

  # patches
  patch -p1 -i "$srcdir/9703.patch"
  patch -p1 -i "$srcdir/10160.patch"
  patch -p1 -i "$srcdir/10775.patch"

  find -type f -name *.py -exec sed 's|^#!.*python$|#!/usr/bin/python2|' -i "{}" +
  sed 's|^#!.*python$|#!/usr/bin/python2|' -i tools/depends/native/rpl-native/rpl
	sed 's/python/python2/' -i tools/Linux/kodi.sh.in
  sed 's/shell python/shell python2/' -i tools/EventClients/Makefile.in

  # patches
  msg2 "Use addons.xml from Parabola server (without nonfree addons support) and remove nonfree stuff references"
  rm -v xbmc/filesystem/Rar{Directory,File,Manager}.{cpp,h}
  patch -Np1 -i ../libre.patch
}

build() {
  cd "$srcdir/xbmc-$pkgver-$_codename"

	# Bootstrapping
  MAKEFLAGS=-j1 ./bootstrap

  #./configure --help
  #return 1

  # Configuring XBMC
  export PYTHON_VERSION=2  # external python v2
  [ "$CARCH" = "armv7h" ] && local configure_extra="--disable-vaapi"
  ./configure --prefix=/usr --exec-prefix=/usr \
    --disable-debug \
    --enable-optimizations \
    --enable-libbluray \
    --enable-shared-libraries \
    --with-lirc-device=/run/lirc/lircd \
    --disable-non-free \
    $configure_extra \
    ac_cv_type__Bool=yes

  # Now (finally) build
  make
}

package_kodi() {
  pkgdesc="A software media player and entertainment hub for digital media, without nonfree decompression engine for RAR archives and support for nonfree addons"

  # depends expected for kodi plugins:
  # 'python2-pillow' 'python2-pybluez' 'python2-simplejson'
  # depends expeced in FEH.py
  # 'mesa-demos' 'xorg-xdpyinfo'
  depends=(
    'python2-pillow' 'python2-pybluez' 'python2-simplejson'
    'mesa-demos' 'xorg-xdpyinfo'
    'bluez-libs' 'fribidi' 'glew' 'hicolor-icon-theme' 'libass' 'libcdio'
    'libjpeg-turbo' 'libmariadbclient' 'libmicrohttpd' 'libpulse' 'libssh'
    'libva' 'libxrandr' 'libxslt' 'lzo' 'sdl2' 'smbclient' 'taglib' 'tinyxml'
    'yajl' 'mesa' 'dcadec' 'desktop-file-utils'
  )
  optdepends=(
    'afpfs-ng: Apple shares support'
    'bluez: Blutooth support'
    'libnfs: NFS shares support'
    'libplist: AirPlay support'
    'libcec: Pulse-Eight USB-CEC adapter support'
    'lirc: Remote controller support'
    'pulseaudio: PulseAudio support'
    'shairplay: AirPlay support'
    'udisks: Automount external drives'
    'unzip: Archives support'
    'upower: Display battery level'
    'lsb-release: log distro information in crashlog'
  )
  provides=('xbmc' 'xbmc-lts')
  conflicts=('xbmc' 'xbmc-lts')
  replaces=('xbmc' 'xbmc-lts')

  cd "$srcdir/xbmc-$pkgver-$_codename"
  # Running make install
  make DESTDIR="$pkgdir" install

  # We will no longer support the xbmc name
  rm "$pkgdir/usr/share/xsessions/xbmc.desktop"
  rm "$pkgdir/usr/bin/"xbmc{,-standalone}
  # we will leave /usr/{include,lib,share}/xbmc for now

  # Licenses
	install -dm755 ${pkgdir}/usr/share/licenses/${pkgname}
  for licensef in LICENSE.GPL copying.txt; do
		mv ${pkgdir}/usr/share/doc/kodi/${licensef} \
			${pkgdir}/usr/share/licenses/${pkgname}
  done
}
