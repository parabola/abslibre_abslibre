# $Id: PKGBUILD 284915 2016-12-28 04:22:48Z heftig $
# Maintainer (Arch): Jan de Groot <jgc@archlinux.org>
# Maintainer: André Silva <emulatorman@parabola.nu>
# Contributor: Márcio Silva <coadde@parabola.nu>

pkgname=epiphany
pkgver=3.22.4
pkgrel=1.parabola1
pkgdesc="A GNOME web browser based on the WebKit rendering engine, with DuckDuckGo HTML support"
url="https://wiki.gnome.org/Apps/Web"
arch=(i686 x86_64 armv7h)
license=(GPL)
depends=(webkit2gtk gcr gnome-desktop)
makedepends=(intltool itstool docbook-xml startup-notification
             gobject-introspection yelp-tools autoconf-archive appstream-glib git)
groups=(gnome)
replaces=(${pkgname}-libre)
conflicts=(${pkgname}-libre)
_commit=516d6e584bf93569b13657f45c8fc8f2bc56b44f  # tags/3.22.4^0
source=("git+https://git.gnome.org/browse/epiphany#commit=$_commit"
        "git+https://git.gnome.org/browse/libgd"
        pluginsdir.diff)
sha256sums=('SKIP'
            'SKIP'
            '42a7b820fd659fee8508a9b9a57e385c658b3b312c7a8f9456078223e4d78f22')

pkgver() {
  cd $pkgname
  git describe --tags | sed 's/-/+/g'
}

prepare() {
  cd $pkgname
  patch -Np1 -i ../pluginsdir.diff

  git submodule init
  git config --local submodule.libgd.url "$srcdir/libgd"
  git submodule update

  NOCONFIGURE=1 ./autogen.sh

  # Replace DuckDuckGo to DuckDuckGo HTML
  sed -i 's|duckduckgo[.]com|duckduckgo.com/html|g' $(grep -rlI 'duckduckgo[.]com')

  # Replace Google support to DuckDuckGo HTML
  sed -i 's|http://www[.]google[.]com/search?q=%s[&]ie=UTF-8[&]oe=UTF-8|https://duckduckgo.com/html/?q=search|g' $(grep -rlI 'http://www[.]google[.]com/search?q=%s&ie=UTF-8&oe=UTF-8')
  sed -i '\|a quoted string should be searched|d' $(grep -rlI 'a quoted string should be searched')
  sed -i 's|http://www[.]google[.]com/search?q=%s[&]amp;ie=UTF-8[&]amp;oe=UTF-8|https://duckduckgo.com/html/?q=%s|g' $(grep -rlI 'http://www[.]google[.]com/search')
  sed -i 's|http://www[.]google[.]com|https://duckduckgo.com/html|g' $(grep -rlI 'google[.]com')
  sed -i 's|http://google[.]com|https://duckduckgo.com/html|g' $(grep -rlI 'google[.]com')
  sed -i 's|mail[.]google[.]com|mail.com|g' $(grep -rlI 'mail[.]google[.]com')
  sed -i 's|google[.]com|duckduckgo.com/html|g' $(grep -rlI 'google[.]com')
}

build() {
  cd $pkgname
  ./configure --prefix=/usr --sysconfdir=/etc \
      --localstatedir=/var --libexecdir=/usr/lib/$pkgname

  # https://bugzilla.gnome.org/show_bug.cgi?id=655517
  sed -i -e 's/ -shared / -Wl,-O1,--as-needed\0/g' libtool

  make
}

package() {
  cd $pkgname
  make DESTDIR="$pkgdir" install
}
