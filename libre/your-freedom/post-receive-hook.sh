#!/bin/bash

# Get this first thing
newpkgver_date=$(LC_ALL=C date -u +%Y%m%d)

. "$(librelib messages)"
setup_traps
lock 9 "${0}.lock" "Waiting for previous run of %q to finish" "$0"

. "$(librelib conf)"
load_files libretools
check_vars libretools WORKDIR ABSLIBRERECV ABSLIBRESEND

# Start in the blacklist.git directory
newgitver=$(git log -n1 --format='%H' master -- blacklist.txt)

# Get the ABSLibre tree
gitget -f -p "$ABSLIBRESEND" checkout "$ABSLIBRERECV" "$WORKDIR/abslibre"
cd "$WORKDIR/abslibre/libre/your-freedom"

# Figure out info about the last version of your-freedom
oldgitver=$(sed -n 's/^_gitver=//p' PKGBUILD)
oldpkgver=$(sed -n 's/^pkgver=//p' PKGBUILD)
oldpkgver_date=${oldpkgver%%.*}
oldpkgver_rel=${oldpkgver#${oldpkgver_date}}; oldpkgver_rel=${oldpkgver_rel#.} oldpkgver_rel=${oldpkgver_rel:-0}

# Make sure we actually have changes
if [[ "$newgitver" == "$oldgitver" ]]; then
	msg 'blacklist.txt has not changed, nothing to do'
	exit 0
fi

# Handle doing multiple versions in the same day
if [[ "$newpkgver_date" == "$oldpkgver_date" ]]; then
	declare -i newpkgver_rel=${oldpkgver_rel}+1
	newpkgver=${newpkgver_date}.${newpkgver_rel}
else
	newpkgver=${newpkgver_date}
fi

# Update the PKGBUILD
sed -i -e 's|^pkgver=.*|pkgver=${newpkgver}|' \
       -e 's|^_gitver=.*|_gitver=${newgitver}|' \
       -e 's|^pkgrel=.*|pkgrel=1|' \
       PKGBUILD
updpkgsums
git add PKGBUILD
git commit -m 'Update libre/your-freedom'

# Build the new package
makepkg
librestage libre

# Publish the updates
git push
librerelease
