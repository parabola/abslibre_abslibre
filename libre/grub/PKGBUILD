# Maintainer (Arch): Tobias Powalowski <tpowa@archlinux.org>
# Maintainer (Arch): Ronald van Haren <ronald.archlinux.org>
# Contributor (Arch): Keshav Amburay <(the ddoott ridikulus ddoott rat) (aatt) (gemmaeiil) (ddoott) (ccoomm)>
# Maintainer: André Silva <emulatorman@parabola.nu>
# Maintainer: Márcio Silva <coadde@parabola.nu>

## '1' to enable Xen support, '0' to disable
_XEN='0' # disabled, due it's fail to build

## '1' to enable IA32-EFI build in Arch x86_64, '0' to disable
_IA32_EFI_IN_ARCH_X64='1'

## '1' to enable IA32-XEN build in Arch x86_64, "0" to disable
_IA32_XEN_IN_ARCH_X64='1'

## '1' to enable EMU build (x86_64 and i686 only), "0" to disable
_GRUB_EMU_BUILD='0'

_pkgver='2.02'
_GRUB_GIT_TAG='grub-2.02-beta3'
_GRUB_EXTRAS_COMMIT=f2a079441939eee7251bf141986cdd78946e1d20

_UNIFONT_VER='9.0.06'

[[ "${CARCH}" = 'armv7h' ]] && _EFI_ARCH='arm'
[[ "${CARCH}" = 'x86_64' ]] && _EFI_ARCH='x86_64'
[[ "${CARCH}" = 'i686' ]] && _EFI_ARCH='i386'

[[ "${CARCH}" = 'x86_64' ]] && _XEN_ARCH='x86_64'

[[ "${CARCH}" = 'armv7h' ]] && _EMU_ARCH='arm'
[[ "${CARCH}" = 'x86_64' ]] && _EMU_ARCH='x86_64'
[[ "${CARCH}" = 'i686' ]] && _EMU_ARCH='i386'

pkgbase='grub'
pkgname=('grub')
[[ $CARCH = armv7h ]] && pkgname+=('grub-am335x_bone' 'grub-udoo' 'grub-omap3_beagle'
                                   'grub-omap3_beagle_xm' 'grub-omap3_beagle_xm_ab')
pkgdesc='GNU GRand Unified Bootloader (2), (Parabola rebranded)'
pkgver='2.02.beta3'
pkgrel='6.parabola1'
epoch='1'
url='https://www.gnu.org/software/grub/'
arch=('x86_64' 'i686' 'armv7h')
license=('GPL3')
backup=('etc/default/grub' 'etc/grub.d/40_custom')
install="${pkgname}.install"
options=('!makeflags')
depends=('sh' 'xz' 'gettext' 'device-mapper')
makedepends=('git' 'rsync' 'xz' 'freetype2' 'ttf-dejavu' 'python' 'autogen'
             'texinfo' 'help2man' 'gettext' 'device-mapper' 'fuse')

#if [[ "${CARCH}" = 'x86_64' ]] && [[ "${_XEN}" = '1' ]]; then
#	makedepends+=('xen')
#fi

if [[ "${_GRUB_EMU_BUILD}" = '1' ]]; then
	makedepends+=('libusbx' 'sdl')
fi

optdepends=('freetype2: For grub-mkfont usage'
            'fuse: For grub-mount usage')

if [[ "${CARCH}" = 'x86_64' ]] || [[ "${CARCH}" = 'i686' ]]; then
	provides=('grub-common' 'grub-bios' 'grub-emu' "grub-efi-${_EFI_ARCH}")
	conflicts=('grub-common' 'grub-bios' 'grub-emu' "grub-efi-${_EFI_ARCH}" 'grub-legacy' 'grub-parabola')
	replaces=('grub-common' 'grub-bios' 'grub-emu' "grub-efi-${_EFI_ARCH}" 'grub-parabola')
elif [[ "${CARCH}" = 'armv7h' ]]; then
	provides=('grub-common' 'grub-emu' "grub-efi-${_EFI_ARCH}")
	conflicts=('grub-common' 'grub-emu' "grub-efi-${_EFI_ARCH}")
	replaces=('grub-common' 'grub-emu' "grub-efi-${_EFI_ARCH}")
fi

source=("grub-${_pkgver}::git+git://git.sv.gnu.org/grub.git#tag=${_GRUB_GIT_TAG}"
        "grub-extras::git+git://git.sv.gnu.org/grub-extras.git#commit=${_GRUB_EXTRAS_COMMIT}"
        "http://ftp.gnu.org/gnu/unifont/unifont-${_UNIFONT_VER}/unifont-${_UNIFONT_VER}.bdf.gz"
        "http://ftp.gnu.org/gnu/unifont/unifont-${_UNIFONT_VER}/unifont-${_UNIFONT_VER}.bdf.gz.sig"
        '0001-Fix-security-issue-when-reading-username-and-passwor.patch'
        '0003-10_linux-20_linux_xen-detect-parabola-initramfs.patch'
        '0004-add-GRUB_COLOR_variables.patch'
        '0005-10_linux-fix-grouping-of-tests.patch'
        '0006-efi-properly-terminate-filepath-with-NULL-in-chainloader.patch'
        'grub.default'
        '0003-10_linux-20_linux_xen-detect-am335x_bone+am335x_boneblack-devicetree-file.patch'
        '0003-10_linux-20_linux_xen-detect-omap3_beagle-devicetree-file.patch'
        '0003-10_linux-20_linux_xen-detect-omap3_beagle_xm-devicetree-file.patch'
        '0003-10_linux-20_linux_xen-detect-omap3_beagle_xm_ab-devicetree-file.patch'
        '0003-10_linux-20_linux_xen-detect-udoo-devicetree-file.patch'
        '0003-10_linux-20_linux_xen-rebrand-free-distros.patch')

md5sums=('SKIP'
         'SKIP'
         'd849b5922485692d26bb1cb239259274'
         'SKIP'
         '9589ec46a04f9bb4d5da987340a4a324'
         '3a9bb9bafe0062388e11f72f0e80ba7e'
         'e506ae4a9f9f7d1b765febfa84e10d48'
         'f1999315bbd25b4b9359919ce9b36144'
         'a248d3f53f1fc38cfd7e49b99a559b43'
         '8d1dd54ae4a1b550c097e056892ce953'
         'a46695e19b588a53b8bac9e3b3c56096'
         '803f5b5f56ba6d74ca84aea67659116e'
         'b2c20ce3aa7944636b3ea499b40f2160'
         '7e3245bdd30e291f44ab8a00b8a38f3e'
         '95b721dfe74f42df52b7ae94e0e7c067'
         'e0133ad89ab3014210d4599f396a556e')

validpgpkeys=('95D2E9AB8740D8046387FD151A09227B1F435A33') #Paul Hardy

_pkgver() {
	cd "${srcdir}/grub-${_pkgver}/"
	echo "$(git describe --tags)" | sed -e 's|grub.||g' -e 's|-|\.|g'
}

prepare() {
	cd "${srcdir}/grub-${_pkgver}/"

	msg 'Patch to fix CVE-2015-8370'
	# CVE-2015-8370
	patch -Np1 -i "${srcdir}/0001-Fix-security-issue-when-reading-username-and-passwor.patch"

	msg 'Patch to detect of Parabola GNU/Linux-libre initramfs images by grub-mkconfig'
	patch -Np1 -i "${srcdir}/0003-10_linux-20_linux_xen-detect-parabola-initramfs.patch"

	msg 'Patch to enable GRUB_COLOR_* variables in grub-mkconfig'
	## Based on http://lists.gnu.org/archive/html/grub-devel/2012-02/msg00021.html
	patch -Np1 -i "${srcdir}/0004-add-GRUB_COLOR_variables.patch"

	msg 'Patch to fix grouping of tests for GRUB_DEVICE'
	patch -Np1 -i "${srcdir}/0005-10_linux-fix-grouping-of-tests.patch"

	msg "Patch to properly terminate filepath with NULL in chainloader"
	patch -Np1 -i "${srcdir}/0006-efi-properly-terminate-filepath-with-NULL-in-chainloader.patch"

	msg 'Fix DejaVuSans.ttf location so that grub-mkfont can create *.pf2 files for starfield theme'
	sed 's|/usr/share/fonts/dejavu|/usr/share/fonts/dejavu /usr/share/fonts/TTF|g' -i "${srcdir}/grub-${_pkgver}/configure.ac"

	msg 'Rebranding for some free distros'
	patch -Np1 -i "${srcdir}/0003-10_linux-20_linux_xen-rebrand-free-distros.patch"

	msg "Fix mkinitcpio 'rw' FS#36275"
	sed 's| ro | rw |g' -i "${srcdir}/grub-${_pkgver}/util/grub.d/10_linux.in"

	# msg 'autogen.sh requires python (2/3). since bzr is in makedepends, use python2 and no need to pull python3'
	# sed 's|python |python2 |g' -i "${srcdir}/grub-${_pkgver}/autogen.sh"

	msg 'Pull in latest language files'
	./linguas.sh

	msg 'Remove not working langs which need LC_ALL=C.UTF-8'
	sed -e 's#en@cyrillic en@greek##g' -i "${srcdir}/grub-${_pkgver}/po/LINGUAS"

	msg 'Avoid problem with unifont during compile of grub, http://savannah.gnu.org/bugs/?40330 and https://bugs.archlinux.org/task/37847'
	cp "${srcdir}/unifont-${_UNIFONT_VER}.bdf" "${srcdir}/grub-${_pkgver}/unifont.bdf"

	msg "Add the grub-extra sources for BIOS build"
	install -d "${srcdir}/grub-${_pkgver}/grub-extras"
	cp -r "${srcdir}/grub-extras/915resolution" "${srcdir}/grub-${_pkgver}/grub-extras/915resolution"
	export GRUB_CONTRIB="${srcdir}/grub-${_pkgver}/grub-extras/"
}

_build_grub-efi() {
	msg "Copy the source for building the ${_EFI_ARCH} EFI part"
	cp -r "${srcdir}/grub-${_pkgver}" "${srcdir}/grub-${_pkgver}-efi-${_EFI_ARCH}"

	msg "Unset all compiler FLAGS for ${_EFI_ARCH} EFI build"
	unset CFLAGS
	unset CPPFLAGS
	unset CXXFLAGS
	unset LDFLAGS
	unset MAKEFLAGS

	cd "${srcdir}/grub-${_pkgver}-efi-${_EFI_ARCH}"

	msg 'Run autogen.sh for ${_EFI_ARCH} EFI build'
	./autogen.sh

	msg "Run ./configure for ${_EFI_ARCH} EFI build"
	./configure \
		--with-platform="efi" \
		--target="${_EFI_ARCH}" \
		--disable-efiemu \
		--enable-mm-debug \
		--enable-nls \
		--enable-device-mapper \
		--enable-cache-stats \
		--enable-boot-time \
		--enable-grub-mkfont \
		--enable-grub-mount \
		--prefix="/usr" \
		--bindir="/usr/bin" \
		--sbindir="/usr/bin" \
		--mandir="/usr/share/man" \
		--infodir="/usr/share/info" \
		--datarootdir="/usr/share" \
		--sysconfdir="/etc" \
		--program-prefix="" \
		--with-bootdir="/boot" \
		--with-grubdir="grub" \
		--disable-silent-rules \
		--disable-werror

	msg "Run make for ${_EFI_ARCH} EFI build"
	make
}

#_build_grub-xen() {
#	msg "Copy the source for building the ${_XEN_ARCH} XEN part"
#	cp -r "${srcdir}/grub-${_pkgver}" "${srcdir}/grub-${_pkgver}-xen-${_XEN_ARCH}"
#
#	msg "Unset all compiler FLAGS for ${_XEN_ARCH} XEN build"
#	unset CFLAGS
#	unset CPPFLAGS
#	unset CXXFLAGS
#	unset LDFLAGS
#	unset MAKEFLAGS
#
#	cd "${srcdir}/grub-${_pkgver}-xen-${_XEN_ARCH}"
#
#	msg "Run autogen.sh for ${_XEN_ARCH} XEN build"
#	./autogen.sh
#
#	msg "Run ./configure for ${_XEN_ARCH} XEN build"
#	./configure \
#		--with-platform='xen' \
#		--target="${_XEN_ARCH}" \
#		--disable-efiemu \
#		--enable-mm-debug \
#		--enable-nls \
#		--enable-device-mapper \
#		--enable-cache-stats \
#		--enable-boot-time \
#		--enable-grub-mkfont \
#		--enable-grub-mount \
#		--prefix='/usr' \
#		--bindir='/usr/bin' \
#		--sbindir='/usr/bin' \
#		--mandir='/usr/share/man' \
#		--infodir='/usr/share/info' \
#		--datarootdir='/usr/share' \
#		--sysconfdir='/etc' \
#		--program-prefix='' \
#		--with-bootdir='/boot' \
#		--with-grubdir='grub' \
#		--disable-silent-rules \
#		--disable-werror
#
#	msg "Run make for ${_XEN_ARCH} XEN build"
#	make
#}

_build_grub-bios() {
	msg 'Set ARCH dependent variables for BIOS build'
	if [[ "${CARCH}" = 'x86_64' ]]; then
		_EFIEMU='--enable-efiemu'
	else
		_EFIEMU='--disable-efiemu'
	fi

	msg 'Copy the source for building the BIOS part'
	cp -r "${srcdir}/grub-${_pkgver}" "${srcdir}/grub-${_pkgver}-bios"

	msg 'Unset all compiler FLAGS for BIOS build'
	unset CFLAGS
	unset CPPFLAGS
	unset CXXFLAGS
	unset LDFLAGS
	unset MAKEFLAGS

	cd "${srcdir}/grub-${_pkgver}-bios"

	msg 'Run autogen.sh for BIOS build'
	./autogen.sh

	msg 'Run ./configure for BIOS build'
	./configure \
		--with-platform='pc' \
		--target='i386' \
		"${_EFIEMU}" \
		--enable-mm-debug \
		--enable-nls \
		--enable-device-mapper \
		--enable-cache-stats \
		--enable-boot-time \
		--enable-grub-mkfont \
		--enable-grub-mount \
		--prefix='/usr' \
		--bindir='/usr/bin' \
		--sbindir='/usr/bin' \
		--mandir='/usr/share/man' \
		--infodir='/usr/share/info' \
		--datarootdir='/usr/share' \
		--sysconfdir='/etc' \
		--program-prefix='' \
		--with-bootdir='/boot' \
		--with-grubdir='grub' \
		--disable-silent-rules \
		--disable-werror

	msg 'Run make for BIOS build'
	make
}

_build_grub-qemu() {
	msg 'Copy the source for building the QEMU part'
	cp -r "${srcdir}/grub-${_pkgver}" "${srcdir}/grub-${_pkgver}-qemu"

	msg 'Unset all compiler FLAGS for QEMU build'
	unset CFLAGS
	unset CPPFLAGS
	unset CXXFLAGS
	unset LDFLAGS
	unset MAKEFLAGS

	cd "${srcdir}/grub-${_pkgver}-qemu"

	msg 'Run autogen.sh for QEMU build'
	./autogen.sh

	msg 'Run ./configure for QEMU build'
	./configure \
		--with-platform='qemu' \
		--target='i386' \
		--disable-efiemu \
		--enable-mm-debug \
		--enable-nls \
		--enable-device-mapper \
		--enable-cache-stats \
		--enable-boot-time \
		--enable-grub-mkfont \
		--enable-grub-mount \
		--prefix='/usr' \
		--bindir='/usr/bin' \
		--sbindir='/usr/bin' \
		--mandir='/usr/share/man' \
		--infodir='/usr/share/info' \
		--datarootdir='/usr/share' \
		--sysconfdir='/etc' \
		--program-prefix='' \
		--with-bootdir='/boot' \
		--with-grubdir='grub' \
		--disable-silent-rules \
		--disable-werror

	msg 'Run make for QEMU build'
	make
}

_build_grub-ieee1275() {
	msg 'Copy the source for building the IEEE1275 (OpenFirmware) part'
	cp -r "${srcdir}/grub-${_pkgver}" "${srcdir}/grub-${_pkgver}-ieee1275"

	msg 'Unset all compiler FLAGS for IEEE1275 (OpenFirmware) build'
	unset CFLAGS
	unset CPPFLAGS
	unset CXXFLAGS
	unset LDFLAGS
	unset MAKEFLAGS

	cd "${srcdir}/grub-${_pkgver}-ieee1275"

	msg 'Run autogen.sh for IEEE1275 (OpenFirmware) build'
	./autogen.sh

	msg 'Run ./configure for IEEE1275 (OpenFirmware) build'
	./configure \
		--with-platform='ieee1275' \
		--target='i386' \
		--disable-efiemu \
		--enable-mm-debug \
		--enable-nls \
		--enable-device-mapper \
		--enable-cache-stats \
		--enable-boot-time \
		--enable-grub-mkfont \
		--enable-grub-mount \
		--prefix='/usr' \
		--bindir='/usr/bin' \
		--sbindir='/usr/bin' \
		--mandir='/usr/share/man' \
		--infodir='/usr/share/info' \
		--datarootdir='/usr/share' \
		--sysconfdir='/etc' \
		--program-prefix='' \
		--with-bootdir='/boot' \
		--with-grubdir='grub' \
		--disable-silent-rules \
		--disable-werror

	msg 'Run make for IEEE1275 (OpenFirmware) build'
	make
}

_build_grub-libreboot() {
	msg 'Copy the source for building the Libreboot part'
	cp -r "${srcdir}/grub-${_pkgver}" "${srcdir}/grub-${_pkgver}-libreboot"

	msg 'Unset all compiler FLAGS for Libreboot build'
	unset CFLAGS
	unset CPPFLAGS
	unset CXXFLAGS
	unset LDFLAGS
	unset MAKEFLAGS

	cd "${srcdir}/grub-${_pkgver}-libreboot"

	msg 'Run autogen.sh for Libreboot build'
	./autogen.sh

	msg 'Run ./configure for Libreboot build'
	./configure \
		--with-platform='coreboot' \
		--target='i386' \
		--disable-efiemu \
		--enable-mm-debug \
		--enable-nls \
		--enable-device-mapper \
		--enable-cache-stats \
		--enable-boot-time \
		--enable-grub-mkfont \
		--enable-grub-mount \
		--prefix='/usr' \
		--bindir='/usr/bin' \
		--sbindir='/usr/bin' \
		--mandir='/usr/share/man' \
		--infodir='/usr/share/info' \
		--datarootdir='/usr/share' \
		--sysconfdir='/etc' \
		--program-prefix='' \
		--with-bootdir='/boot' \
		--with-grubdir='grub' \
		--disable-silent-rules \
		--disable-werror

	msg 'Run make for Libreboot build'
	make
}

_build_grub-multiboot() {
	msg 'Copy the source for building the Multiboot part'
	cp -r "${srcdir}/grub-${_pkgver}" "${srcdir}/grub-${_pkgver}-multiboot"

	msg 'Unset all compiler FLAGS for Multiboot build'
	unset CFLAGS
	unset CPPFLAGS
	unset CXXFLAGS
	unset LDFLAGS
	unset MAKEFLAGS

	cd "${srcdir}/grub-${_pkgver}-multiboot"

	msg 'Run autogen.sh for Multiboot build'
	./autogen.sh

	msg 'Run ./configure for Multiboot build'
	./configure \
		--with-platform='multiboot' \
		--target='i386' \
		--disable-efiemu \
		--enable-mm-debug \
		--enable-nls \
		--enable-device-mapper \
		--enable-cache-stats \
		--enable-boot-time \
		--enable-grub-mkfont \
		--enable-grub-mount \
		--prefix='/usr' \
		--bindir='/usr/bin' \
		--sbindir='/usr/bin' \
		--mandir='/usr/share/man' \
		--infodir='/usr/share/info' \
		--datarootdir='/usr/share' \
		--sysconfdir='/etc' \
		--program-prefix='' \
		--with-bootdir='/boot' \
		--with-grubdir='grub' \
		--disable-silent-rules \
		--disable-werror

	msg 'Run make for Multiboot build'
	make
}

_build_grub-emu() {
	msg 'Copy the source for building the emu part'
	cp -r "${srcdir}/grub-${_pkgver}/" "${srcdir}/grub-${_pkgver}-emu/"

	msg 'Unset all compiler FLAGS for emu build'
	unset CFLAGS
	unset CPPFLAGS
	unset CXXFLAGS
	unset LDFLAGS
	unset MAKEFLAGS

	cd "${srcdir}/grub-${_pkgver}-emu"

	msg 'Run autogen.sh for emu build'
	./autogen.sh

	msg 'Run ./configure for emu build'
	./configure \
		--with-platform='emu' \
		--target="${_EMU_ARCH}" \
		--enable-mm-debug \
		--enable-nls \
		--enable-device-mapper \
		--enable-cache-stats \
		--enable-grub-mkfont \
		--enable-grub-mount \
		--enable-grub-emu-usb=no \
		--enable-grub-emu-sdl=no \
		--disable-grub-emu-pci \
		--prefix='/usr' \
		--bindir='/usr/bin' \
		--sbindir='/usr/bin' \
		--mandir='/usr/share/man' \
		--infodir='/usr/share/info' \
		--datarootdir='/usr/share' \
		--sysconfdir='/etc' \
		--program-prefix='' \
		--with-bootdir='/boot' \
		--with-grubdir='grub' \
		--disable-silent-rules \
		--disable-werror

	msg 'Run make for emu build'
	make
}

_build_grub-uboot() {
	msg "Copy the source for building the U-Boot part"
	cp -r "${srcdir}/grub-${_pkgver}" "${srcdir}/grub-${_pkgver}-uboot"

	msg 'Unset all compiler FLAGS for U-Boot build'
	unset CFLAGS
	unset CPPFLAGS
	unset CXXFLAGS
	unset LDFLAGS
	unset MAKEFLAGS

	cd "${srcdir}/grub-${_pkgver}-uboot"

	#sed -i '\|grub_arm_disable_caches_mmu|,+4 d
	#       ' grub-core/loader/arm/linux.c

	msg 'Run autogen.sh for U-Boot build'
	./autogen.sh

	msg 'Run ./configure for U-Boot build'
	./configure \
		--with-platform='uboot' \
		--target='arm' \
		--disable-efiemu \
		--enable-mm-debug \
		--enable-nls \
		--enable-device-mapper \
		--enable-cache-stats \
		--enable-boot-time \
		--enable-grub-mkfont \
		--enable-grub-mount \
		--prefix='/usr' \
		--bindir='/usr/bin' \
		--sbindir='/usr/bin' \
		--mandir='/usr/share/man' \
		--infodir='/usr/share/info' \
		--datarootdir='/usr/share' \
		--sysconfdir='/etc' \
		--program-prefix='' \
		--with-bootdir='/boot' \
		--with-grubdir='grub' \
		--disable-silent-rules \
		--disable-werror

	msg 'Run make for U-Boot build'
	make
}

build() {
	cd "${srcdir}/grub-${_pkgver}/"

	if [[ "${CARCH}" = 'x86_64' ]] || [[ "${CARCH}" = 'i686' ]] || [[ "${CARCH}" = 'armv7h' ]]; then
		msg "Build grub ${_EFI_ARCH} EFI stuff"
		_build_grub-efi

		if [[ "${CARCH}" = 'x86_64' ]] && [[ "${_IA32_EFI_IN_ARCH_X64}" = '1' ]]; then
			msg 'Build grub i386 EFI stuff'
			_EFI_ARCH='i386' _build_grub-efi
		fi
	fi

	if [[ "${CARCH}" = 'x86_64' ]] || [[ "${CARCH}" = 'i686' ]]; then
#		if [[ "${CARCH}" = 'x86_64' ]] && [[ "${_XEN}" = '1' ]]; then
#			msg "Build grub ${_XEN_ARCH} XEN stuff"
#			_build_grub-xen
#
#			if [[ "${_IA32_XEN_IN_ARCH_X64}" = '1' ]]; then
#				msg 'Build grub i386 XEN stuff'
#				_XEN_ARCH='i386' _build_grub-xen
#			fi
#		fi

		msg 'Build grub BIOS stuff'
		_build_grub-bios

		msg 'Build grub QEMU stuff'
		_build_grub-qemu

		msg 'Build grub IEEE1275 (OpenFirmware) stuff'
		_build_grub-ieee1275

		msg 'Build grub Libreboot stuff'
		_build_grub-libreboot

		msg 'Build grub Multiboot stuff'
		_build_grub-multiboot

#		msg "Build grub ${_XEN_ARCH} XEN stuff"
#		_build_grub-xen
	elif [[ "${CARCH}" = 'armv7h' ]]; then
		msg 'Build grub U-Boot stuff'
		_build_grub-uboot
	fi

	if [[ "${_GRUB_EMU_BUILD}" = '1' ]]; then
		msg 'Build grub emu stuff'
		_build_grub-emu
	fi
}

_package_grub-efi() {
	cd "${srcdir}/grub-${_pkgver}-efi-${_EFI_ARCH}/"

	msg "Run make install for ${_EFI_ARCH} EFI build"
	make DESTDIR="${pkgdir}/" bashcompletiondir="/usr/share/bash-completion/completions" install

	msg "Remove gdb debugging related files for ${_EFI_ARCH} EFI build"
	rm -f "${pkgdir}/usr/lib/grub/${_EFI_ARCH}-efi"/*.module || true
	rm -f "${pkgdir}/usr/lib/grub/${_EFI_ARCH}-efi"/*.image || true
	rm -f "${pkgdir}/usr/lib/grub/${_EFI_ARCH}-efi"/{kernel.exec,gdb_grub,gmodule.pl} || true
}

#_package_grub-xen() {
#	cd "${srcdir}/grub-${_pkgver}-xen-${_XEN_ARCH}/"
#
#	msg "Run make install for ${_XEN_ARCH} XEN build"
#	make DESTDIR="${pkgdir}/" bashcompletiondir="/usr/share/bash-completion/completions" install
#
#	msg "Remove gdb debugging related files for ${_XEN_ARCH} XEN build"
#	rm -f "${pkgdir}/usr/lib/grub/${_XEN_ARCH}-xen"/*.module || true
#	rm -f "${pkgdir}/usr/lib/grub/${_XEN_ARCH}-xen"/*.image || true
#	rm -f "${pkgdir}/usr/lib/grub/${_XEN_ARCH}-xen"/{kernel.exec,gdb_grub,gmodule.pl} || true
#}

_package_grub-bios() {
	cd "${srcdir}/grub-${_pkgver}-bios/"

	msg 'Run make install for BIOS build'
	make DESTDIR="${pkgdir}/" bashcompletiondir="/usr/share/bash-completion/completions" install

	msg 'Remove gdb debugging related files for BIOS build'
	rm -f "${pkgdir}/usr/lib/grub/i386-pc"/*.module || true
	rm -f "${pkgdir}/usr/lib/grub/i386-pc"/*.image || true
	rm -f "${pkgdir}/usr/lib/grub/i386-pc"/{kernel.exec,gdb_grub,gmodule.pl} || true
}

_package_grub-qemu() {
	cd "${srcdir}/grub-${_pkgver}-qemu/"

	msg 'Run make install for QEMU build'
	make DESTDIR="${pkgdir}/" bashcompletiondir="/usr/share/bash-completion/completions" install

	msg 'Remove gdb debugging related files for QEMU build'
	rm -f "${pkgdir}/usr/lib/grub/i386-qemu"/*.module || true
	rm -f "${pkgdir}/usr/lib/grub/i386-qemu"/*.image || true
	rm -f "${pkgdir}/usr/lib/grub/i386-qemu"/{kernel.exec,gdb_grub,gmodule.pl} || true
}

_package_grub-ieee1275() {
	cd "${srcdir}/grub-${_pkgver}-ieee1275/"

	msg 'Run make install for IEEE1275 (OpenFirmware) build'
	make DESTDIR="${pkgdir}/" bashcompletiondir="/usr/share/bash-completion/completions" install

	msg 'Remove gdb debugging related files for IEEE1275 (OpenFirmware) build'
	rm -f "${pkgdir}/usr/lib/grub/i386-ieee1275"/*.module || true
	rm -f "${pkgdir}/usr/lib/grub/i386-ieee1275"/*.image || true
	rm -f "${pkgdir}/usr/lib/grub/i386-ieee1275"/{kernel.exec,gdb_grub,gmodule.pl} || true
}

_package_grub-libreboot() {
	cd "${srcdir}/grub-${_pkgver}-libreboot/"

	msg 'Run make install for Libreboot build'
	make DESTDIR="${pkgdir}/" bashcompletiondir="/usr/share/bash-completion/completions" install

	msg 'Remove gdb debugging related files for Libreboot build'
	rm -f "${pkgdir}/usr/lib/grub/i386-coreboot"/*.module || true
	rm -f "${pkgdir}/usr/lib/grub/i386-coreboot"/*.image || true
	rm -f "${pkgdir}/usr/lib/grub/i386-coreboot"/{kernel.exec,gdb_grub,gmodule.pl} || true
}

_package_grub-multiboot() {
	cd "${srcdir}/grub-${_pkgver}-multiboot/"

	msg 'Run make install for Multiboot build'
	make DESTDIR="${pkgdir}/" bashcompletiondir="/usr/share/bash-completion/completions" install

	msg 'Remove gdb debugging related files for Multiboot build'
	rm -f "${pkgdir}/usr/lib/grub/i386-multiboot"/*.module || true
	rm -f "${pkgdir}/usr/lib/grub/i386-multiboot"/*.image || true
	rm -f "${pkgdir}/usr/lib/grub/i386-multiboot"/{kernel.exec,gdb_grub,gmodule.pl} || true
}

_package_grub-emu() {
	cd "${srcdir}/grub-${_pkgver}-emu/"

	msg 'Run make install for emu build'
	make DESTDIR="${pkgdir}/" bashcompletiondir="/usr/share/bash-completion/completions" install

	msg 'Remove gdb debugging related files for emu build'
	rm -f "${pkgdir}/usr/lib/grub/${_EMU_ARCH}-emu"/*.module || true
	rm -f "${pkgdir}/usr/lib/grub/${_EMU_ARCH}-emu"/*.image || true
	rm -f "${pkgdir}/usr/lib/grub/${_EMU_ARCH}-emu"/{kernel.exec,gdb_grub,gmodule.pl} || true
}

_package_grub-uboot() {
	cd "${srcdir}/grub-${_pkgver}-uboot/"

	msg 'Run make install for U-Boot build'
	make DESTDIR="${pkgdir}/" bashcompletiondir="/usr/share/bash-completion/completions" install

	msg 'Remove gdb debugging related files for U-Boot build'
	rm -f "${pkgdir}/usr/lib/grub/arm-uboot"/*.module || true
	rm -f "${pkgdir}/usr/lib/grub/arm-uboot"/*.image || true
	rm -f "${pkgdir}/usr/lib/grub/arm-uboot"/{kernel.exec,gdb_grub,gmodule.pl} || true
}

package_grub() {
	optdepends+=('dosfstools: For grub-mkrescue FAT FS and EFI support'
	             'efibootmgr: For grub-install EFI support'
	             'libisoburn: Provides xorriso for generating grub rescue iso using grub-mkrescue'
	             'os-prober: To detect other OSes when generating grub.cfg in BIOS systems'
	             'mtools: For grub-mkrescue FAT FS support')

	if [[ "${CARCH}" = 'i686' ]]; then
		optdepends+=('linux-libre-xen: For boot support into XEN server')
	fi

	if [[ "${_GRUB_EMU_BUILD}" = '1' ]]; then
		optdepends+=('libusbx: For grub-emu USB support'
		             'sdl: For grub-emu SDL support')
	fi

	cd "${srcdir}/grub-${_pkgver}/"

	if [[ "${CARCH}" = 'x86_64' ]] || [[ "${CARCH}" = 'i686' ]] || [[ "${CARCH}" = 'armv7h' ]]; then	
		msg "Package grub ${_EFI_ARCH} EFI stuff"
		_package_grub-efi

		if [[ "${CARCH}" = 'x86_64' ]] && [[ "${_IA32_EFI_IN_ARCH_X64}" = '1' ]]; then
			msg 'Package grub i386 EFI stuff'
			_EFI_ARCH='i386' _package_grub-efi
		fi
	fi

	if [[ "${CARCH}" = 'x86_64' ]] || [[ "${CARCH}" = 'i686' ]]; then
#		if [[ "${CARCH}" = 'x86_64' ]] && [[ "${_XEN}" = '1' ]]; then
#			msg "Package grub ${_XEN_ARCH} XEN stuff"
#			_package_grub-xen
#
#			if [[ "${_IA32_XEN_IN_ARCH_X64}" = '1' ]]; then
#				msg 'Package grub i386 XEN stuff'
#				_XEN_ARCH='i386' _package_grub-xen
#			fi
#		fi

		msg 'Package grub BIOS stuff'
		_package_grub-bios

		msg 'Package grub QEMU stuff'
		_package_grub-qemu

		msg 'Package grub IEEE1275 (OpenFirmware) stuff'
		_package_grub-ieee1275

		msg 'Package grub Libreboot stuff'
		_package_grub-libreboot

		msg 'Package grub Multiboot stuff'
		_package_grub-multiboot
	elif [[ "${CARCH}" = 'armv7h' ]]; then
		msg 'Package grub U-Boot stuff'
		_package_grub-uboot
	fi

	if [[ "${_GRUB_EMU_BUILD}" = '1' ]]; then
		msg 'Package grub emu stuff'
		_package_grub-emu
	fi

	msg 'Install /etc/default/grub (used by grub-mkconfig)'
	install -D -m0644 "${srcdir}/grub.default" "${pkgdir}/etc/default/grub"
}

package_grub-am335x_bone() {
	pkgdesc='GNU GRand Unified Bootloader (2) for BeagleBone and BeagleBone Black (am335x-bone), (Parabola rebranded)'
	optdepends+=('dosfstools: For grub-mkrescue FAT FS'
	             'os-prober: To detect other OSes when generating grub.cfg in BIOS systems'
	             'mtools: For grub-mkrescue FAT FS support')

	msg "Copy the source for packaging the U-Boot (am335x-bone) part"
	cp -r "${srcdir}/grub-${_pkgver}-uboot" "${srcdir}/grub-${_pkgver}-uboot_am335x-bone"

	cd "${srcdir}/grub-${_pkgver}-uboot_am335x-bone/"

	msg 'Patch to detect am335x-bone device tree blob file (dtb)'
	patch -Np1 -i "${srcdir}/0003-10_linux-20_linux_xen-detect-am335x_bone+am335x_boneblack-devicetree-file.patch"

	msg 'Package grub U-Boot stuff'
	## _package_grub-uboot

	msg 'Run make install for U-Boot build'
	make DESTDIR="${pkgdir}/" bashcompletiondir="/usr/share/bash-completion/completions" install

	msg 'Remove gdb debugging related files for U-Boot build'
	rm -f "${pkgdir}/usr/lib/grub/arm-uboot"/*.module || true
	rm -f "${pkgdir}/usr/lib/grub/arm-uboot"/*.image || true
	rm -f "${pkgdir}/usr/lib/grub/arm-uboot"/{kernel.exec,gdb_grub,gmodule.pl} || true

	msg 'Install /etc/default/grub (used by grub-mkconfig)'
	install -D -m0644 "${srcdir}/grub.default" "${pkgdir}/etc/default/grub"

	## msg 'Patch to detect am335x-bone device tree blob file (dtb)'
	## cd "${pkgdir}/etc/grub.d/"
	## patch -Np1 -i "${srcdir}/0003-10_linux-20_linux_xen-detect-am335x_bone+am335x_boneblack-devicetree-file.patch"
}

package_grub-omap3_beagle() {
	pkgdesc='GNU GRand Unified Bootloader (2) for BeagleBoard (omap3_beagle), (Parabola rebranded)'
	provides+=('grub-beagleboard')
	optdepends+=('dosfstools: For grub-mkrescue FAT FS'
	             'os-prober: To detect other OSes when generating grub.cfg in BIOS systems'
	             'mtools: For grub-mkrescue FAT FS support')

	msg "Copy the source for packaging the U-Boot (omap3_beagle) part"
	cp -r "${srcdir}/grub-${_pkgver}-uboot" "${srcdir}/grub-${_pkgver}-uboot_omap3_beagle"

	cd "${srcdir}/grub-${_pkgver}-uboot_omap3_beagle/"

	msg 'Patch to detect omap3_beagle device tree blob file (dtb)'
	patch -Np1 -i "${srcdir}/0003-10_linux-20_linux_xen-detect-omap3_beagle-devicetree-file.patch"

	msg 'Package grub U-Boot stuff'
	## _package_grub-uboot

	msg 'Run make install for U-Boot build'
	make DESTDIR="${pkgdir}/" bashcompletiondir="/usr/share/bash-completion/completions" install

	msg 'Remove gdb debugging related files for U-Boot build'
	rm -f "${pkgdir}/usr/lib/grub/arm-uboot"/*.module || true
	rm -f "${pkgdir}/usr/lib/grub/arm-uboot"/*.image || true
	rm -f "${pkgdir}/usr/lib/grub/arm-uboot"/{kernel.exec,gdb_grub,gmodule.pl} || true

	msg 'Install /etc/default/grub (used by grub-mkconfig)'
	install -D -m0644 "${srcdir}/grub.default" "${pkgdir}/etc/default/grub"

	## msg 'Patch to detect omap3_beagle device tree blob file (dtb)'
	## cd "${pkgdir}/etc/grub.d/"
	## patch -Np1 -i "${srcdir}/0003-10_linux-20_linux_xen-detect-omap3_beagle-devicetree-file.patch"
}

package_grub-omap3_beagle_xm() {
	pkgdesc='GNU GRand Unified Bootloader (2) for BeagleBoard-xM (omap3_beagle_xm), (Parabola rebranded)'
	provides+=('grub-beagleboard')
	optdepends+=('dosfstools: For grub-mkrescue FAT FS'
	             'os-prober: To detect other OSes when generating grub.cfg in BIOS systems'
	             'mtools: For grub-mkrescue FAT FS support')

	msg "Copy the source for packaging the U-Boot (omap3_beagle_xm) part"
	cp -r "${srcdir}/grub-${_pkgver}-uboot" "${srcdir}/grub-${_pkgver}-uboot_omap3_beagle_xm"

	cd "${srcdir}/grub-${_pkgver}-uboot_omap3_beagle_xm/"

	msg 'Patch to detect omap3_beagle_xm device tree blob file (dtb)'
	patch -Np1 -i "${srcdir}/0003-10_linux-20_linux_xen-detect-omap3_beagle_xm-devicetree-file.patch"

	msg 'Package grub U-Boot stuff'
	## _package_grub-uboot

	msg 'Run make install for U-Boot build'
	make DESTDIR="${pkgdir}/" bashcompletiondir="/usr/share/bash-completion/completions" install

	msg 'Remove gdb debugging related files for U-Boot build'
	rm -f "${pkgdir}/usr/lib/grub/arm-uboot"/*.module || true
	rm -f "${pkgdir}/usr/lib/grub/arm-uboot"/*.image || true
	rm -f "${pkgdir}/usr/lib/grub/arm-uboot"/{kernel.exec,gdb_grub,gmodule.pl} || true

	msg 'Install /etc/default/grub (used by grub-mkconfig)'
	install -D -m0644 "${srcdir}/grub.default" "${pkgdir}/etc/default/grub"

	## msg 'Patch to detect omap3_beagle_xm device tree blob file (dtb)'
	## cd "${pkgdir}/etc/grub.d/"
	## patch -Np1 -i "${srcdir}/0003-10_linux-20_linux_xen-detect-omap3_beagle_xm-devicetree-file.patch"
}

package_grub-omap3_beagle_xm_ab() {
	pkgdesc='GNU GRand Unified Bootloader (2) for BeagleBoard-xM rev A/B (omap3_beagle_xm_ab), (Parabola rebranded)'
	provides+=('grub-beagleboard')
	optdepends+=('dosfstools: For grub-mkrescue FAT FS'
	             'os-prober: To detect other OSes when generating grub.cfg in BIOS systems'
	             'mtools: For grub-mkrescue FAT FS support')

	msg "Copy the source for packaging the U-Boot (omap3_beagle_xm_ab) part"
	cp -r "${srcdir}/grub-${_pkgver}-uboot" "${srcdir}/grub-${_pkgver}-uboot_omap3_beagle_xm_ab"

	cd "${srcdir}/grub-${_pkgver}-uboot_omap3_beagle_xm_ab/"

	msg 'Patch to detect omap3_beagle_xm_ab device tree blob file (dtb)'
	patch -Np1 -i "${srcdir}/0003-10_linux-20_linux_xen-detect-omap3_beagle_xm_ab-devicetree-file.patch"

	msg 'Package grub U-Boot stuff'
	## _package_grub-uboot

	msg 'Run make install for U-Boot build'
	make DESTDIR="${pkgdir}/" bashcompletiondir="/usr/share/bash-completion/completions" install

	msg 'Remove gdb debugging related files for U-Boot build'
	rm -f "${pkgdir}/usr/lib/grub/arm-uboot"/*.module || true
	rm -f "${pkgdir}/usr/lib/grub/arm-uboot"/*.image || true
	rm -f "${pkgdir}/usr/lib/grub/arm-uboot"/{kernel.exec,gdb_grub,gmodule.pl} || true

	msg 'Install /etc/default/grub (used by grub-mkconfig)'
	install -D -m0644 "${srcdir}/grub.default" "${pkgdir}/etc/default/grub"

	## msg 'Patch to detect omap3_beagle_xm_ab device tree blob file (dtb)'
	## cd "${pkgdir}/etc/grub.d/"
	## patch -Np1 -i "${srcdir}/0003-10_linux-20_linux_xen-detect-omap3_beagle_xm_ab-devicetree-file.patch"
}

package_grub-udoo() {
	pkgdesc='GNU GRand Unified Bootloader (2) for UDOO (udoo), (Parabola rebranded)'
	optdepends+=('dosfstools: For grub-mkrescue FAT FS'
	             'os-prober: To detect other OSes when generating grub.cfg in BIOS systems'
	             'mtools: For grub-mkrescue FAT FS support')

	msg "Copy the source for packaging the U-Boot (udoo) part"
	cp -r "${srcdir}/grub-${_pkgver}-uboot" "${srcdir}/grub-${_pkgver}-uboot_udoo"

	cd "${srcdir}/grub-${_pkgver}-uboot_udoo/"

	msg 'Patch to detect udoo device tree blob file (dtb)'
	patch -Np1 -i "${srcdir}/0003-10_linux-20_linux_xen-detect-udoo-devicetree-file.patch"

	msg 'Package grub U-Boot stuff'
	## _package_grub-uboot

	msg 'Run make install for U-Boot build'
	make DESTDIR="${pkgdir}/" bashcompletiondir="/usr/share/bash-completion/completions" install

	msg 'Remove gdb debugging related files for U-Boot build'
	rm -f "${pkgdir}/usr/lib/grub/arm-uboot"/*.module || true
	rm -f "${pkgdir}/usr/lib/grub/arm-uboot"/*.image || true
	rm -f "${pkgdir}/usr/lib/grub/arm-uboot"/{kernel.exec,gdb_grub,gmodule.pl} || true

	msg 'Install /etc/default/grub (used by grub-mkconfig)'
	install -D -m0644 "${srcdir}/grub.default" "${pkgdir}/etc/default/grub"

	## msg 'Patch to detect udoo device tree blob file (dtb)'
	## cd "${pkgdir}/etc/grub.d/"
	## patch -Np1 -i "${srcdir}/0003-10_linux-20_linux_xen-detect-udoo-devicetree-file.patch"
}
