s|DuckDuckGo|DuckDuckGo HTML|
\|m_startingEngineName = settings[.]value("activeEngine", "Google")[.]toString();| s|Google|DuckDuckGo HTML|
\|m_defaultEngineName = settings[.]value("DefaultEngine", "Google")[.]toString();| s|Google|DuckDuckGo HTML|
\|Engine google;|d
\|google[.]name = "Google";|d
\|google[.]icon = QIcon(":icons/sites/google[.]png");|d
\|google[.]url = "https://www[.]google[.]com/search?client=qupzilla[&]q=%s";|d
\|google[.]shortcut = "g";|d
\|google[.]suggestionsUrl = "https://suggestqueries[.]google[.]com/complete/search?output=firefox[&]q=%s";|d
\|Engine yt;|d
\|yt[.]name = "YouTube";|d
\|yt[.]icon = QIcon(":/icons/sites/youtube.png");|d
\|yt[.]url = "https://www[.]youtube[.]com/results?search_query=%s[&]search=Search";|d
\|yt[.]shortcut = "yt";|d
\|yt[.]suggestionsUrl = "https://suggestqueries[.]google[.]com/complete/search?ds=yt[&]output=firefox[&]q=%s";|d
\|https://duckduckgo[.]com/?q=%s[&]t=qupzilla| s|duckduckgo[.]com|duckduckgo.com/html|
\|addEngine(google);|d
\|addEngine(yt);|d
\|m_defaultEngine = google;| s|google|duck|
