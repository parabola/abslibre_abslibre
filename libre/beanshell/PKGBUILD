# $Id$
# Maintainer (Arch): AndyRTR <andyrtr at archlinux.org>
# Maintainer: Omar Vega Ramos <ovruni@gnu.org.pe>

pkgname=beanshell
_pkgname=bsh
pkgver=2.0b6
pkgrel=1.parabola1
pkgdesc="Small, free, embeddable, source level Java interpreter with object based scripting language features written in Java"
arch=('any')
url="https://github.com/beanshell/beanshell"
license=('Apache')
depends=('java-runtime')
provides=('bsh')
replaces=('beanshell2')
conflicts=('beanshell2')
makedepends=('apache-ant' 'java-commons-bsf2' 'jh' 'tomcat7')
source=("https://github.com/beanshell/beanshell/releases/download/$pkgver/bsh-$pkgver-src.zip")
sha1sums=('76497846de1f3d2ef438d79e31328107658d10be')

# beanshell2 fork is dead
# http://stackoverflow.com/questions/26549307/current-state-of-beanshell
# https://github.com/stain/beanshell2 - google code got removed and only this fork is left and outdated 
# original url http://www.beanshell.org - unmaintained

# Fedora and our only package making use of it use the follow up of the old beanshell:
# https://github.com/beanshell/beanshell

prepare() {
  cd "$srcdir/BeanShell-$pkgver"
  rm -v $(find . -name \*.jar)

  ln -s /usr/share/java/tomcat7/servlet-api.jar lib/servlet.jar
}

build() {
  cd "$srcdir/BeanShell-$pkgver"
  ant dist
}

package() {
  cd "$srcdir/BeanShell-$pkgver"

  # Install license file
  install -Dm644 LICENSE "$pkgdir/usr/share/licenses/$pkgname/LICENSE"

  # Install documentation
  install -d "$pkgdir/usr/share/doc/$pkgname"
  cp -r javadoc "$pkgdir/usr/share/doc/$pkgname"

  # Install Maven artifact
  export DESTDIR=$pkgdir
  jh mvn-install org.apache-extras.beanshell bsh $pkgver \
    "dist/${_pkgname}-$pkgver.pom" \
    "dist/${_pkgname}-$pkgver.jar" \
    "${_pkgname}-$pkgver.jar"

  ln -s "/usr/share/java/${_pkgname}-$pkgver.jar" \
    "$pkgdir/usr/share/java/${_pkgname}.jar"

  # Install jars
  for _jar in bsf classgen classpath commands core engine reflect util; do
    install -Dm644 dist/${_pkgname}-${_jar}-$pkgver.jar \
      "${pkgdir}/usr/share/java/${_pkgname}-${_jar}.jar"
  done
}
