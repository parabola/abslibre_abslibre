From 0c2242fa1569a3516c59e3d84d28a2d8603aa774 Mon Sep 17 00:00:00 2001
From: Luke Shumaker <lukeshu@sbcglobal.net>
Date: Thu, 14 Apr 2016 17:06:07 -0400
Subject: [PATCH 1/1] makepkg: treat pkgrel more similarly to pkgver

This is perfectly fine with libalpm; it was only makepkg that was more
strict with pkgrel than pkgver.

Further, the former error message about invalid pkgrel formats claimed that
pkgrel was a "decimal", which would mean that `1.1 == 1.10`.  This is not
the case; alpm parses pkgrel as a version, not a decimal.  In that light,
enforcing /[0-9]+(\.([0-9]+)?/ on a version spec seems silly.
---
 doc/PKGBUILD.5.txt                            |  4 ++--
 scripts/libmakepkg/lint_pkgbuild/pkgrel.sh.in |  4 ++--
 test/util/vercmptest.sh                       | 23 ++++++++++++++++++++++-
 3 files changed, 26 insertions(+), 5 deletions(-)

diff --git a/doc/PKGBUILD.5.txt b/doc/PKGBUILD.5.txt
index e410481..86df1e8 100644
--- a/doc/PKGBUILD.5.txt
+++ b/doc/PKGBUILD.5.txt
@@ -48,7 +48,7 @@ similar to `$_basekernver`.
 
 *pkgver*::
 	The version of the software as released from the author (e.g., '2.7.1').
-	The variable is not allowed to contain colons or hyphens.
+	The variable is not allowed to contain colons, hyphens or whitespace.
 +
 The `pkgver` variable can be automatically updated by providing a `pkgver()`
 function in the PKGBUILD that outputs the new package version.
@@ -62,7 +62,7 @@ below).
 	allows package maintainers to make updates to the package's configure
 	flags, for example. This is typically set to '1' for each new upstream
 	software release and incremented for intermediate PKGBUILD updates. The
-	variable is not allowed to contain hyphens.
+	variable is not allowed to contain colons, hyphens or whitespace.
 
 *epoch*::
 	Used to force the package to be seen as newer than any previous versions
diff --git a/scripts/libmakepkg/lint_pkgbuild/pkgrel.sh.in b/scripts/libmakepkg/lint_pkgbuild/pkgrel.sh.in
index 95cb808..ca618f8 100644
--- a/scripts/libmakepkg/lint_pkgbuild/pkgrel.sh.in
+++ b/scripts/libmakepkg/lint_pkgbuild/pkgrel.sh.in
@@ -35,8 +35,8 @@ lint_pkgrel() {
 		return 1
 	fi
 
-	if [[ $pkgrel != +([0-9])?(.+([0-9])) ]]; then
-		error "$(gettext "%s must be a decimal, not %s.")" "pkgrel" "$pkgrel"
+	if [[ $pkgrel = *[[:space:]:-]* ]]; then
+		error "$(gettext "%s is not allowed to contain colons, hyphens or whitespace.")" "pkgrel" "$pkgrel"
 		return 1
 	fi
 }
diff --git a/test/util/vercmptest.sh b/test/util/vercmptest.sh
index 5542f59..4f9b1f3 100755
--- a/test/util/vercmptest.sh
+++ b/test/util/vercmptest.sh
@@ -39,7 +39,7 @@ tap_runtest() {
 	tap_is_str "$($bin "$ver2" "$ver1")" "$exp" "$ver2 $ver1"
 }
 
-tap_plan 92
+tap_plan 124
 
 # all similar length, no pkgrel
 tap_runtest 1.5.0 1.5.0  0
@@ -113,6 +113,27 @@ tap_runtest 1:1.0    1.0   1
 tap_runtest 1:1.0    1.1   1
 tap_runtest 1:1.1    1.1   1
 
+# complex pkgrel values
+tap_runtest 1-1.5.0    1-1.5.0     0
+tap_runtest 1-1.5.1    1-1.5.0     1
+tap_runtest 1-1.5.1    1-1.5       1
+tap_runtest 1-1.5b     1-1.5      -1
+tap_runtest 1-1.5b     1-1.5.1    -1
+# based on "from the manpage"
+tap_runtest 1-1.0a     1-1.0alpha -1
+tap_runtest 1-1.0alpha 1-1.0b     -1
+tap_runtest 1-1.0b     1-1.0beta  -1
+tap_runtest 1-1.0beta  1-1.0rc    -1
+tap_runtest 1-1.0rc    1-1.0      -1
+# based on "going crazy? alpha-dotted versions"
+tap_runtest 1-1.5.a    1-1.5       1
+tap_runtest 1-1.5.b    1-1.5.a     1
+tap_runtest 1-1.5.1    1-1.5.b     1
+# based on Parabola usage
+tap_runtest 1-1        1-2.par1   -1
+tap_runtest 1-2        1-2.par1   -1
+tap_runtest 1-3        1-2.par1    1
+
 tap_finish
 
 # vim: set noet:
-- 
2.8.0

