/******************************************************************************
 * user.js											*
 * Adapted from...									*
 * https://github.com/pyllyukko/user.js					*
 * https://github.com/The-OP/Fox/tree/master/prefs			*
 ******************************************************************************/

 /*****************************************************************************
 * Avoid hardware based fingerprintings                                       *
 * Canvas/Font's/Plugins                                                      *
 ******************************************************************************/
// https://wiki.mozilla.org/Platform/GFX/HardwareAcceleration
// https://www.macromedia.com/support/documentation/en/flashplayer/help/help01.html
// https://github.com/dillbyrne/random-agent-spoofer/issues/74
pref("gfx.direct2d.disabled",				true);
pref("layers.acceleration.disabled",			true);
pref("gfx.downloadable_fonts.fallback_delay", -1);
pref("intl.charset.default", "windows-1252");
pref("intl.locale.matchOS", false);
pref("javascript.use_us_english_locale", true);
pref("noscript.forbidFonts", true);

/******************************************************************************
 * HTML5 / APIs / DOM                                                         *
 *                                                                            *
 ******************************************************************************/

// disable Location-Aware Browsing
// http://www.mozilla.org/en-US/firefox/geolocation/
pref("geo.enabled",					false);

// Disable dom.mozTCPSocket.enabled (raw TCP socket support)
// https://trac.torproject.org/projects/tor/ticket/18863
// https://www.mozilla.org/en-US/security/advisories/mfsa2015-97/
// https://developer.mozilla.org/docs/Mozilla/B2G_OS/API/TCPSocket
pref("dom.mozTCPSocket.enabled",				false);

// Disable DOM Shared Workers
// See https://bugs.torproject.org/15562
pref("dom.workers.sharedWorkers.enabled", 				false); 

// Disable WebSockets
// https://www.infoq.com/news/2012/03/websockets-security
// http://mdn.beonex.com/en/WebSockets.html
pref("network.websocket.max-connections", 0);

// Disable DOM Push API
// https://developer.mozilla.org/en-US/docs/Web/API/Push_API
// https://wiki.mozilla.org/Security/Reviews/Push_API
// https://wiki.mozilla.org/Privacy/Reviews/Push_API
// https://bugzilla.mozilla.org/show_bug.cgi?id=1038811
// https://bugzilla.mozilla.org/show_bug.cgi?id=1153499
pref("dom.push.enabled", false);
pref("dom.push.serverURL", "");
pref("dom.push.userAgentID", "");
// https://hg.mozilla.org/releases/mozilla-beta/file/e549349b8d66/modules/libpref/init/all.js#l4237
pref("dom.push.connection.enabled", false);
pref("dom.push.adaptive.enabled", false);
pref("dom.push.udp.wakeupEnabled", false);
// https://hg.mozilla.org/releases/mozilla-beta/file/00bcc10b3bdc/modules/libpref/init/all.js#l4445
// https://hg.mozilla.org/releases/mozilla-beta/file/00bcc10b3bdc/dom/push/PushRecord.jsm#l59
pref("dom.push.maxQuotaPerSubscription", 0);
// https://wiki.mozilla.org/Security/Reviews/SimplePush
pref("services.push.enabled", false);
pref("services.push.serverURL", "");

// Disable Kinto Cloud
// Note: Pref may change name in future release
// https://bugzilla.mozilla.org/show_bug.cgi?id=1266235#c2
pref("services.kinto.base", "");

// Disable MDNS (Supposedly only for Android but is in Desktop version also)
// https://hg.mozilla.org/releases/mozilla-beta/file/00bcc10b3bdc/dom/presentation/provider/MulticastDNSDeviceProvider.cpp#l18
pref("dom.presentation.discovery.enabled", false);
pref("dom.presentation.discoverable", false);
pref("dom.presentation.discovery.legacy.enabled", false);

// http://kb.mozillazine.org/Dom.storage.enabled
// http://dev.w3.org/html5/webstorage/#dom-localstorage
// you can also see this with Panopticlick's "DOM localStorage"
pref("dom.storage.enabled",		false);

// Whether JS can get information about the network/browser connection
// Network Information API provides general information about the system's connection type (WiFi, cellular, etc.)
// https://developer.mozilla.org/en-US/docs/Web/API/Network_Information_API
// https://wicg.github.io/netinfo/#privacy-considerations
// https://bugzilla.mozilla.org/show_bug.cgi?id=960426
pref("dom.netinfo.enabled",				false);
// fingerprinting due to differing OS implementations
pref("dom.network.enabled",				false);

// Disable Web Audio API
// https://bugzil.la/1288359
pref("dom.webaudio.enabled",				false);

// Audio_data is deprecated in future releases, but still present
// in FF24. This is a dangerous combination (spotted by iSec)
pref("media.audio_data.enabled", false);

// Don't autoplay WebM and other embedded media files
// https://support.mozilla.org/en-US/questions/1073167
pref("media.autoplay.enabled", false);
pref("noscript.forbidMedia", true);

// Don't reveal your internal IP
// Check the settings with: http://net.ipcalf.com/
// https://wiki.mozilla.org/Media/WebRTC/Privacy
pref("media.peerconnection.ice.default_address_only",	true); // Firefox < 51
pref("media.peerconnection.ice.no_host",			true); // Firefox >= 51
// Disable WebRTC entirely
pref("media.peerconnection.enabled",			false);

// getUserMedia
// https://wiki.mozilla.org/Media/getUserMedia
pref("media.getusermedia.screensharing.allowed_domains", "");
pref("media.getusermedia.screensharing.enabled", false);
// https://developer.mozilla.org/en-US/docs/Web/API/Navigator
pref("media.navigator.enabled",				false);
// https://developer.mozilla.org/en-US/docs/Web/API/BatteryManager
pref("dom.battery.enabled",				false);
// https://wiki.mozilla.org/WebAPI/Security/WebTelephony
pref("dom.telephony.enabled",				false);
// https://developer.mozilla.org/en-US/docs/Web/API/navigator.sendBeacon
pref("beacon.enabled",					false);
// https://developer.mozilla.org/en-US/docs/Mozilla/Preferences/Preference_reference/dom.event.clipboardevents.enabled
pref("dom.event.clipboardevents.enabled",			false);
// https://wiki.mozilla.org/Security/Reviews/Firefox/NavigationTimingAPI
pref("dom.enable_performance",				false);
// https://wiki.mozilla.org/B2G/QA/WebAPI_Test_Plan/Vibration#API
pref("dom.vibrator.enabled",					false);

// Speech recognition
// https://dvcs.w3.org/hg/speech-api/raw-file/tip/speechapi.html
// https://wiki.mozilla.org/HTML5_Speech_API
pref("media.webspeech.recognition.enable",			false);
pref("media.webspeech.synth.enabled",				false);

// Disable getUserMedia screen sharing
// https://mozilla.github.io/webrtc-landing/gum_test.html
pref("media.getusermedia.screensharing.enabled",		false);

// Disable sensor API
// https://wiki.mozilla.org/Sensor_API
pref("device.sensors.enabled",				false);

// Disable MMS
pref("dom.mms.retrieval_mode", "never");

// http://kb.mozillazine.org/Browser.send_pings
pref("browser.send_pings",					false);
// this shouldn't have any effect, since we block pings altogether, but we'll set it anyway.
// http://kb.mozillazine.org/Browser.send_pings.require_same_host
pref("browser.send_pings.require_same_host",		true);

// https://developer.mozilla.org/en-US/docs/IndexedDB
// https://wiki.mozilla.org/Security/Reviews/Firefox4/IndexedDB_Security_Review
// TODO: find out why html5test still reports this as available
// Note: Disabled, Can be enable if it breaks plugins/sites which require it. Privacy Risk.
//       see: http://forums.mozillazine.org/viewtopic.php?p=13842047#p13842047
pref("dom.indexedDB.enabled",		false);

// TODO: "Access Your Location" "Maintain Offline Storage" "Show Notifications"

// Disable gamepad input
// http://www.w3.org/TR/gamepad/
pref("dom.gamepad.enabled",				false);
pref("dom.gamepad.test.enabled", false);

// Disable virtual reality devices
// https://developer.mozilla.org/en-US/Firefox/Releases/36#Interfaces.2FAPIs.2FDOM
pref("dom.vr.enabled",					false);
pref("dom.vr.cardboard.enabled", 				false);
pref("dom.vr.oculus.enabled", 				false);
pref("dom.vr.oculus050.enabled",				 false);
pref("dom.vr.poseprediction.enabled", 				false);
pref("dom.vr.add-test-devices", 				0);

// disable notifications
pref("dom.webnotifications.enabled",			false);

// HTML5 privacy https://bugzilla.mozilla.org/show_bug.cgi?id=500328
pref("browser.history.allowPopState",		false);
pref("browser.history.allowPushState",		false);
pref("browser.history.allowReplaceState",		false);
// Idle Observation
pref("dom.idle-observers-api.enabled", false);

// Prevent Timing Attacks
// https://network23.org/inputisevil/2015/09/06/how-html5-apis-can-fingerprint-users/
pref("dom.performance.enable_user_timing_logging", false);
pref("dom.enable_resource_timing", false); // Bug 13024
pref("dom.enable_user_timing", false); // Bug 16336
pref("dom.event.highrestimestamp.enabled", true); // Bug 17046: Don't leak system uptime in Events

// disable webGL
// http://www.contextis.com/resources/blog/webgl-new-dimension-browser-exploitation/
pref("webgl.disabled",					true);
// https://bugzilla.mozilla.org/show_bug.cgi?id=1171228
// https://developer.mozilla.org/en-US/docs/Web/API/WEBGL_debug_renderer_info
pref("webgl.enable-debug-renderer-info",			false);
pref("webgl.disable-extensions",				false);
pref("webgl.min_capability_mode",				true);
pref("webgl.disable-wgl",						true);
pref("webgl.enable-webgl2",					false);
// somewhat related...
pref("pdfjs.enableWebGL",					false);

// Disable File and Directory Entries API (Imported from Edge/Chromium)
// https://developer.mozilla.org/en-US/Firefox/Releases/50#Files_and_directories
// https://developer.mozilla.org/en-US/docs/Web/API/File_and_Directory_Entries_API
// https://developer.mozilla.org/en-US/docs/Web/API/File_and_Directory_Entries_API/Introduction
// https://developer.mozilla.org/en-US/docs/Web/API/File_and_Directory_Entries_API/Firefox_support
// https://bugzilla.mozilla.org/show_bug.cgi?id=1265767
pref("dom.webkitBlink.filesystem.enabled", false);
// https://developer.mozilla.org/en-US/docs/Web/API/HTMLInputElement/webkitdirectory
// https://bugzilla.mozilla.org/show_bug.cgi?id=1258489
// https://hg.mozilla.org/releases/mozilla-release/rev/133af19777be
pref("dom.webkitBlink.dirPicker.enabled", false);

// Directory Upload API, webkitdirectory
// https://bugzilla.mozilla.org/show_bug.cgi?id=1188880
// https://bugzilla.mozilla.org/show_bug.cgi?id=907707
// https://wicg.github.io/directory-upload/proposal.html
pref("dom.input.dirpicker", false);

// Disable FlyWeb
// http://www.ghacks.net/2016/07/26/firefox-flyweb/
// https://www.reddit.com/r/firefox/comments/4uwd1n/flyweb_we_dont_need_no_stinking_iot_apps/
// https://hg.mozilla.org/releases/mozilla-release/rev/576019c74103
// https://hg.mozilla.org/releases/mozilla-release/file/8dc18bf5abac/browser/extensions/flyweb/bootstrap.js#l36
pref("dom.flyweb.enabled", false);


// Disable Pointer Lock API.
// https://developer.mozilla.org/en-US/docs/Web/API/Pointer_Lock_API
// https://bugzilla.mozilla.org/show_bug.cgi?id=1273351
 pref("full-screen-api.pointer-lock.enabled", false);
pref("pointer-lock-api.prefixed.enabled", false);

/******************************************************************************
 * Misc                                                                       *
 *                                                                            *
 ******************************************************************************/
 
 // Disable website autorefresh, user can still proceed with warning
pref("accessibility.blockautorefresh", 			true);
pref("browser.meta_refresh_when_inactive.disabled",  			true);
pref("noscript.forbidMetaRefresh",				true);


// Disable face detection by default
pref("camera.control.face_detection.enabled",		false);
pref("camera.control.autofocus_moving_callback.enabled", false);

// Default search engine
//pref("browser.search.defaultenginename",		"DuckDuckGo");

// http://kb.mozillazine.org/Clipboard.autocopy
//pref("clipboard.autocopy",					false);

// Display an error message indicating the entered information is not a valid
// URL instead of asking from google.
// http://kb.mozillazine.org/Keyword.enabled#Caveats
pref("keyword.enabled",					false);

// Don't trim HTTP off of URLs in the address bar.
// https://bugzilla.mozilla.org/show_bug.cgi?id=665580
pref("browser.urlbar.trimURLs",				false);

// Don't try to guess where i'm trying to go!!! e.g.: "http://foo" -> "http://(prefix)foo(suffix)"
// http://www-archive.mozilla.org/docs/end-user/domain-guessing.html
pref("browser.fixup.alternate.enabled",			false);

// Set TOR as default proxy
pref("network.proxy.socks", "127.0.0.1");
pref("network.proxy.socks_port", 9050);
// Proxy off by default, user can toggle it on.
pref("network.proxy.type", 0);
// Protect TOR ports
pref("network.security.ports.banned", "9050,9051,9150,9151");

// https://trac.torproject.org/projects/tor/wiki/doc/TorifyHOWTO/WebBrowsers
pref("network.proxy.socks_remote_dns",			true);

// For fingerprinting and local service vulns (#10419)
pref("network.proxy.no_proxies_on", "");

// We not want to monitoring the connection state of users 
// https://trac.torproject.org/projects/tor/ticket/18945
pref("network.manage-offline-status",		false);

// Mixed content stuff
// https://developer.mozilla.org/en-US/docs/Site_Compatibility_for_Firefox_23#Non-SSL_contents_on_SSL_pages_are_blocked_by_default
// https://blog.mozilla.org/tanvi/2013/04/10/mixed-content-blocking-enabled-in-firefox-23/
pref("security.mixed_content.block_active_content",	true);
// Mixed Passive Content (a.k.a. Mixed Display Content).
pref("security.mixed_content.block_display_content",	true);

// https://secure.wikimedia.org/wikibooks/en/wiki/Grsecurity/Application-specific_Settings#Firefox_.28or_Iceweasel_in_Debian.29
pref("javascript.options.methodjit.chrome",		false);
pref("javascript.options.methodjit.content",		false);

// CIS Mozilla Firefox 24 ESR v1.0.0 - 3.7 Disable JAR from opening Unsafe File Types
// http://kb.mozillazine.org/Network.jar.open-unsafe-types
pref("network.jar.open-unsafe-types",			false);
// https://bugzilla.mozilla.org/show_bug.cgi?id=1173171
pref("network.jar.block-remote-files", true);

// CIS 2.7.4 Disable Scripting of Plugins by JavaScript
pref("security.xpconnect.plugin.unrestricted",		false);

// CIS Mozilla Firefox 24 ESR v1.0.0 - 3.8 Set File URI Origin Policy
// http://kb.mozillazine.org/Security.fileuri.strict_origin_policy
pref("security.fileuri.strict_origin_policy",		true);

// CIS 2.3.6 Disable Displaying Javascript in History URLs
// http://kb.mozillazine.org/Browser.urlbar.filter.javascript
pref("browser.urlbar.filter.javascript",			true);

// http://asmjs.org/
// https://www.mozilla.org/en-US/security/advisories/mfsa2015-29/
// https://www.mozilla.org/en-US/security/advisories/mfsa2015-50/
// https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2015-2712
pref("javascript.options.asmjs",				false);
// https://hacks.mozilla.org/2016/03/a-webassembly-milestone/
pref("javascript.options.wasm", false);
pref("javascript.options.wasm_baselinejit", false);
// https://trac.torproject.org/projects/tor/ticket/9387#comment:43
pref("javascript.options.typeinference", false);
pref("javascript.options.baselinejit.content", false);
pref("javascript.options.ion.content", false);
// https://www.torproject.org/projects/torbrowser/design
pref("mathml.disabled", true);

// https://wiki.mozilla.org/SVGOpenTypeFonts
// the iSEC Partners Report recommends to disable this
pref("gfx.font_rendering.opentype_svg.enabled",		false);

// Disable SVG
// Note: May only work in TBB due to upstream not implementing?
// https://trac.torproject.org/projects/tor/ticket/18770
// https://bugzilla.mozilla.org/show_bug.cgi?id=1173199#c9
pref("svg.in-content.enabled", false);

// https://bugzil.la/654550
// https://github.com/pyllyukko/user.js/issues/9#issuecomment-100468785
// https://github.com/pyllyukko/user.js/issues/9#issuecomment-148922065
pref("media.video_stats.enabled",				false);

// Don't reveal build ID
// Value taken from Tor Browser
// https://bugzil.la/583181
pref("general.buildID.override",				"20100101");

// Prevent font fingerprinting
// http://www.browserleaks.com/fonts
// https://github.com/pyllyukko/user.js/issues/120
pref("browser.display.use_document_fonts",			0);

// Prefer sans-serif
pref("font.default.x-western", "sans-serif");


/******************************************************************************
 * extensions / plugins                                                       *
 *                                                                            *
 ******************************************************************************/

// Require signatures
pref("xpinstall.signatures.required",		true);

// Opt-out of add-on metadata updates
// https://blog.mozilla.org/addons/how-to-opt-out-of-add-on-metadata-updates/
pref("extensions.getAddons.cache.enabled",			false);

// Flash plugin state - never activate
pref("plugin.state.flash",					0);
pref("plugins.notifyMissingFlash",		false);

// Java plugin state - never activate
pref("plugin.state.java",					0);

// disable Gnome Shell Integration
pref("plugin.state.libgnome-shell-browser-plugin",		0);

// disable the bundled OpenH264 video codec
// http://forums.mozillazine.org/viewtopic.php?p=13845077&sid=28af2622e8bd8497b9113851676846b1#p13845077
pref("media.gmp-provider.enabled",		false);

// https://wiki.mozilla.org/Firefox/Click_To_Play
// https://blog.mozilla.org/security/2012/10/11/click-to-play-plugins-blocklist-style/
pref("plugins.click_to_play",				true);

// Updates addons automatically
// Disabled due to Fingerprinting, you can update addons manually.
// https://blog.mozilla.org/addons/how-to-turn-off-add-on-updates/
pref("extensions.update.enabled",				false);
pref("extensions.update.autoUpdateDefault", false);
// User can still update manually, but we disable background updates.
pref("extensions.update.background.url", "");
// The system add-ons infrastructure that's used to ship Hello and Pocket in Firefox
pref("extensions.systemAddon.update.url", "");
// We can update our themes manually, may fingerprint the user.
pref("lightweightThemes.update.enabled", false);

// Only install extensions to user profile
// https://developer.mozilla.org/en-US/Add-ons/Installing_extensions
// https://mike.kaply.com/2012/02/21/understanding-add-on-scopes/
pref("extensions.enabledScopes", 1);

// http://kb.mozillazine.org/Extensions.blocklist.enabled
pref("extensions.blocklist.enabled",			false);
pref("extensions.blocklist.detailsURL", "about:blank");
pref("extensions.blocklist.itemURL", "about:blank");
pref("extensions.blocklist.url", "about:blank");
pref("extensions.getAddons.get.url", "about:blank");
pref("extensions.getAddons.getWithPerformance.url", "about:blank");
pref("extensions.getAddons.recommended.url", "about:blank");
pref("services.settings.server", "");
// If blocklist downloads, we want it to be signed.
pref("services.blocklist.signing.enforced", true);

// Disable Freedom Violating DRM Feature
// https://bugzilla.mozilla.org/show_bug.cgi?id=1144903#c8
pref("media.eme.apiVisible",				false);
pref("media.eme.enabled",				false);
pref("browser.eme.ui.enabled",			false);
pref("media.gmp-eme-adobe.enabled",		false);

// Fingerprints the user, not HTTPS. Remove it.
pref("pfs.datasource.url", "about:blank");
pref("pfs.filehint.url", "about:blank");

/******************************************************************************
 * firefox features / components                                              *
 *                                                                            *
 ******************************************************************************/

// WebIDE
// https://trac.torproject.org/projects/tor/ticket/16222
pref("devtools.webide.enabled",				false);
pref("devtools.webide.autoinstallADBHelper",		false);
pref("devtools.webide.autoinstallFxdtAdapters",		false);

// disable remote debugging
// https://developer.mozilla.org/docs/Tools/Remote_Debugging/Debugging_Firefox_Desktop#Enable_remote_debugging
// https://developer.mozilla.org/en-US/docs/Tools/Tools_Toolbox#Advanced_settings
pref("devtools.debugger.remote-enabled",			false);
// "to use developer tools in the context of the browser itself, and not only web content"
pref("devtools.chrome.enabled",				false);
// https://developer.mozilla.org/en-US/docs/Tools/Remote_Debugging/Debugging_Firefox_Desktop#Firefox_37_onwards
pref("devtools.debugger.force-local",			true);
pref("devtools.devices.url",  "about:blank");
pref("devtools.gcli.imgurUploadURL", "about:blank");
pref("devtools.gcli.jquerySrc", "about:blank");
pref("devtools.gcli.lodashSrc", "about:blank");
pref("devtools.gcli.underscoreSrc", "about:blank");
// http://forum.top-hat-sec.com/index.php?topic=4951.5;wap2
pref("devtools.remote.wifi.scan", false);
pref("devtools.remote.wifi.visible", false);
pref("devtools.webide.adaptersAddonURL", "about:blank");
pref("devtools.webide.adbAddonURL", "about:blank");
pref("devtools.webide.addonsURL", "about:blank");
//https://trac.torproject.org/projects/tor/ticket/16222
pref("devtools.webide.enabled", false); 
pref("devtools.webide.simulatorAddonsURL", "about:blank");
pref("devtools.webide.templatesURL", "about:blank");

// https://wiki.mozilla.org/Platform/Features/Telemetry
// https://www.mozilla.org/en-US/legal/privacy/firefox.html#telemetry
// https://wiki.mozilla.org/Security/Reviews/Firefox6/ReviewNotes/telemetry
pref("toolkit.telemetry.enabled",				false);
// https://gecko.readthedocs.org/en/latest/toolkit/components/telemetry/telemetry/preferences.html
pref("toolkit.telemetry.unified",				false);
pref("toolkit.telemetry.server", "about:blank");
pref("toolkit.telemetry.archive.enabled", false);
// https://wiki.mozilla.org/Telemetry/Experiments
pref("experiments.supported",				false);
pref("experiments.enabled",				false);
pref("experiments.manifest.uri",			false);
// https://trac.torproject.org/projects/tor/ticket/13170
pref("network.allow-experiments",			false);

// Disable the UITour backend so there is no chance that a remote page
// can use it to confuse Tor Browser users.
pref("browser.uitour.enabled",				false);

// https://wiki.mozilla.org/Security/Tracking_protection
// https://support.mozilla.org/en-US/kb/tracking-protection-firefox
pref("privacy.trackingprotection.enabled",			true);
// https://support.mozilla.org/en-US/kb/tracking-protection-pbm
pref("privacy.trackingprotection.pbmode.enabled",		true);

// Third Party Isolation Enabled Always
// https://github.com/arthuredelstein/tor-browser/commit/b8da7721a9df4af1b595eb046e94280fe8e32d31
pref("privacy.thirdparty.isolate", 2);

// Resist fingerprinting via window.screen and CSS media queries and other techniques
// https://bugzil.la/418986
// https://bugzil.la/1281949
// https://bugzil.la/1281963
pref("privacy.resistFingerprinting",			true);

// Disable the built-in PDF viewer (CVE-2015-2743)
// https://web.nvd.nist.gov/view/vuln/detail?vulnId=CVE-2015-2743
pref("pdfjs.disabled",					true);

// Disable sending of the health report
// https://support.mozilla.org/en-US/kb/firefox-health-report-understand-your-browser-perf
pref("datareporting.healthreport.uploadEnabled",		false);
// disable collection of the data (the healthreport.sqlite* files)
pref("datareporting.healthreport.service.enabled",		false);
// https://gecko.readthedocs.org/en/latest/toolkit/components/telemetry/telemetry/preferences.html
pref("datareporting.policy.dataSubmissionEnabled",		false);
pref("datareporting.healthreport.about.reportUrl", "about:blank");
pref("datareporting.healthreport.documentServerURI", "about:blank");
pref("datareporting.policy.firstRunTime", 0);

// Disable new tab tile ads & preload
// http://www.thewindowsclub.com/disable-remove-ad-tiles-from-firefox
// http://forums.mozillazine.org/viewtopic.php?p=13876331#p13876331
pref("browser.newtabpage.enhanced",			false);
pref("browser.newtab.preload",				false);
pref("browser.newtabpage.introShown",			true);
// https://wiki.mozilla.org/Tiles/Technical_Documentation#Ping
// https://gecko.readthedocs.org/en/latest/browser/browser/DirectoryLinksProvider.html#browser-newtabpage-directory-ping
pref("browser.newtabpage.directory.ping",			"");
// https://gecko.readthedocs.org/en/latest/browser/browser/DirectoryLinksProvider.html#browser-newtabpage-directory-source
pref("browser.newtabpage.directory.source",		"data:text/plain,{}");

// disable heartbeat
// https://wiki.mozilla.org/Advocacy/heartbeat
pref("browser.selfsupport.url",				"");

// Disable firefox hello
// https://wiki.mozilla.org/Loop
//pref("loop.enabled",		false);
// https://groups.google.com/d/topic/mozilla.dev.platform/nyVkCx-_sFw/discussion
pref("loop.logDomains",					false);

// Disable Crash Reporter (Massive browser fingerprinting)
pref("dom.ipc.plugins.flash.subprocess.crashreporter.enabled",	false);
pref("browser.tabs.crashReporting.sendReport", 				false);
pref("breakpad.reportURL",						"about:blank");

// Disable Slow Startup Notifications
pref("browser.slowStartup.maxSamples", 0);
pref("browser.slowStartup.notificationDisabled", true);
pref("browser.slowStartup.samples", 0);

// CIS 2.1.1 Disable Auto Update / Balrog
pref("app.update.auto", 						false);
pref("app.update.checkInstallTime", 			false);
pref("app.update.enabled",					false);
pref("app.update.staging.enabled",				false); 
pref("app.update.url",				"about:blank");
pref("media.gmp-manager.certs.1.commonName",	"");
pref("media.gmp-manager.certs.2.commonName",	"");

// CIS 2.3.4 Block Reported Web Forgeries
// http://kb.mozillazine.org/Browser.safebrowsing.enabled
// http://kb.mozillazine.org/Safe_browsing
// https://support.mozilla.org/en-US/kb/how-does-phishing-and-malware-protection-work
// http://forums.mozillazine.org/viewtopic.php?f=39&t=2711237&p=12896849#p12896849
pref("browser.safebrowsing.enabled",			false);

// CIS 2.3.5 Block Reported Attack Sites
// http://kb.mozillazine.org/Browser.safebrowsing.malware.enabled
pref("browser.safebrowsing.malware.enabled",		false);

// Disable safe browsing remote lookups for downloaded files.
// This leaks information to google.
// https://www.mozilla.org/en-US/firefox/39.0/releasenotes/
// https://wiki.mozilla.org/Security/Application_Reputation
pref("browser.safebrowsing.downloads.remote.enabled",					false);
pref("browser.safebrowsing.appRepURL",						"about:blank");
pref("browser.safebrowsing.provider.mozilla.gethashURL",		"about:blank");
pref("browser.safebrowsing.provider.mozilla.updateURL",			"about:blank");
pref("browser.safebrowsing.downloads.remote.block_dangerous",			false);
pref("browser.safebrowsing.downloads.remote.block_dangerous_host",		false);
pref("browser.safebrowsing.downloads.remote.block_potentially_unwanted",	false);
pref("browser.safebrowsing.downloads.remote.block_uncommon",	false);
pref("browser.safebrowsing.downloads.remote.enabled",			false);
pref("browser.safebrowsing.downloads.remote.url",				"");
pref("browser.safebrowsing.provider.google.gethashURL",		"");
pref("browser.safebrowsing.provider.google.updateURL",			"");
pref("browser.safebrowsing.provider.google.lists",				"");

// https://bugzilla.mozilla.org/show_bug.cgi?id=1025965
pref("browser.safebrowsing.phishing.enabled", false);
pref("browser.safebrowsing.provider.google4.lists", "");
pref("browser.safebrowsing.provider.google4.updateURL", "");
pref("browser.safebrowsing.provider.google4.gethashURL", "");
pref("browser.safebrowsing.provider.google4.reportURL", "");
pref("browser.safebrowsing.provider.mozilla.lists", "");

// Disable Microsoft Family Safety MiTM support
// https://bugzilla.mozilla.org/show_bug.cgi?id=1239166
// https://wiki.mozilla.org/SecurityEngineering/Untrusted_Certificates_in_Windows_Child_Mode
// https://hg.mozilla.org/releases/mozilla-release/file/ddb37c386bb2ffa180117b4d30ca3b41a8af233c/security/manager/ssl/nsNSSComponent.cpp#l782
pref("security.family_safety.mode", 0);

// Disable pocket
// https://support.mozilla.org/en-US/kb/save-web-pages-later-pocket-firefox
pref("browser.pocket.enabled",				false);
// https://github.com/pyllyukko/user.js/issues/143
pref("extensions.pocket.enabled",				false);
pref("extensions.pocket.api",			"about:blank");
pref("extensions.pocket.enabled",				false);
pref("browser.pocket.api",			"about:blank");
pref("browser.pocket.enabledLocales",	"about:blank");
pref("browser.pocket.oAuthConsumerKey", "about:blank");
pref("browser.pocket.site",			"about:blank");
pref("browser.pocket.useLocaleList",			false);
pref("browser.toolbarbuttons.introduced.pocket-button", true);

// Disable Hello (Soon to be removed upstream finally!)
pref("loop.copy.throttler", "about:blank");
pref("loop.enabled",false);
pref("loop.facebook.appId", "about:blank");
pref("loop.facebook.enabled", false);
pref("loop.facebook.fallbackUrl", "about:blank");
pref("loop.facebook.shareUrl", "about:blank");
pref("loop.feedback.baseUrl", "about:blank");
pref("loop.feedback.formURL", "about:blank");
pref("loop.feedback.manualFormURL", "about:blank");
pref("loop.gettingStarted.url", "about:blank");
pref("loop.learnMoreUrl", "about:blank");
pref("loop.legal.ToS_url", "about:blank");
pref("loop.legal.privacy_url", "about:blank");
pref("loop.linkClicker.url", "about:blank");
pref("loop.oauth.google.redirect_uri", "about:blank");
pref("loop.oauth.google.scope", "about:blank");
pref("loop.remote.autostart", false);
pref("loop.server", "about:blank");
pref("loop.soft_start_hostname", "about:blank");
pref("loop.support_url", "about:blank");
pref("loop.throttled2", false);

// Disable Social
pref("social.directories", "");
pref("social.enabled", false);
// remote-install allows any website to activate a provider, with extended UI
pref("social.remote-install.enabled", false);
pref("social.shareDirectory", "");
pref("social.toast-notifications.enabled", false);
pref("social.whitelist", "");

// Disable Snippets
pref("browser.snippets.enabled",					false);
pref("browser.snippets.geoUrl",			"about:blank");
pref("browser.snippets.statsUrl",			"about:blank");
pref("browser.snippets.syncPromo.enabled",			false);
pref("browser.snippets.updateUrl",			"about:blank");

// Disable WAN IP leaks
pref("captivedetect.canonicalURL", "about:blank");
pref("noscript.ABE.wanIpAsLocal", false);

// Disable Default Protocol Handlers, always warn user instead
pref("network.protocol-handler.external-default", false);
pref("network.protocol-handler.external.mailto", false);
pref("network.protocol-handler.external.news", false);
pref("network.protocol-handler.external.nntp", false);
pref("network.protocol-handler.external.snews", false);
pref("network.protocol-handler.warn-external.mailto", true);
pref("network.protocol-handler.warn-external.news", true);
pref("network.protocol-handler.warn-external.nntp", true);
pref("network.protocol-handler.warn-external.snews", true);

// Disable Sync
pref("services.sync.engine.addons", false);
// Never sync prefs, addons, or tabs with other browsers
pref("services.sync.engine.prefs", false);
pref("services.sync.engine.tabs", false);
pref("services.sync.prefs.sync.addons.ignoreUserEnabledChanges", false);
pref("services.sync.prefs.sync.extensions.update.enabled", false);
pref("services.sync.serverURL", "about:blank");
pref("services.sync.jpake.serverURL", "about:blank");
// Disable Failed Sync Logs since we killed sync.
pref("services.sync.log.appender.file.logOnError", false);

/******************************************************************************
 * automatic connections                                                      *
 *                                                                            *
 ******************************************************************************/

// Disable link prefetching
// http://kb.mozillazine.org/Network.prefetch-next
// https://developer.mozilla.org/en-US/docs/Web/HTTP/Link_prefetching_FAQ#Is_there_a_preference_to_disable_link_prefetching.3F
pref("network.prefetch-next",				false);

// https://support.mozilla.org/en-US/kb/how-stop-firefox-making-automatic-connections#w_geolocation-for-default-search-engine
pref("browser.search.geoip.url",				"");
pref("browser.search.geoSpecificDefaults.url",		"about:blank");
pref("browser.search.geoSpecificDefaults",			false);
pref("browser.search.geoip.url",			"about:blank");

// http://kb.mozillazine.org/Network.dns.disablePrefetch
// https://developer.mozilla.org/en-US/docs/Web/HTTP/Controlling_DNS_prefetching
pref("network.dns.disablePrefetch",			true);
pref("network.dns.disablePrefetchFromHTTPS",		true);

// https://bugzilla.mozilla.org/show_bug.cgi?id=1228457
pref("network.dns.blockDotOnion",				true);

// https://wiki.mozilla.org/Privacy/Reviews/Necko
pref("network.predictor.enabled",				false);
// https://wiki.mozilla.org/Privacy/Reviews/Necko#Principle:_Real_Choice
pref("network.seer.enabled",				false);

// http://kb.mozillazine.org/Browser.search.suggest.enabled
pref("browser.search.suggest.enabled",			false);
// Disable "Show search suggestions in location bar results"
pref("browser.urlbar.suggest.searches",			false);

// Disable SSDP
// https://bugzil.la/1111967
pref("browser.casting.enabled",				false);

// https://support.mozilla.org/en-US/kb/how-stop-firefox-making-automatic-connections#w_media-capabilities
// http://andreasgal.com/2014/10/14/openh264-now-in-firefox/
pref("media.gmp-gmpopenh264.enabled",			false);
// Disable Gecko media plugins: https://wiki.mozilla.org/GeckoMediaPlugins
pref("media.gmp-manager.url",				"");
pref("media.gmp-manager.url.override", "data:text/plain");
pref("media.gmp.trial-create.enabled", false);

// https://support.mozilla.org/en-US/kb/how-stop-firefox-making-automatic-connections#w_speculative-pre-connections
// https://bugzil.la/814169
pref("network.http.speculative-parallel-limit",		0);

// https://github.com/arthuredelstein/tor-browser/blob/tbb-esr31.1.1/browser/app/profile/000-tor-browser.js
pref("network.http.pipelining", true);
pref("network.http.pipelining.aggressive", true);
pref("network.http.pipelining.maxrequests", 12);
pref("network.http.pipelining.ssl", true);
pref("network.http.proxy.pipelining", true);
pref("security.ssl.enable_false_start", true);
pref("network.http.keep-alive.timeout", 20);
pref("network.http.connection-retry-timeout", 0);
pref("network.http.max-persistent-connections-per-proxy", 256);
pref("network.http.pipelining.reschedule-timeout", 15000);
pref("network.http.pipelining.read-timeout", 60000);
pref("network.http.pipelining.max-optimistic-requests", 3);
pref("network.http.spdy.enabled", false); // Stores state and may have keepalive issues (both fixable)
pref("network.http.spdy.enabled.v2", false); // Seems redundant, but just in case
pref("network.http.spdy.enabled.v3", false); // Seems redundant, but just in case

// https://support.mozilla.org/en-US/kb/how-stop-firefox-making-automatic-connections#w_mozilla-content
pref("browser.aboutHomeSnippets.updateUrl",		"");

// https://support.mozilla.org/en-US/kb/how-stop-firefox-making-automatic-connections#w_auto-update-checking
pref("browser.search.update",				false);

//Disable Link to FireFox Marketplace, currently loaded with non-free "apps"
pref("browser.apps.URL", "");
pref("browser.webapps.checkForUpdates", 0);
pref("browser.webapps.updateCheckUrl", "about:blank");
pref("dom.mozApps.signed_apps_installable_from", "");

// https://bugzilla.mozilla.org/show_bug.cgi?id=1223838#c31
pref("network.http.enablePerElementReferrer", false);

// Disable Favicon lookups
// http://kb.mozillazine.org/Browser.chrome.favicons
// pref("browser.chrome.favicons", false);
// pref("browser.chrome.site_icons", false);

/******************************************************************************
 * HTTP                                                                       *
 *                                                                            *
 ******************************************************************************/

// Disallow NTLMv1
// https://bugzilla.mozilla.org/show_bug.cgi?id=828183
pref("network.negotiate-auth.allow-insecure-ntlm-v1",	false);
// it is still allowed through HTTPS. uncomment the following to disable it completely.
//pref("network.negotiate-auth.allow-insecure-ntlm-v1-https",		false);

// https://bugzilla.mozilla.org/show_bug.cgi?id=855326
pref("security.csp.experimentalEnabled",			true);

// CSP https://developer.mozilla.org/en-US/docs/Web/Security/CSP/Introducing_Content_Security_Policy
pref("security.csp.enable",				true);

// Subresource integrity
// https://developer.mozilla.org/en-US/docs/Web/Security/Subresource_Integrity
// https://wiki.mozilla.org/Security/Subresource_Integrity
pref("security.sri.enable",				true);

// DNT HTTP header
// http://dnt.mozilla.org/
// https://en.wikipedia.org/wiki/Do_not_track_header
// https://dnt-dashboard.mozilla.org
// https://github.com/pyllyukko/user.js/issues/11
// http://www.howtogeek.com/126705/why-enabling-do-not-track-doesnt-stop-you-from-being-tracked/
//pref("privacy.donottrackheader.enabled",		true);

// Disable HTTP Alternative Services header 
// https://trac.torproject.org/projects/tor/ticket/16673
pref("network.http.altsvc.enabled", false);
pref("network.http.altsvc.oe", false); 

// http://kb.mozillazine.org/Network.http.sendRefererHeader#0
// https://bugzilla.mozilla.org/show_bug.cgi?id=822869
// Send a referer header with the target URI as the source
//pref("network.http.sendRefererHeader",			1);
pref("network.http.referer.spoofSource",			true);

// CIS 2.5.1 Accept Only 1st Party Cookies
// http://kb.mozillazine.org/Network.cookie.cookieBehavior#1
// This breaks a number of payment gateways so you may need to comment it out.
pref("network.cookie.cookieBehavior",			1);
// Make sure that third-party cookies (if enabled) never persist beyond the session.
// https://feeding.cloud.geek.nz/posts/tweaking-cookies-for-privacy-in-firefox/
// http://kb.mozillazine.org/Network.cookie.thirdparty.sessionOnly
// https://developer.mozilla.org/en-US/docs/Cookies_Preferences_in_Mozilla#network.cookie.thirdparty.sessionOnly
pref("network.cookie.thirdparty.sessionOnly",		true);

// user-agent
//pref("general.useragent.override",		"Mozilla/5.0 (Windows NT 6.1; rv:31.0) Gecko/20100101 Firefox/31.0");

/******************************************************************************
 * Caching                                                                    *
 *                                                                            *
 ******************************************************************************/
 
 // Prevents the Permissions manager from writing to disk (regardless of whether we are in PBM)
 // https://bugzilla.mozilla.org/show_bug.cgi?id=967812#c9
 pref("permissions.memory_only", true);
 
 // Ensures the intermediate certificate store is memory only.
 // Note: Conflicts with old HTTP Basic Authentication
 // https://bugzilla.mozilla.org/show_bug.cgi?id=1216882#c0
 pref("security.nocertdb", true);

// http://kb.mozillazine.org/Browser.sessionstore.postdata
// NOTE: relates to CIS 2.5.7
pref("browser.sessionstore.postdata",			0);
// http://kb.mozillazine.org/Browser.sessionstore.enabled
pref("browser.sessionstore.enabled",			false);

// http://kb.mozillazine.org/Browser.cache.offline.enable
pref("browser.cache.offline.enable",			false);

// Always use private browsing
// https://support.mozilla.org/en-US/kb/Private-Browsing
// https://wiki.mozilla.org/PrivateBrowsing
// pref("browser.privatebrowsing.autostart",			true);
pref("extensions.ghostery.privateBrowsing",		true);

// Clear history when Firefox closes
// https://support.mozilla.org/en-US/kb/Clear%20Recent%20History#w_how-do-i-make-firefox-clear-my-history-automatically
pref("privacy.sanitize.sanitizeOnShutdown",		true);
pref("privacy.clearOnShutdown.cache",			true);
pref("privacy.clearOnShutdown.cookies",			true);
pref("privacy.clearOnShutdown.downloads",			true);
pref("privacy.clearOnShutdown.formdata",			true);
pref("privacy.clearOnShutdown.history",			true);
pref("privacy.clearOnShutdown.offlineApps",		true);
pref("privacy.clearOnShutdown.passwords",			true);
pref("privacy.clearOnShutdown.sessions",			true);
//pref("privacy.clearOnShutdown.siteSettings",		false);

// Firefox will store small amounts (less than 50 MB) of data without asking for permission, unless this is set to false
// https://support.mozilla.org/en-US/questions/1014708
pref("offline-apps.allow_by_default", false);

// don't remember browsing history
pref("places.history.enabled",				false);

// The cookie expires at the end of the session (when the browser closes).
// http://kb.mozillazine.org/Network.cookie.lifetimePolicy#2
pref("network.cookie.lifetimePolicy",			2);

// http://kb.mozillazine.org/Browser.cache.disk.enable
pref("browser.cache.disk.enable",				false);

// http://kb.mozillazine.org/Browser.cache.memory.enable
//pref("browser.cache.memory.enable",		false);

// CIS Version 1.2.0 October 21st, 2011 2.5.8 Disable Caching of SSL Pages
// http://kb.mozillazine.org/Browser.cache.disk_cache_ssl
pref("browser.cache.disk_cache_ssl",			false);

// CIS Version 1.2.0 October 21st, 2011 2.5.2 Disallow Credential Storage
pref("signon.rememberSignons",				false);

// CIS Version 1.2.0 October 21st, 2011 2.5.5 Delete Download History
// Zero (0) is an indication that no download history is retained for the current profile.
pref("browser.download.manager.retention",			0);

// CIS Version 1.2.0 October 21st, 2011 2.5.6 Delete Search and Form History
pref("browser.formfill.enable",				false);
pref("browser.formfill.expire_days",			0);

// CIS Version 1.2.0 October 21st, 2011 2.5.7 Clear SSL Form Session Data
// http://kb.mozillazine.org/Browser.sessionstore.privacy_level#2
// Store extra session data for unencrypted (non-HTTPS) sites only.
// NOTE: CIS says 1, we use 2
pref("browser.sessionstore.privacy_level",			2);

// https://bugzil.la/238789#c19
pref("browser.helperApps.deleteTempFileOnExit",		true);

// Disable the media cache, prvents HTML5 videos from being written to the OS temporary directory
// https://www.torproject.org/projects/torbrowser/design/
pref("media.cache_size", 0);

// https://support.mozilla.org/en-US/questions/973320
// https://developer.mozilla.org/en-US/docs/Mozilla/Preferences/Preference_reference/browser.pagethumbnails.capturing_disabled
pref("browser.pagethumbnails.capturing_disabled",		true);

/******************************************************************************
 * UI related                                                                 *
 *                                                                            *
 ******************************************************************************/

// Webpages will not be able to affect the right-click menu
//pref("dom.event.contextmenu.enabled",		false);

// Don't promote sync
pref("browser.syncPromoViewsLeftMap", "{\"addons\":0, \"passwords\":0, \"bookmarks\":0}");

// CIS 2.3.2 Disable Downloading on Desktop
pref("browser.download.folderList",			2);

// always ask the user where to download
// https://developer.mozilla.org/en/Download_Manager_preferences
pref("browser.download.useDownloadDir",			false);

// https://wiki.mozilla.org/Privacy/Reviews/New_Tab
pref("browser.newtabpage.enabled",				false);
// https://support.mozilla.org/en-US/kb/new-tab-page-show-hide-and-customize-top-sites#w_how-do-i-turn-the-new-tab-page-off
pref("browser.newtab.url",					"about:blank");

// CIS Version 1.2.0 October 21st, 2011 2.1.2 Enable Auto Notification of Outdated Plugins
// https://wiki.mozilla.org/Firefox3.6/Plugin_Update_Awareness_Security_Review
// Note: Disabled, we get plugin updates from repository.
//pref("plugins.update.notifyUser",				true);

// CIS Version 1.2.0 October 21st, 2011 2.1.3 Enable Information Bar for Outdated Plugins
pref("plugins.hide_infobar_for_outdated_plugin",		false);

// CIS Mozilla Firefox 24 ESR v1.0.0 - 3.6 Enable IDN Show Punycode
// http://kb.mozillazine.org/Network.IDN_show_punycode
pref("network.IDN_show_punycode",				true);

// http://kb.mozillazine.org/About:config_entries#Browser
// http://kb.mozillazine.org/Inline_autocomplete
pref("browser.urlbar.autoFill",				false);
pref("browser.urlbar.autoFill.typed",			false);

// http://www.labnol.org/software/browsers/prevent-firefox-showing-bookmarks-address-location-bar/3636/
// http://kb.mozillazine.org/Browser.urlbar.maxRichResults
// "Setting the preference to 0 effectively disables the Location Bar dropdown entirely."
pref("browser.urlbar.maxRichResults",			0);

// https://blog.mozilla.org/security/2010/03/31/plugging-the-css-history-leak/
// http://dbaron.org/mozilla/visited-privacy
pref("layout.css.visited_links_enabled",			false);

// http://kb.mozillazine.org/Places.frecency.unvisited%28place_type%29Bonus

// http://kb.mozillazine.org/Disabling_autocomplete_-_Firefox#Firefox_3.5
pref("browser.urlbar.autocomplete.enabled",		false);

// http://kb.mozillazine.org/Signon.autofillForms
// https://www.torproject.org/projects/torbrowser/design/#identifier-linkability
pref("signon.autofillForms",				false);

// do not check if firefox is the default browser
pref("browser.shell.checkDefaultBrowser",			false);

// CIS Version 1.2.0 October 21st, 2011 2.5.3 Disable Prompting for Credential Storage
pref("security.ask_for_password",				0);

// Bug 9881: Open popups in new tabs (to avoid fullscreen popups)
pref("browser.link.open_newwindow.restriction",	0);

// Enable Insecure login field contextual warning
// https://bugzilla.mozilla.org/show_bug.cgi?id=1217162
pref("security.insecure_field_warning.contextual.enabled", true);

/******************************************************************************
 * TLS / HTTPS / OCSP related stuff                                           *
 *                                                                            *
 ******************************************************************************/

// https://blog.mozilla.org/security/2012/11/01/preloading-hsts/
// https://wiki.mozilla.org/Privacy/Features/HSTS_Preload_List
pref("network.stricttransportsecurity.preloadlist",	false);

// CIS Version 1.2.0 October 21st, 2011 2.2.4 Enable Online Certificate Status Protocol
// https://en.wikipedia.org/wiki/Online_Certificate_Status_Protocol#Privacy_concerns
pref("security.OCSP.enabled",				0);
pref("security.OCSP.require", false);

// https://blog.mozilla.org/security/2013/07/29/ocsp-stapling-in-firefox/
pref("security.ssl.enable_ocsp_stapling",			true);

// require certificate revocation check through OCSP protocol.
// NOTICE: this leaks information about the sites you visit to the CA.
pref("security.OCSP.require",				true);

// https://www.blackhat.com/us-13/briefings.html#NextGen
// https://media.blackhat.com/us-13/US-13-Daigniere-TLS-Secrets-Slides.pdf
// https://media.blackhat.com/us-13/US-13-Daigniere-TLS-Secrets-WP.pdf
// https://bugzil.la/917049
// https://bugzil.la/967977
pref("security.ssl.disable_session_identifiers",		true);
// https://www.torproject.org/projects/torbrowser/design/index.html.en
pref("security.ssl.enable_false_start",			true);
pref("security.enable_tls_session_tickets", false);

// TLS 1.[012]
// http://kb.mozillazine.org/Security.tls.version.max
// 1 = TLS 1.0 is the minimum required / maximum supported encryption protocol. (This is the current default for the maximum supported version.)
// 2 = TLS 1.1 is the minimum required / maximum supported encryption protocol.
pref("security.tls.version.min",				1);
pref("security.tls.version.max",				3);

// pinning
// https://wiki.mozilla.org/SecurityEngineering/Public_Key_Pinning#How_to_use_pinning
// "2. Strict. Pinning is always enforced."
pref("security.cert_pinning.enforcement_level",		2);

// disallow SHA-1
// https://bugzilla.mozilla.org/show_bug.cgi?id=1302140
//pref("security.pki.sha1_enforcement_level",		1);

// https://wiki.mozilla.org/Security:Renegotiation#security.ssl.treat_unsafe_negotiation_as_broken
// see also CVE-2009-3555
pref("security.ssl.treat_unsafe_negotiation_as_broken",	true);

// https://wiki.mozilla.org/Security:Renegotiation#security.ssl.require_safe_negotiation
// this makes browsing next to impossible=) (13.2.2012)
// update: the world is not ready for this! (6.5.2014)
// see also CVE-2009-3555
// The world must comply with this! (11.20.2016)
pref("security.ssl.require_safe_negotiation",		true);

// https://support.mozilla.org/en-US/kb/certificate-pinning-reports
//
// we could also disable security.ssl.errorReporting.enabled, but I think it's
// good to leave the option to report potentially malicious sites if the user
// chooses to do so.
//
// you can test this at https://pinningtest.appspot.com/
pref("security.ssl.errorReporting.automatic",		false);

// http://kb.mozillazine.org/Browser.ssl_override_behavior
// Pre-populate the current URL but do not pre-fetch the certificate.
pref("browser.ssl_override_behavior",			1);

/******************************************************************************
 * CIPHERS                                                                    *
 *                                                                            *
 * you can debug the SSL handshake with tshark:                               *
 *     tshark -t ad -n -i wlan0 -T text -V -R ssl.handshake                   *
 ******************************************************************************/

// disable null ciphers
pref("security.ssl3.rsa_null_sha",				false);
pref("security.ssl3.rsa_null_md5",				false);
pref("security.ssl3.ecdhe_rsa_null_sha",			false);
pref("security.ssl3.ecdhe_ecdsa_null_sha",			false);
pref("security.ssl3.ecdh_rsa_null_sha",			false);
pref("security.ssl3.ecdh_ecdsa_null_sha",			false);

// SEED
// https://en.wikipedia.org/wiki/SEED
pref("security.ssl3.rsa_seed_sha",				false);

// 40 bits...
pref("security.ssl3.rsa_rc4_40_md5",			false);
pref("security.ssl3.rsa_rc2_40_md5",			false);

// 56 bits
pref("security.ssl3.rsa_1024_rc4_56_sha",			false);

// 128 bits
pref("security.ssl3.rsa_camellia_128_sha",			false);
pref("security.ssl3.ecdhe_rsa_aes_128_sha",		false);
pref("security.ssl3.ecdhe_ecdsa_aes_128_sha",		false);
pref("security.ssl3.ecdh_rsa_aes_128_sha",			false);
pref("security.ssl3.ecdh_ecdsa_aes_128_sha",		false);
pref("security.ssl3.dhe_rsa_camellia_128_sha",		false);
pref("security.ssl3.dhe_rsa_aes_128_sha",			false);

// RC4 (CVE-2013-2566)
pref("security.ssl3.ecdh_ecdsa_rc4_128_sha",		false);
pref("security.ssl3.ecdh_rsa_rc4_128_sha",			false);
pref("security.ssl3.ecdhe_ecdsa_rc4_128_sha",		false);
pref("security.ssl3.ecdhe_rsa_rc4_128_sha",		false);
pref("security.ssl3.rsa_rc4_128_md5",			false);
pref("security.ssl3.rsa_rc4_128_sha",			false);
// https://developer.mozilla.org/en-US/Firefox/Releases/38#Security
// https://bugzil.la/1138882
// https://rc4.io/
pref("security.tls.unrestricted_rc4_fallback",		false);

// 3DES -> false because effective key size < 128
// https://en.wikipedia.org/wiki/3des#Security
// http://en.citizendium.org/wiki/Meet-in-the-middle_attack
// http://www-archive.mozilla.org/projects/security/pki/nss/ssl/fips-ssl-ciphersuites.html
pref("security.ssl3.dhe_dss_des_ede3_sha",			false);
pref("security.ssl3.dhe_rsa_des_ede3_sha",			false);
pref("security.ssl3.ecdh_ecdsa_des_ede3_sha",		false);
pref("security.ssl3.ecdh_rsa_des_ede3_sha",		false);
pref("security.ssl3.ecdhe_ecdsa_des_ede3_sha",		false);
pref("security.ssl3.ecdhe_rsa_des_ede3_sha",		false);
pref("security.ssl3.rsa_des_ede3_sha",			false);
pref("security.ssl3.rsa_fips_des_ede3_sha",		false);

// Ciphers with ECDH (without /e$/)
pref("security.ssl3.ecdh_rsa_aes_256_sha",			false);
pref("security.ssl3.ecdh_ecdsa_aes_256_sha",		false);

// 256 bits without PFS
pref("security.ssl3.rsa_camellia_256_sha",			false);

// Ciphers with ECDHE and > 128bits
pref("security.ssl3.ecdhe_rsa_aes_256_sha",		true);
pref("security.ssl3.ecdhe_ecdsa_aes_256_sha",		true);

// GCM, yes please!
pref("security.ssl3.ecdhe_ecdsa_aes_128_gcm_sha256",	true);
pref("security.ssl3.ecdhe_rsa_aes_128_gcm_sha256",		true);

// ChaCha20 and Poly1305. Supported since Firefox 47.
// https://www.mozilla.org/en-US/firefox/47.0/releasenotes/
// https://tools.ietf.org/html/rfc7905
// https://bugzil.la/917571
// https://bugzil.la/1247860
// https://cr.yp.to/chacha.html
pref("security.ssl3.ecdhe_ecdsa_chacha20_poly1305_sha256",	true);
pref("security.ssl3.ecdhe_rsa_chacha20_poly1305_sha256",	true);

// Susceptible to the logjam attack - https://weakdh.org/
pref("security.ssl3.dhe_rsa_camellia_256_sha",		false);
pref("security.ssl3.dhe_rsa_aes_256_sha",			false);

// Ciphers with DSA (max 1024 bits)
pref("security.ssl3.dhe_dss_aes_128_sha",			false);
pref("security.ssl3.dhe_dss_aes_256_sha",			false);
pref("security.ssl3.dhe_dss_camellia_128_sha",		false);
pref("security.ssl3.dhe_dss_camellia_256_sha",		false);

// Fallbacks due compatibility reasons
pref("security.ssl3.rsa_aes_256_sha",			true);
pref("security.ssl3.rsa_aes_128_sha",			true);

// Disable static TLS insecure fallback whitelist 
//  https://bugzilla.mozilla.org/show_bug.cgi?id=1128227
pref("security.tls.insecure_fallback_hosts.use_static_list", false);
