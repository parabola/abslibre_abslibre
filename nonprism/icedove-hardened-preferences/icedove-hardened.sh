#!/bin/sh

##############################################
# Icedove-Hardened Jail and Cleaner Script #
##############################################
echo "Closing any other instances of Icedove to avoid crashes..."
pkill -x icedove
wait
echo "Copying Hardened Prefs..."
cp /usr/lib/icedove/defaults/preferences/icedove-branding.js /usr/lib/icedove/defaults/preferences/thunderbird-branding.js
wait
echo "Waking the Icedove..."

# Trap cleaner function for IceDove exit cleaning
function finish {
echo "Removing hardened preferences..."
echo "" > /usr/lib/icedove/defaults/preferences/thunderbird-branding.js
    }

## Firejail Icedove startup
/usr/bin/firejail --profile=/etc/firejail/icedove.profile --private-bin=bash,gpg2,pinentry,pinentry-gnome3,pinentry-curses,pinentry-tty,pinentry-gtk-2,pinentry-qt,pinentry-emacs,gpg-agent,icedove --private-tmp --private-etc=nsswitch.conf,resolv.conf --private-dev --nogroups /usr/bin/icedove

## Exiting Icedove triggers the trap
trap finish EXIT