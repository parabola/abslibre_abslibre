# $Id: PKGBUILD 282099 2016-11-26 21:17:51Z foutrelis $
# Maintainer (Arch): Eric B�langer <eric@archlinux.org>
# Maintainer: Andr� Silva <emulatorman@parabola.nu>
# Contributor: Luke R. <g4jc@openmailbox.org>

pkgname=webkit2gtk
pkgver=2.14.2
pkgrel=2.nonprism1
pkgdesc="GTK+ Web content engine library, without geoclue2 support"
arch=('i686' 'x86_64')
url="http://webkitgtk.org/"
license=('custom')
depends=('libxt' 'libxslt' 'enchant' 'gst-plugins-base-libs'
	 'libsecret' 'libwebp' 'harfbuzz-icu' 'gtk3' 'libnotify' 'hyphen' 'libsoup') # since geoclue2 was removed, libsoup has been added as deps
makedepends=('gtk2' 'gperf' 'gobject-introspection' 'ruby' 'gtk-doc' 'cmake' 'python' 'python2')
optdepends=('gtk2: Netscape plugin support'
            'gst-plugins-base: free media decoding'
            'gst-plugins-good: media decoding'
            'gst-libav: nonfree media decoding')
options=('!emptydirs')

# webkitgtk's signature scheme (sha1-file-as-pgp-message) is bananas and not supported
source=(https://webkitgtk.org/releases/webkitgtk-${pkgver}.tar.xz)
sha1sums=('857590669ed1c6278413dfbf0619e8473664537f')

prepare() {
  mkdir build

  cd webkitgtk-$pkgver
  sed -i '1s/python$/&2/' Tools/gtk/generate-gtkdoc
  rm -r Source/ThirdParty/gtest/
  rm -r Source/ThirdParty/qunit/
}

build() {
  cd build
  cmake -DPORT=GTK -DCMAKE_BUILD_TYPE=Release \
        -DCMAKE_SKIP_RPATH=ON -DCMAKE_INSTALL_PREFIX=/usr \
        -DLIB_INSTALL_DIR=/usr/lib -DLIBEXEC_INSTALL_DIR=/usr/lib/webkit2gtk-4.0 \
	-DUSE_GEOCLUE2=OFF -DENABLE_GEOLOCATION=OFF \
        -DENABLE_GTKDOC=ON -DPYTHON_EXECUTABLE=/usr/bin/python2 ../webkitgtk-$pkgver
  make
}

package() {
  cd build
  make DESTDIR="$pkgdir" install

  install -m755 -d "$pkgdir/usr/share/licenses/webkit2gtk"
  cd "$srcdir/webkitgtk-$pkgver/Source"
  for f in $(find -name 'COPYING*' -or -name 'LICENSE*'); do
    echo $f >> "$pkgdir/usr/share/licenses/webkit2gtk/LICENSE"
    cat $f >> "$pkgdir/usr/share/licenses/webkit2gtk/LICENSE"
    echo "" >> "$pkgdir/usr/share/licenses/webkit2gtk/LICENSE"
  done
}
