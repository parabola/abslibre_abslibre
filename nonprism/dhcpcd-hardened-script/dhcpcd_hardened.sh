#!/bin/sh
# This script wipes hostname and DUID so that DHCP can't send it over the network
hostnamectl set-hostname localhost
sed -i '/127.0.0.1/c\127.0.0.1       localhost.localdomain   localhost 'localhost'' /etc/hosts  ##properly sets our new host in /etc/hosts file
hostname localhost ##avoids need to reboot before showing our new hostname in terminals etc.
rm -rf /etc/dhcpcd.duid # Delete DUID
