# Contributor (Arch): Xavier Devlamynck <magicrhesus@ouranos.be>
# Contributor (Arch): Keshav P R, atommix aka Aleks Lifey, Xavion, Ananda Samaddar, Dan Serban, Xyne
# Maintainer: André Silva <emulatorman@parabola.nu>
# Contributor: Márcio Silva <coadde@parabola.nu>

pkgname=jitsi
pkgver=2.8.5426
pkgrel=1.nonprism1
pkgdesc="An audio/video SIP VoIP phone and instant messenger written in Java (formerly SIP-Communicator), without support for unsafe and dangerous for privacy protocols"
arch=('i686' 'x86_64' 'mips64el')
url="http://jitsi.org"
license=('LGPL')
replaces=(${pkgname}-nonprism)
conflicts=(${pkgname}-nonprism)
depends=('java-runtime')
makedepends=('apache-ant' 'java-environment')
options=(!strip !emptydirs zipman !libtool docs)
source=("http://download.jitsi.org/jitsi/src/jitsi-src-${pkgver}.zip"
		jitsi.desktop
		jitsi.sh
		drop-open-term-on-index-jn-html.patch
                nonprism.patch)
sha256sums=('e649867e186cf195c4b1bb159d7ffe0a38c4dc60659e5c55f9a74461a309f36c'
            '61e3bec3470790fa067f87d978016ec4452a6fd3dfba2c9afa5245b58d3cb19d'
            '529722efc3bae6cb8923680363a4d778ccf78db9caf20bee90a110d34181d9f5'
            'a862465431e9d272e00bb9aafe56edec0d5c4f3718062add53972685adb90631'
            'bb3a3473fb70b6392ec3c8b400f81c18e1895b9186817401d9fca69e251223aa')

prepare() {
  cd $srcdir/$pkgname

  # append the build revision to the jitsi version
  sed -i "s|0.build.by.SVN|build.$pkgver|" src/net/java/sip/communicator/impl/version/NightlyBuildID.java

  # remove Open Source term
  patch -Np0 -i ../drop-open-term-on-index-jn-html.patch
  sed -i 's|Open Source / ||;
          s|open source / ||;
          s|Open Source|Free Software|;
          s|open source|free software|;
         ' $(grep -ril 'open source')
  sed -i 's|OpenSource / ||;
          s|opensource / ||;
          s|OpenSource|FreeSoftware|;
          s|opensource|freesoftware|;
         ' $(grep -ril opensource)

  # remove Linux term
  sed -i '/, Linux/ s|Linux|GNU/Linux|;
          /And Linux/ s|Linux|GNU/Linux|;
          /and Linux/ s|Linux|GNU/Linux|;
          /For Linux/ s|Linux|GNU/Linux|;
          /for Linux/ s|Linux|GNU/Linux|;
          /Linux,/ s|Linux|GNU/Linux|;
          /Linux and/ s|Linux|GNU/Linux|;
          /Linux distribution/ s|Linux|GNU/Linux|;
          /Linux machine/ s|Linux|GNU/Linux|;
          /Linux operating/ s|Linux|GNU/Linux|;
          /Linux OS/ s|Linux|GNU/Linux|;
          /Linux specific/ s|Linux|GNU/Linux|;
          /Linux system/ s|Linux|GNU/Linux|;
          /Under Linux/ s|Linux|GNU/Linux|;
          /under Linux/ s|Linux|GNU/Linux|;
          /On Linux/ s|Linux|GNU/Linux|;
          /on Linux/ s|Linux|GNU/Linux|;
          s|GNU/GNU|GNU|;
          s|GNU/Linux kernel|Linux kernel|;
          s|GNU/Linux specific kernel|Linux specific kernel|;
         ' $(grep -rl Linux)

  # remove support for unsafe and dangerous for privacy protocols (AIM, Facebook, Google Talk, ICQ, MSN and Yahoo!)
  patch -Np1 -i $srcdir/nonprism.patch
}

build() {
  cd $srcdir/$pkgname
  . /etc/profile.d/apache-ant.sh
  ant rebuild
}

package() {
  cd $srcdir/$pkgname
  find lib/ lib/bundle/ -maxdepth 1 -type f -exec install -Dm644 {} "${pkgdir}/usr/lib/${pkgname}/"{} \;
#  find lib/os-specific/linux/ -maxdepth 1 -type f -execdir install -Dm644 {} "${pkgdir}/usr/lib/${pkgname}/lib/"{} \;
  shopt -sq extglob
  find lib/native/linux$(sed 's/_/-/g' <<<${CARCH/#*(i?86|x86)/})/ -maxdepth 1 -type f -execdir install -Dm644 {} "${pkgdir}/usr/lib/${pkgname}/lib/native/"{} \;
  find sc-bundles/{,os-specific/linux/} -maxdepth 1 -type f -execdir install -Dm644 {} "${pkgdir}/usr/lib/${pkgname}/sc-bundles/"{} \;
  install -Dm755 "${srcdir}/${pkgname}.sh" "${pkgdir}/usr/bin/${pkgname}"
  install -Dm644 "${srcdir}/${pkgname}.desktop" "${pkgdir}/usr/share/applications/${pkgname}.desktop"

  cd "resources/install/debian/"
  for _file in *.{svg,xpm}; do
    install -Dm644 "$_file" "${pkgdir}/usr/share/pixmaps/${_file}"
  done
}
